# -*- coding: utf-8 -*-
# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved

import logging
import traceback

from AsiUtilities import log
from AsiUtilities.JsonStdTypesEncoder import JsonStdTypesEncoder
from AsiUtilities.NetMgrIf import NetMgr
from AsiUtilities.parameters_from_string import parameters_from_string
from AsiUtilities.sort_util import natural_key
from flask import Flask, render_template, request, jsonify, Response, json, flash, Blueprint
from flask_autoindex import AutoIndexBlueprint
from flask_uploads import UploadSet, ARCHIVES, configure_uploads, UploadNotAllowed
from api_v2 import bp_api_v2
from AsiBroker.ApiClient import ApiClient

# TODO: fix hard coding the IP address
PHASE_CAL_IP = "192.168.50.195"

phase_client = ApiClient(PHASE_CAL_IP, port=5556)
# nm = NetMgr()

# Init logging
log.init_log('debug')

# Init App
app = Flask(__name__)
app.jinja_env.trim_blocks = True
app.jinja_env.lstrip_blocks = True


# Jinja filter
@app.template_filter('sort_nat')
def sort_nat(L):
    return sorted(L, key=natural_key)


# API
app.config.SWAGGER_UI_DOC_EXPANSION = 'list'
# app.config.SWAGGER_UI_JSONEDITOR = True
app.config['RESTPLUS_MASK_SWAGGER'] = False
app.register_blueprint(bp_api_v2)


# enable configuring web server logging
def set_logger_level_web_server(level=None):
    if isinstance(level, str):
        try:
            level = getattr(logging, level.upper())
        except AttributeError:
            pass
    if isinstance(level, int) and 0 <= level <= 50:
        logging.getLogger('werkzeug').setLevel(level)
        logging.getLogger('geventwebsocket.handler').setLevel(level)
    return logging.getLevelName(logging.getLogger('werkzeug').getEffectiveLevel())


def get_logger_level_web_server():
    return set_logger_level_web_server()


def set_logger_level_app(level=None):
    if isinstance(level, str):
        try:
            level = getattr(logging, level.upper())
        except AttributeError:
            pass
    if isinstance(level, int) and 0 <= level <= 50:
        app.logger.setLevel(level)
    return logging.getLevelName(app.logger.getEffectiveLevel())


def get_logger_level_app():
    return set_logger_level_app()


# Set web server loggers to warning
set_logger_level_web_server('warning')
# Set app logger to info
set_logger_level_app('info')

app.json_encoder = JsonStdTypesEncoder.StdTypesEncoder

app.secret_key = '\xad\xfa\xe5\t\xb4\x88sZP!\xef\x1fck\x89n\xd5\x04x\x97\xb0\xb1Kp'

auto_bp = Blueprint('auto_bp', __name__)
AutoIndexBlueprint(auto_bp, browse_root='archives')
app.register_blueprint(auto_bp, url_prefix='/archives')

cal_archives = UploadSet('archives', ARCHIVES)
archives_dest = 'archives'
app.config['UPLOADED_ARCHIVES_DEST'] = archives_dest
configure_uploads(app, cal_archives)


def get_value(req):
    """
    Accepts a GET Query -> http://localhost:8000/logger/ws?level
    Accepts a PUT of a JSON boolean true (put must be JSON with only LEVEL) ->
    curl -H "Content-type: application/json" http://localhost:8000/logger/ws -X PUT -d 'info'

    :param req:
    :return:
    """
    value = None
    if req.method == 'GET' and req.query_string:
        value = req.query_string.decode()
    if req.method == 'PUT':
        try:
            value = req.get_data(as_text=True)
        except Exception as err:
            app.logger.warning("{}: {}".format(type(err).__name__, err))
    return value


@app.route('/ws', methods=['GET'])
def display_json():
    return render_template('json-display.html')


@app.route('/loggers', methods=['GET'])
def get_loggers():
    result = list(logging.Logger.manager.loggerDict.keys())
    result = {k: logging.getLevelName(logging.getLogger(k).getEffectiveLevel()) for k in result}
    # jsonify only works with dicts
    resp = Response(response=json.dumps(result),
                    status=200,
                    mimetype="application/json")
    return resp


@app.route('/logger/app', methods=['GET', 'PUT'])
def logger_app():
    app.logger.info("{}".format(request.url))
    value = get_value(request)
    result = set_logger_level_app(value)
    # jsonify only works with dicts
    resp = Response(response=json.dumps(result),
                    status=200,
                    mimetype="application/json")
    app.logger.info("{}{}?{}={}".format(request.host_url.rstrip('/'), request.path, value, result))
    return resp


@app.route('/logger/web', methods=['GET', 'PUT'])
def logger_web():
    app.logger.info("{}".format(request.url))
    value = get_value(request)
    result = set_logger_level_web_server(value)
    # jsonify only works with dicts
    resp = Response(response=json.dumps(result),
                    status=200,
                    mimetype="application/json")
    app.logger.info("{}{}?{}={}".format(request.host_url.rstrip('/'), request.path, value, result))
    return resp


@app.route('/api/help', methods=['GET'])
def api_help():
    routes_dict = {rule.rule: app.view_functions[rule.endpoint].__doc__
                   for rule in app.url_map.iter_rules()
                   if rule.endpoint != 'static'}
    return jsonify(routes_dict)


@app.route('/')
def hello():
    return render_template('index.html')


@app.route('/issue_command', methods=['POST'])
@app.route('/issueCommand', methods=['POST'])
def issue_command(api_obj=phase_client):
    count = 0
    message = request.get_json(force=True)
    try:
        cmd_str = '{}({})'.format(message['command'], message['command_arg'])
    except KeyError:
        cmd_str = '{}'.format(message)
    remote_addr = request.remote_addr
    app.logger.info(remote_addr + " -> " + cmd_str)
    try:
        args, kwargs = parameters_from_string(message['command_arg'])
        result, count = api_obj.issueCommandDetailed(message['command'], *args, **kwargs)
        app.logger.info(remote_addr + " -> " + cmd_str + "=" + str(result))
    except Exception as err:
        result = "{}: {}".format(type(err).__name__, err)
        app.logger.warning(cmd_str + " -> " + result)
        app.logger.debug(traceback.format_exc())
    return jsonify(command=cmd_str, result=result, count=count)


@app.route('/issue_command_ns', methods=['POST'])
def issue_command_ns():
    return issue_command(phase_client)



def render_settings_template(sm_obj, sm_ref, sm_name, ic_url, remote_addr, url_req):
    message = dict(command=sm_ref + '.get_settings')
    current, _ = sm_obj.issueCommandDetailed(message['command'])
    message = dict(command=sm_ref + '.get_settings_factory')
    factory, _ = sm_obj.issueCommandDetailed(message['command'])
    message = dict(command=sm_ref + '.get_settings_user_file')
    user_file, _ = sm_obj.issueCommandDetailed(message['command'])
    user_file_missing = {k: v for k, v in factory.items() if k not in user_file}
    user_file.update(user_file_missing)
    results = {'settings_current': current,
               'settings_factory': factory,
               'settings_user_file': user_file,
               'sm_ref': sm_ref,
               'sm_name': sm_name,
               'ic_url': ic_url,
               }
    app.logger.info("{} -> {}={}".format(remote_addr, url_req, results))
    return render_template('settings_manager.html', **results)


@app.route('/settings', methods=['GET'])
def settings():
    return render_template('settings.html')


@app.route('/publisher', methods=['GET'])
def pub_config():
    remote_addr = request.remote_addr
    url_req = request.url_rule
    sm_obj = phase_client
    sm_ref = 'settings_manager'
    sm_name = 'Publisher'
    ic_url = '/issue_command'
    return render_settings_template(sm_obj, sm_ref, sm_name, ic_url, remote_addr, url_req)


@app.route('/radio_config', methods=['GET'])
def radio_config():
    remote_addr = request.remote_addr
    url_req = request.url_rule
    sm_obj = phase_client
    sm_ref = 'asi_settings_manager'
    sm_name = '(CHANGES REQUIRE RESTART) Radio'
    ic_url = '/issue_command'
    return render_settings_template(sm_obj, sm_ref, sm_name, ic_url, remote_addr, url_req)



app.logger.info("--Web Api Initialized--")

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=False)
