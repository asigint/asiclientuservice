# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

import logging
import os
import sys
import traceback
from ctypes import cdll


# noinspection PyPep8Naming
def initPythonApi(LibraryName, DEBUG_DLL=False, lib_path=None, logger=None):
    """
    Load a C/C++ Library referenced by Library Name (not filename)
    If lib_path not specified, search the working directory, user directory, and package directory
    for a /lib with the file.
    :param LibraryName: Name of the library to be loaded, absent extensions (such as .dll)
    :type LibraryName: str
    :param DEBUG_DLL: use /lib-debug instead of /lib when True
    :type DEBUG_DLL: bool
    :param lib_path: path to where the library file is found
    :type lib_path: str | None
    :param logger: active logger
    :type logger: logging.logger | None
    :return: Handle that points to the library
    """

    if logger is None:
        logging.basicConfig(level=logging.DEBUG)
        logger = logging.getLogger(__name__)

    if sys.platform.startswith('linux'):
        lib_name = 'lib' + LibraryName + '.so'
        system_lib = '/usr/lib'
    else:
        lib_name = LibraryName + '.dll'
        system_lib = os.path.join(os.environ['WINDIR'], 'system32')

    if DEBUG_DLL:
        lib_dir = 'lib-debug'
    else:
        lib_dir = 'lib'

    lib_ref = None

    try:
        if lib_path is not None and os.path.isdir(lib_path) and os.path.isfile(os.path.join(lib_path, lib_name)):
            # File found in specified path
            search_file = os.path.join(lib_path, lib_name)
            lib_ref = cdll.LoadLibrary(search_file)
            logger.debug("Loaded Library: {}".format(search_file))
        else:
            # Generate search paths
            user_dir = os.path.expanduser('~')
            working_dir, _ = os.path.split(os.getcwd())
            file_dir, _ = os.path.split(os.path.realpath(__file__))
            package_parent_dir = os.path.realpath(os.path.join(file_dir, os.pardir, os.pardir))

            # Order search
            search_order = [working_dir, user_dir, package_parent_dir, system_lib]

            # Search
            for directory in search_order:
                if directory == system_lib:
                    search_dir = system_lib
                else:
                    search_dir = os.path.join(directory, lib_dir)
                search_file = os.path.join(search_dir, lib_name)
                if os.path.isdir(search_dir) and os.path.isfile(search_file):
                    lib_ref = cdll.LoadLibrary(search_file)
                    logger.debug("Loaded Library: {}".format(search_file))
                    break
    except OSError as err:
        logger.warning("OSError: {} for {}".format(err, LibraryName))
        for err_data in traceback.extract_tb(sys.exc_info()[2], limit=3):
            filename, line_no, function_name, line_text = err_data
            logger.debug("{}:{} in {}".format(os.path.basename(filename), line_no, function_name))

    if lib_ref is None:
        logger.critical("Failed to find library: {}".format(LibraryName))
    return lib_ref


if __name__ == "__main__":
    libRef = initPythonApi(sys.argv[1])
