import time

from flask import current_app as app
from flask import request
from flask_restplus import Namespace, Resource, fields
from AsiBroker.ApiClient import ApiClient


PHASE_CAL_IP = "192.168.50.195"

phase_client = ApiClient(PHASE_CAL_IP)


api = Namespace('phase_tone', description='Control phase tone')


cal_model = api.model('cal_model', {
    "cal_status": fields.Boolean,
    "center_frequency_hz": fields.Float,
    "phase_cal_frequency_hz": fields.Float,
    "receiver_status": fields.Boolean,
    "antenna_status": fields.Boolean,
    "channels": fields.List(fields.Integer, description='channels'),
})


@api.marshal_with(cal_model)
@api.route('')
class Tone(Resource):
    @staticmethod
    def get():
        """
        Get the current Front End Control parameters

        Key definitions in PUT function.

            curl -v -H "Content-type: application/json" http://localhost:8000/api/v2/stream
        """
        message = dict(command='get_status', command_arg=None)
        result, count = phase_client.issueCommandDetailed(message['get_settings'])
        return result

    @api.expect(cal_model)
    def put(self):
        """
        Set the current Front End Control parameters

        Definitions (all keys are optional):

            {
            "cal_status": turn cal tone on/off
            "center_frequency_hz": radio's tune frequency in Hz
            "phase_cal_frequency_hz": phase cal board's frequency in Hz
            "receiver_status": turns the receiver switch on/off
            "antenna_status": turns antenna switch on/off
            "channels": channels affected ([] = all channels)
            }

        """
        start_time = time.time()
        remote_addr = request.remote_addr
        request_path = request.path

        # Ensure Content-Type is set
        ct = request.headers.get('Content-Type')
        if request.headers.get('Content-Type') != 'application/json':
            app.logger.warning("[{:.2f}s]{}->{}<-Content-Type={!r}".format(time.time() - start_time,
                                                                           remote_addr, request_path, ct))

        iq_unicast_parms = request.get_json(force=True)
        app.logger.debug("{}->{}t:{}".format(remote_addr, request_path, iq_unicast_parms))
        if iq_unicast_parms:
            if 'ip_addr' in iq_unicast_parms and iq_unicast_parms['ip_addr'] == "":
                iq_unicast_parms['ip_addr'] = remote_addr
                app.logger.warning("Stream: using requester address: {}".format(remote_addr))
            result, count = phase_client.issueCommandDetailed('set_udp_stream', **iq_unicast_parms)
            app.logger.info("[{:.2f}s]{}->{}\n:{}\n={}".format(time.time() - start_time, remote_addr, request_path,
                                                               iq_unicast_parms, result))
            return result
        else:
            # Warn unable to parse json
            data = request.get_data(as_text=True)
            app.logger.warning("[{:.2f}s]{}->{}:{}<-INVALID DATA".format(time.time() - start_time,
                                                                         remote_addr, request_path, data))
        return None