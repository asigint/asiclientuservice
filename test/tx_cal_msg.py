from AsiProto.python_proto import Frontend_pb2 as AsiMessage_FE
import socket

if __name__ == "__main__":
	ip = '127.0.0.1'
	port = 22322

	tx_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	tx_sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

	ctm = AsiMessage_FE.CalTone()
	ctm.cal_status = True  # status is set to ON
	ctm.receiver_status = True
	ctm.antenna_status = False
	ctm.asi_id = AsiMessage_FE.ASI_ID_pb2.CalTone
	ctm.center_frequency_hz = 162000000.0
	ctm.phase_cal_frequency_hz = 162001000.0
	ctm.bandwidth_frequency_hz = 40000000.0
	ctm.sample_rate = 51200000.0
	ctm.channel.extend([1,2,3,4,5,6])
	tx_sock.sendto(ctm.SerializeToString(), (ip, port))
	print('Sent : {}'.format(ctm))
