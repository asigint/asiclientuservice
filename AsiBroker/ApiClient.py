#!/usr/bin/python env
# -*- coding: utf-8 -*-
# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

import logging
import re
import time
import traceback
from datetime import datetime

from AsiBroker.ApiListStub import ApiListStub
from AsiBroker.ApiNestStub import ApiNestStub
from AsiBroker.ApiStub import ApiStub
from AsiBroker.ZmqClient import ZmqClient, ZeroMqClientReplyEx
from AsiUtilities.JsonNpB64Encoder import JsonNpB64Encoder


# noinspection PyPep8Naming
class ApiClient(ZmqClient):
    def __init__(self, *args, **kwargs):
        """

        :param args: optional host address, equivalent to host=
        :param kwargs:
        :keyword protocol: default='tcp://'
        :keyword host: default='localhost'
        :keyword port: default=5555
        :keyword client_name: default=hostname:pid
        :keyword req_timeout_retries: default=3
        :keyword req_timeout_ms: default=10000
        :keyword log_level: default=logging.DEBUG (10)
        :keyword auto_api: default=True
        """
        log_level = kwargs.pop('log_level', logging.DEBUG)
        self._logger_api = logging.getLogger(name='ApiClient')
        self._logger_api.setLevel(log_level)
        self._logger_api.debug("Initialized: {}".format(str(datetime.now())))
        start = time.time()

        auto_api = kwargs.pop('auto_api', True)

        # Make optional arg the desired host
        if len(args) == 1 and isinstance(args[0], str):
            kwargs['host'] = args[0]

        self._methods = None

        ZmqClient.__init__(self, log_level=logging.INFO, **kwargs)

        if auto_api:
            # Build API functions
            """:type: dict | None"""
            self.api_generate()

        duration = time.time() - start
        self._logger_api.debug("Ready ({:.2f}s): {}".format(duration, str(datetime.now())))

    def api_logging_level(self, level):
        if isinstance(level, int) and level in [logging.DEBUG, logging.INFO, logging.WARNING]:
            self._logger_api.setLevel(level)
        return self._logger_api.getEffectiveLevel()

    def api_generate(self, rebuild=False):
        # Simply return True if methods exist and not rebuilding
        if self._methods is None or rebuild:

            if rebuild and self._methods is not None:
                self._logger_api.debug("REBUILDING API")
                for key in self._methods.keys():
                    list_name = re.findall('(.*)\[\d+\]', key)
                    if list_name:
                        try:
                            delattr(self, list_name[0])
                        except AttributeError:
                            # List has already been deleted
                            pass
                    else:
                        try:
                            delattr(self, key)
                        except AttributeError as err:
                            # Attribute is a member of ApiClient
                            delattr(self, 'remote_' + key)
            else:
                self._logger_api.debug("BUILDING API")

            self._methods = self.issueCommand('getMethods')
            """:type: dict"""

            if self._methods is None:
                self._logger_api.critical("NO METHODS FOUND")
                return False
            else:
                ApiClient._generate_attributes(self, self._methods)
                self._logger_api.info("METHODS LOADED: {}".format(str(datetime.now())))
        return True

    @staticmethod
    def _generate_attributes(parent, methods):
        for key, value in methods.items():
            if isinstance(value, dict):
                list_index = re.findall('\[(\d+)\]', key)
                if list_index:
                    list_index = int(list_index[0])
                    key_list_name = re.findall('(.*)\[\d+\]', key)[0]
                    list_variable = getattr(parent, key_list_name, None)
                    """:type: list"""
                    if list_variable is None:
                        setattr(parent, key_list_name, [])
                        list_variable = getattr(parent, key_list_name)
                    list_len = len(list_variable)
                    if list_index >= list_len:
                        list_variable.extend([None for _ in range(list_index - list_len + 1)])
                    list_variable[list_index] = ApiListStub(key_list_name, parent, value, list_index)
                else:
                    setattr(parent, key, ApiNestStub(key, parent, value))
            else:
                if hasattr(parent, key):
                    logger = logging.getLogger('ApiClient')
                    logger.warning("Key in parent class: {0} - Using remote_{0}".format(key))
                    setattr(parent, 'remote_' + key, ApiStub(key, parent, value))
                else:
                    setattr(parent, key, ApiStub(key, parent, value))

    def issueCommandDetailed(self, cmd, *args, **kwargs):
        message = dict()
        message['command'] = cmd
        argspec = ''
        kwargspec = ''
        # Need a tuple or list to ensure args is unpackable
        # args = tuple(value for value in args if value)
        if len(args) > 0:
            message['args'] = args
            argspec = ', '.join('{!r}'.format(val) for (count, val) in enumerate(args))
        if len(kwargs) > 0:
            message['kwargs'] = kwargs
            kwargspec = ', '.join("{!s}={!r}".format(key, val) for (key, val) in kwargs.items())
        fullspec = argspec
        if argspec and kwargspec: fullspec += ', '
        fullspec += kwargspec
        result = None
        timeout = -1  # Use the ZeroMqClient default
        try:
            self._logger_api.debug("{}({})".format(message['command'], fullspec))
            result = self.zmq_request(message, timeout=timeout)
        except ZeroMqClientReplyEx:
            self._logger_api.warning("RPC Request Timeout")
        except Exception as err:
            self._logger_api.warning("{}: {}".format(type(err).__name__, err))
            self._logger_api.error(traceback.format_exc())
        if result is not None:
            try:
                response = JsonNpB64Encoder.loads(result)
                return response['result'], response['count']
            except ValueError as err:
                self._logger_api.warning("{}: {} for RESULT={}".format(type(err).__name__, err, result))
        return result, 0

    def issueCommand(self, cmd, *args, **kwargs):
        result, __ = self.issueCommandDetailed(cmd, *args, **kwargs)
        return result

    def issueKeepAlive(self):
        message = dict()
        message['keepalive'] = 'keepalive'
        try:
            result = self.zmq_request(message, timeout=500)
            return result
        except ZeroMqClientReplyEx:
            self._logger_api.warning("RPC Request Timeout")
        except Exception as err:
            self._logger_api.warning("{}: {}".format(type(err).__name__, err))
            self._logger_api.error(traceback.format_exc())
        return None

    def issueApiServerCommand(self, cmd, *args, **kwargs):
        message = dict()
        message['api_server_cmd'] = cmd
        argspec = list()
        # Need a tuple or list to ensure args is unpackable
        # args = tuple(value for value in args if value)
        if len(args) > 0:
            message['args'] = args
            argspec.append(args)
        if len(kwargs) > 0:
            message['kwargs'] = kwargs
            argspec.append(kwargs)
        result = None
        timeout = -1  # Use the ZeroMqClient default
        try:
            # self._logger_api.debug("SENDING: {}".format(message))
            self._logger_api.debug("{}({})".format(message['api_server_cmd'], argspec))
            result = self.zmq_request(message, timeout=timeout)
        except ZeroMqClientReplyEx:
            self._logger_api.warning("Request Timeout")
        except Exception as err:
            self._logger_api.warning("{}: {}".format(type(err).__name__, err))
            self._logger_api.error(traceback.format_exc())
        if result is not None:
            try:
                return JsonNpB64Encoder.loads(result)
            except ValueError as err:
                self._logger_api.warning("{}: {} for RESULT={}".format(type(err).__name__, err, result))
                return result
        else:
            return None

    @property
    def api_server_available(self):
        return bool(self._methods)


if __name__ == '__main__':
    # Import for type checking of remote class

    remote_class = ApiClient()
    """:type: ApiTestClass | ApiClient"""
    remote_class.echo_args(10)
    remote_class.nest.nested_get_count()
    remote_class.nest.nested_list[0].other_get_count()
    remote_class.api_generate(True)
    try:
        for delay in range(0, 40, 5):
            output = remote_class.busy_echo(delay, delay)
            print(output)
    except (KeyboardInterrupt, SystemExit):
        print("Exiting")
