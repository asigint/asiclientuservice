#!/usr/bin/env python
# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

import logging
import signal
import sys
import time
import traceback
from datetime import datetime

from colorama import Back

from AsiBroker.ApiServer import ApiServer


def add_args(config):
    from os.path import basename
    from argparse import ArgumentParser
    """Adds commandline arguments and formatted Help"""
    parser = ArgumentParser(description=basename(__file__))
    parser.add_argument('module_name',
                        help="module name to serve".format(config['module_name']),
                        type=str,
                        default=config['module_name'])
    args = parser.parse_args()
    for option in config.keys():
        config[option] = getattr(args, option, config[option])
    return config


def api_server_manager(class_name, port=5555, logger=None):
    # Catch SIGTERM as SystemExit
    signal.signal(signal.SIGTERM, lambda signum, stack_frame: sys.exit(0))

    if logger is None:
        logger = logging.getLogger(name='api_server_manager')

    logger.info(Back.RED + "STARTING:    {}".format(str(datetime.now())) + Back.RESET)
    start = time.time()

    api_server = None
    try:
        api_server = ApiServer(class_name, port=port)
        duration = time.time() - start
        logger.debug("SERVER INIT DURATION={:.2f}s".format(duration))
        api_server.start()
        logger.info("SERVER STARTED: {}".format(str(datetime.now())))
        while api_server.is_alive():
            api_server.join(1)
    except (KeyboardInterrupt, SystemExit):
        logger.info("SERVER ENDING: {}".format(str(datetime.now())))
    except Exception as err:
        logger.critical("{}: {}".format(type(err).__name__, err))
        logger.debug("{}".format(traceback.format_exc()))
    finally:
        if api_server is not None:
            api_server.stop()
            api_server.join(timeout=20)
    logger.info(Back.RED + "EXIT: {}".format(str(datetime.now())) + Back.RESET)


if __name__ == '__main__':
    from pprint import pformat
    from os.path import basename
    import importlib

    application_config = dict(
        module_name='',
    )

    logger = logging.getLogger(name=basename(__file__))
    application_config = add_args(application_config)
    logger.debug('CONFIG:\n' + pformat(application_config))

    try:
        module = importlib.import_module(application_config.module_name, package=None)
    except Exception as err:
        logger.critical('Failed to load module: {}'.format(application_config.module_name))
        sys.exit(0)

    api_server_manager(module)
