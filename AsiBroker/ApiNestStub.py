# -*- coding: utf-8 -*-
# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

from AsiBroker.ApiStub import ApiStub
from AsiBroker.ApiListStub import ApiListStub
import re


class ApiNestStub(object):
    def __init__(self, name, parent, method_list):
        """
        :type name: str
        :type parent: BrokerClient
        :type method_list: dict
        :type index: int
        """
        self.name = name
        self.parent = parent
        self.method_list = method_list
        for key, value in self.method_list.items():
            list_index = re.findall('\[(\d+)\]', key)
            # TODO: seems we don't iterate correctly through this
            if isinstance(value, dict) and list_index:
                list_index = int(list_index[0])
                key_list_name = re.findall('(.*)\[\d+\]', key)[0]
                list_variable = getattr(self, key_list_name, None)
                """:type: list"""
                if list_variable is None:
                    setattr(self, key_list_name, [])
                    list_variable = getattr(self, key_list_name)
                list_len = len(list_variable)
                if list_index >= list_len:
                    list_variable.extend([None for _ in range(list_index - list_len + 1)])
                list_variable[list_index] = ApiListStub(key_list_name, self, value, list_index)
            else:
                setattr(self, key, ApiStub(key, self, value))

    def issueCommand(self, method_name, *args, **kwargs):
        cmd = self.name + '.' + method_name
        if not args and not kwargs:
            # print("Sending message:", cmd)
            result = self.parent.issueCommand(cmd)
        elif not kwargs:
            # print("Sending message:", cmd, args)
            result = self.parent.issueCommand(cmd, *args)
        elif not args:
            result = self.parent.issueCommand(cmd, **kwargs)
            # print("Sending message:", cmd, kwargs)
        else:
            result = self.parent.issueCommand(cmd, *args, **kwargs)
            # print("Sending message:", cmd, args, kwargs)
        return result
