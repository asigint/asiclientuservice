#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

import errno
import logging
import os
import socket
import time
import traceback
from datetime import datetime
from threading import Thread, Event

import zmq


class ZmqServer(Thread):
    def __init__(self, **kwargs):
        log_level = kwargs.pop('log_level', logging.DEBUG)
        self._logger_zmq = logging.getLogger(name='ZeroMqServer')
        self._logger_zmq.setLevel(log_level)
        self._logger_zmq.debug("Initialized: {}".format(str(datetime.now())))
        start = time.time()

        # Thread Setup
        super(ZmqServer, self).__init__()
        self._stop_request = Event()

        # Configuration
        self._defaults = dict(protocol='tcp://', host='*', port=5555,
                              server_name=socket.gethostname() + ':' + str(os.getpid()),
                              req_timeout_retries=3, req_timeout_ms=10000)
        self._config = self._defaults.copy()
        self._server_url = None
        self.zmq_configure(**kwargs)

        # ZMQ Setup
        self._context = zmq.Context(1)
        self._server = None
        """:type: zmq.sugar.socket.Socket | None"""
        self._poll = zmq.Poller()
        duration = time.time() - start
        self._logger_zmq.debug("Ready ({:.2f}s): {}".format(duration, str(datetime.now())))

    def __del__(self):
        self._context.term()
        self._logger_zmq.debug("Shutdown: {}".format(str(datetime.now())))

    def zmq_logging_level(self, level=None):
        if isinstance(level, int) and level in [logging.DEBUG, logging.INFO, logging.WARNING]:
            self._logger_zmq.setLevel(level)
        return self._logger_zmq.getEffectiveLevel()

    def zmq_configure(self, **kwargs):
        if kwargs:
            updates = {k: v for k, v in kwargs.items() if k in self._defaults}
            self._config.update(updates)
        self._server_url = self._config['protocol'] + self._config['host'] + ':' + str(self._config['port'])
        return self._config

    def _connect(self):
        if isinstance(self._server, zmq.sugar.socket.Socket):
            self._disconnect()
        self._server = self._context.socket(zmq.REP)
        try:
            self._server.bind(self._server_url)
        except zmq.ZMQError as err:
            self._logger_zmq.critical("ERROR BINDING {}: {}".format(self._server_url, err))
            self.stop()
            return False
        self._poll.register(self._server, zmq.POLLIN)
        self._logger_zmq.info("BOUND: {}".format(self._server_url))
        return True

    def _disconnect(self):
        if isinstance(self._server, zmq.sugar.socket.Socket):
            self._server.setsockopt(zmq.LINGER, 0)
            self._server.close()
            try:
                self._poll.unregister(self._server)
            except KeyError:
                pass
            self._server = None
        self._logger_zmq.info("Disconnected: {}".format(self._server_url))

    def run(self):
        prior_clients = dict()
        try:
            while self.is_running():
                if self._server is None:
                    self._connect()
                socks = None
                try:
                    socks = dict(self._poll.poll(self._config['req_timeout_ms']))
                except zmq.ZMQError as err:
                    if err.errno != errno.EINTR:
                        raise
                    else:
                        # TODO: Should I disconnect in this condition?
                        self._logger_zmq.warning("{}: {}".format(type(err).__name__, err))
                if self.is_stopping():
                    # This is reached when loop stopped while blocked on _poll (or failed bind)
                    break
                if socks and socks.get(self._server) == zmq.POLLIN:
                    request = self._server.recv_json()
                    """:type: dict"""

                    # Check if this is a repeated request and if so return prior result
                    result = None
                    try:
                        prior_client = prior_clients.pop(request['client_id'], None)
                        if prior_client is not None:
                            prior_result = prior_client.pop(request['request_id'], None)
                            if prior_result is not None:
                                self._logger_zmq.debug(
                                    "REPEATED REQUEST:{} FROM:{}".format(request['request'], request['client_name']))
                                result = prior_result

                        # Perform Action if no prior result
                        if result is None:
                            # Use a method that can be over-ridden by the inheriting class
                            result = self.process_message(request['request'])

                        # Send message and remember result
                        request.update({'result': result})
                        self._server.send_json(request)
                        prior_clients[request['client_id']] = {request['request_id']: result}
                    except KeyError as err:
                        self._logger_zmq.warning("INVALID request JSON. Missing key: {}".format(err))
                        request.update({'result': 'INVALID request JSON. Missing key: {}'.format(err)})
                        self._server.send_json(request)
        except Exception as err:
            self._logger_zmq.critical("{}: {}".format(type(err).__name__, err))
            self._logger_zmq.error("{}".format(traceback.format_exc()))
            # Let the thread end and require a restart to continue
        self._disconnect()
        self._context.term()

    def process_message(self, request):
        """This method should be over-ridden by the inheriting class

        :param request:
        :type request:
        :return: result
        :rtype:
        """
        self._logger_zmq.info("Normal request (%s)" % request)
        result = "RECEIVED: {}".format(request)
        return result

    def stop(self):
        self._stop_request.set()

    def is_stopping(self):
        return self._stop_request.is_set()

    def is_running(self):
        return not self._stop_request.is_set()


def main():
    server = ZmqServer()
    server.start()
    try:
        while server.is_alive():
            server.join(.5)
    except (KeyboardInterrupt, SystemExit):
        print("Exiting")
        pass
    print("Stopping")
    server.stop()
    print("Joining")
    server.join()


if __name__ == "__main__":
    main()
