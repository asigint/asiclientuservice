# -*- coding: utf-8 -*-
# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

from AsiBroker.ApiStub import ApiStub


# noinspection PyPep8Naming
class ApiListStub(object):
    def __init__(self, name, parent, method_list, index=0):
        """
        :type name: str
        :type parent: BrokerClient
        :type method_list: dict
        :type index: int
        """
        self.name = name
        self.parent = parent
        self.method_list = method_list
        self.index = index
        for key, value in self.method_list.items():
            setattr(self, key, ApiStub(key, self, value))

    def issueCommand(self, method_name, *args, **kwargs):
        cmd = self.name + '[' + str(self.index) + '].' + method_name
        if not args and not kwargs:
            # print("Sending message:", cmd)
            result = self.parent.issueCommand(cmd)
        elif not kwargs:
            # print("Sending message:", cmd, args)
            result = self.parent.issueCommand(cmd, *args)
        elif not args:
            result = self.parent.issueCommand(cmd, **kwargs)
            # print("Sending message:", cmd, kwargs)
        else:
            result = self.parent.issueCommand(cmd, *args, **kwargs)
            # print("Sending message:", cmd, args, kwargs)
        return result
