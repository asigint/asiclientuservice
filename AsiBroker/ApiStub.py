# -*- coding: utf-8 -*-
# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved


# noinspection PyPep8Naming
class ApiStub(object):
    def __init__(self, name, parent, arg_list):
        self.name = name
        self.parent = parent
        self.arg_list = arg_list

    def __call__(self, *args, **kwargs):
        result = None
        # if isinstance(args, tuple):
        #     # Need a tuple or list to ensure args is unpackable
        #     args = tuple(value for value in args if value)
        if not (len(args) > 0) and not (len(kwargs) > 0):
            # print("Sending message:", self.name)
            result = self.parent.issueCommand(self.name)
        elif not (len(kwargs) > 0):
            # print("Sending message:", self.name, args)
            result = self.parent.issueCommand(self.name, *args)
        elif not (len(args) > 0):
            result = self.parent.issueCommand(self.name, **kwargs)
            # print("Sending message:", self.name, kwargs)
        else:
            result = self.parent.issueCommand(self.name, *args, **kwargs)
            # print("Sending message:", self.name, args, kwargs)
        return result
