#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

import enum
import inspect
import logging
import os
import re
import sys
import time
import traceback
from datetime import datetime

import numpy

from AsiBroker.ZmqServer import ZmqServer
from AsiUtilities.JsonNpB64Encoder import JsonNpB64Encoder


class ApiServer(ZmqServer):
    LOOP_DELAY = 0.001

    def __init__(self, class_object, **kwargs):
        log_level = kwargs.pop('log_level', logging.DEBUG)
        self._logger_api = logging.getLogger(name='ApiServer')
        self._logger_api.setLevel(log_level)
        self._logger_api.debug("Initialized: {}".format(str(datetime.now())))
        start = time.time()

        super(ApiServer, self).__init__(log_level=logging.INFO, **kwargs)

        try:
            self._class_instance = class_object()
            self._class_instance_lock = getattr(self._class_instance, 'getLock', None)
            self._class_instance_lock_count = getattr(self._class_instance, 'setLockCount', None)
            self._class_instance_lock_command = getattr(self._class_instance, 'setLockCommand', None)
            self._class_instance_lock_result = getattr(self._class_instance, 'setLockResult', None)
            if self._class_instance_lock is None:
                self._logger_api.warning("Class Instance missing getLock() method...potential for unsafe concurrence")
            self._methods = None
            self._class_instance.idle = True
            self._class_instance.paused = False
        except Exception as err:
            self._logger_api.critical("{}: {}".format(type(err).__name__, err))
            for err_data in traceback.extract_tb(sys.exc_info()[2]):
                filename, line_no, function_name, line_text = err_data
                self._logger_api.debug("{}:{} in {}".format(os.path.basename(filename), line_no, function_name))
            exit()
        duration = time.time() - start
        self._logger_api.debug("Ready ({:.2f}s): {}".format(duration, str(datetime.now())))

    def stop(self):
        super(ApiServer, self).stop()
        if getattr(self._class_instance, 'stop', None) is not None:
            self._class_instance.stop()

    def api_server_set_logger_level(self, level=None):
        if isinstance(level, str):
            try:
                level = getattr(logging, level.upper())
            except AttributeError:
                pass
        if isinstance(level, int) and 0 <= level <= 50:
            self._logger_api.setLevel(level)
        return logging.getLevelName(self._logger_api.getEffectiveLevel())

    @staticmethod
    def get_methods_new(class_reference, depth=1):
        # TODO: depth is really to cover up recursion problems
        if depth < 0:
            return {}
        # TODO: does not handle properties well...calls their __get__() method
        # TODO: ...maybe see http://stackoverflow.com/questions/3681272/can-i-get-a-reference-to-a-python-property
        # All public and non-magic attributes
        attr_pub = [(name, obj) for name, obj in inspect.getmembers(class_reference.__class__) if name[0] != '_']
        # Attributes holding references
        attr_reference = [(name, obj) for name, obj in attr_pub
                          if hasattr(obj, '__dict__') and
                          not isinstance(obj, (logging.Logger, enum.EnumMeta, numpy.generic, numpy.matrix))]
        # Simple routines
        attr_routine = [(name, inspect.formatargspec(*inspect.getargspec(obj)))
                        for name, obj in attr_reference if inspect.isroutine(obj)]
        attr_routine_dict = ApiServer._attr_list_to_dict(attr_routine)
        # Class objects
        attr_class = [(name, obj) for name, obj in attr_reference if name not in attr_routine_dict.keys()]
        attr_class_ref = [(class_name, ApiServer.get_methods_new(class_obj, depth - 1))
                          for class_name, class_obj in attr_class]
        attr_routine_dict.update(ApiServer._attr_list_to_dict(attr_class_ref))
        # List of references
        attr_list = [(name, obj) for name, obj in attr_pub if isinstance(obj, list) and len(obj) != 0]
        list_attr_reference = [('{}[{}]'.format(list_name, index), ApiServer.get_methods_new(c_r, depth - 1))
                               for list_name, list_obj in attr_list
                               for c_r in list_obj
                               for index in range(len(list_obj))]
        attr_routine_dict.update(ApiServer._attr_list_to_dict(list_attr_reference))
        return attr_routine_dict

    @staticmethod
    def _attr_list_to_dict(attr_list):
        # Simply args by removing self reference
        return dict([(name, args if isinstance(args, dict) else re.sub('(?<=^\()self(, )?', '', args)) for name, args in
                     attr_list])

    @staticmethod
    def _get_exclude_types():
        import types
        import numpy

        primitive_types = tuple([getattr(types, p_type) for p_type in dir(types) if p_type[0] != '_'])
        numpy_types = (numpy.ndarray, numpy.generic)
        exclude_types = primitive_types + numpy_types
        return exclude_types

    def process_message(self, request):
        """This method overrides the ProxyServer method

        :param request:
        :type request: dict
        :return:
        :rtype: str
        """
        result = ''
        count = 0
        if 'mode' in request:
            if request['mode'] == 'auto_update':
                self._class_instance.auto_update = True
                result = 'SET MODE auto_update'
            else:
                self._class_instance.auto_update = False
                result = 'SET MODE interactive'
        if 'command' in request or 'api_server_cmd' in request:
            # Use lock if available
            if self._class_instance_lock is not None:
                with self._class_instance_lock():
                    if self._class_instance_lock_count:
                        count = self._class_instance_lock_count()
                    result = self.process_command(request)
            else:
                result = self.process_command(request)
        # TODO: This is a hack!
        if 'keepalive' in request:
            result = True
        try:
            return JsonNpB64Encoder.dumps({'result': result, 'count': count})
        except Exception as err:
            err_msg = "{}: {}".format(type(err).__name__, err)
            self._logger_api.warning(err_msg)
            return err_msg

    def process_command(self, body):
        start = time.time()
        args = None
        kwargs = None
        argspec = ''
        kwargspec = ''
        proxy_method = None
        result = None
        if 'args' in body:
            args = body['args']
            # argspec = ', '.join('{!r}'.format(val) for (count, val) in enumerate(args))
            argspec = '{!r}'.format(args)  # This is better than above but untested
        if 'kwargs' in body:
            kwargs = body['kwargs']
            kwargspec = ', '.join("{!s}={!r}".format(key, val) for (key, val) in kwargs.items())
        fullspec = argspec
        if argspec and kwargspec: fullspec += ', '
        fullspec += kwargspec
        if 'api_server_cmd' in body:
            try:
                # TODO: Ugly hack here
                body['command'] = body['api_server_cmd']
                proxy_method = getattr(self, body['api_server_cmd'])
            except Exception as err:
                result = self._display_error_and_stack(body['command'], fullspec, err,
                                                       traceback.extract_tb(sys.exc_info()[2]))
        elif body['command'].startswith('api_server_'):
            try:
                proxy_method = getattr(self, body['command'])
            except Exception as err:
                result = self._display_error_and_stack(body['command'], fullspec, err,
                                                       traceback.extract_tb(sys.exc_info()[2]))
        elif body['command'] == 'getMethods':
            if self._methods is None:
                self._logger_api.debug("Methods Gen.:  {}".format(str(datetime.now())))
                start = time.time()
                self._methods = ApiServer.get_methods_new(self._class_instance)
                duration = time.time() - start
                self._logger_api.debug(
                    "Methods-{} ({:.2f}s): {}".format(len(self._methods), duration, str(datetime.now())))
            result = self._methods
        elif re.findall('\[(\d+)\]', body['command']):
            self._logger_api.debug("FOUND LIST OF METHODS")
            parts = re.match('(.*)\[(\d+)\]\.(.*)', body['command'])
            index = int(parts.group(2))
            try:
                proxy_list = getattr(self._class_instance, parts.group(1))
                proxy_method = getattr(proxy_list[index], parts.group(3))
            except Exception as err:
                result = self._display_error_and_stack(body['command'], fullspec, err,
                                                       traceback.extract_tb(sys.exc_info()[2]))
        elif '.' in body['command']:
            # self._logger_api.debug("FOUND NESTED METHOD")
            try:
                instance = self._class_instance
                for obj in body['command'].split('.'):
                    proxy_method = getattr(instance, obj)
                    instance = proxy_method
            except Exception as err:
                result = self._display_error_and_stack(body['command'], fullspec, err,
                                                       traceback.extract_tb(sys.exc_info()[2]))
        else:
            try:
                proxy_method = getattr(self._class_instance, body['command'])
            except Exception as err:
                result = self._display_error_and_stack(body['command'], fullspec, err,
                                                       traceback.extract_tb(sys.exc_info()[2]))
        # self._logger_api.debug("[{:.2f}s] {}({})".format(time.time() - start, body['command'], fullspec))
        if proxy_method:
            try:
                if isinstance(args, tuple):
                    args = tuple(value for value in args if value)
                if args and kwargs:
                    result = proxy_method(*args, **kwargs)
                elif args:
                    result = proxy_method(*args)
                elif kwargs:
                    result = proxy_method(**kwargs)
                else:
                    result = proxy_method()
            except Exception as err:
                result = self._display_error_and_stack(body['command'], fullspec, err,
                                                       traceback.extract_tb(sys.exc_info()[2]))
        duration = time.time() - start
        self._logger_api.debug("[{:.2f}s] {}({})={}".format(duration, body['command'], fullspec, result))
        if self._class_instance_lock_command:
            self._class_instance_lock_command("{}({})".format(body['command'], fullspec))
        if self._class_instance_lock_result:
            self._class_instance_lock_result(result)
        return result

    def class_instance_start(self):
        try:
            self._class_instance.start()
        except AttributeError:
            pass

    def class_instance_stop(self):
        try:
            self._class_instance.stop()
        except AttributeError:
            pass

    def class_instance_is_alive(self):
        try:
            return self._class_instance.is_alive()
        except AttributeError:
            return None

    def class_instance_is_running(self):
        try:
            return self._class_instance.is_running()
        except AttributeError:
            return None

    def class_instance_is_stopping(self):
        try:
            return not self._class_instance.is_running()
        except AttributeError:
            return None

    def _display_error_and_stack(self, body_cmd, fullspec, err, tb):
        result = "{}({})<-{}:{}".format(body_cmd, fullspec, type(err).__name__, err)
        self._logger_api.warning(result)
        for item in tb:
            filename, line_no, function_name, line = item
            error_msg = "{}:{}:{}".format(os.path.basename(filename), function_name, line_no)
            self._logger_api.error(error_msg)
        self._logger_api.error("{}: {}".format(type(err).__name__, err))
        return result


class ApiTestBase(object):
    status = True

    def inherited_method(self):
        return self.status

    @staticmethod
    def inherited_static_echo(value):
        return value


class ApiTestClass(ApiTestBase):
    def __init__(self):
        self.count = 0
        self.nest = ApiTestNest()
        self.list_m = list([ApiTestNest(), ApiTestOther()])
        self.dict_m = dict(first=ApiTestNest())
        self._private = None
        self._my_property = None
        self._logging_level = None

    def _private_method(self):
        return self._private

    @property
    def my_property(self):
        print("RETURNING PROPERTY")
        return self._my_property

    @my_property.setter
    def my_property(self, value):
        self._my_property = value

    def busy_echo(self, message, delay):
        self.count += 1
        time.sleep(delay)
        return message

    def get_count(self):
        return self.count

    def echo_args(self, *args, **kwargs):
        print(args)
        print(kwargs)
        return args, kwargs

    def api_logging_level(self, value):
        self._logging_level = value
        return self._logging_level


class ApiTestNest(object):
    def __init__(self):
        self.count = 0
        self.nested_list = list([ApiTestOther()])

    def nested_echo(self, message, delay):
        self.count += 1
        time.sleep(delay)
        return message

    def nested_get_count(self):
        return self.count


class ApiTestOther(object):
    def __init__(self):
        self.count = 0

    def other_echo(self, message, delay):
        self.count += 1
        time.sleep(delay)
        return message

    def other_get_count(self):
        return self.count


def main():
    server = ApiServer(ApiTestClass)
    server.start()
    try:
        while server.is_alive():
            server.join(.5)
    except (KeyboardInterrupt, SystemExit):
        print("Exiting")
        pass
    print("Stopping")
    server.stop()
    print("Joining")
    server.join()


if __name__ == "__main__":
    main()
