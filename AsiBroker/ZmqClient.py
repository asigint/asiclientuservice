#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

import logging
import os
import socket
import time
import uuid
from datetime import datetime

import zmq


class ZmqClient(object):
    def __init__(self, **kwargs):
        """

        :param kwargs:
        :keyword protocol: default='tcp://'
        :keyword host: default='localhost'
        :keyword port: default=5555
        :keyword client_name: default=hostname:pid
        :keyword req_timeout_retries: default=3
        :keyword req_timeout_ms: default=10000
        :keyword log_level: default=logging.DEBUG (10)
        """
        log_level = kwargs.pop('log_level', logging.DEBUG)
        self._logger_zmq = logging.getLogger(name='ZeroMqClient')
        self._logger_zmq.setLevel(log_level)
        self._logger_zmq.debug("Initialized: {}".format(str(datetime.now())))

        # Configuration
        self._defaults = dict(protocol='tcp://', host='localhost', port=5555,
                              client_name=socket.gethostname() + ':' + str(os.getpid()),
                              req_timeout_retries=3, req_timeout_ms=10000)
        self._config = self._defaults.copy()
        self._server_url = None
        self._request_dict = None
        """:type: dict | None"""
        self._client_id = uuid.uuid4().hex
        self.zmq_configure(**kwargs)

        # ZMQ Setup
        self._context = zmq.Context(1)
        self._client = None
        """:type: zmq.sugar.socket.Socket | None"""
        self._poll = zmq.Poller()

    def __del__(self):
        self._disconnect()
        self._context.term()

    def zmq_logging_level(self, level=None):
        if isinstance(level, int) and level in [logging.DEBUG, logging.INFO, logging.WARNING]:
            self._logger_zmq.setLevel(level)
        return self._logger_zmq.getEffectiveLevel()

    def zmq_configure(self, **kwargs):
        if kwargs:
            updates = {k: v for k, v in kwargs.items() if k in self._defaults}
            self._config.update(updates)
        self._server_url = self._config['protocol'] + self._config['host'] + ':' + str(self._config['port'])
        self._request_dict = {'client_id': self._client_id,
                             'client_name': self._config['client_name'],
                             'request_id': None,
                             'request': None,
                              }
        return self._config

    def _connect(self):
        if isinstance(self._client, zmq.sugar.socket.Socket):
            self._disconnect()
        self._client = self._context.socket(zmq.REQ)
        self._client.connect(self._server_url)
        self._poll.register(self._client, zmq.POLLIN)

    def _disconnect(self):
        if isinstance(self._client, zmq.sugar.socket.Socket):
            self._client.setsockopt(zmq.LINGER, 0)
            self._client.close()
            self._poll.unregister(self._client)
            self._client = None

    def zmq_request(self, request, timeout=-1):
        """

        :param request: Typically a JSON string for the server to process
        :param timeout: Timeout in milliseconds. None=Infinite, <0 will use class default
        :type timeout: int | float | None
        :return:
        """
        # Use configured timeout if not specified
        if timeout < 0:
            timeout = self._config['req_timeout_ms']
        if self._client is None:
            self._connect()
        if request is not None:
            request_id = uuid.uuid4().hex
            self._request_dict.update({'request_id': request_id,
                                      'request': request})
            self._logger_zmq.debug("SENDING: {}".format(self._request_dict))

            retries_left = self._config['req_timeout_retries']
            zmq_retries_left = 2
            while True:
                try:
                    self._client.send_json(self._request_dict)
                except zmq.ZMQError as err:
                    zmq_retries_left -= 1
                    if zmq_retries_left == 0:
                        self._logger_zmq.error("FATAL ZMQ FAILURE")
                        break
                    self._logger_zmq.warning("ZMQError: {} - RETRYING".format(err))
                    self._disconnect()
                    self._connect()
                    continue
                socks = dict(self._poll.poll(timeout))
                if socks.get(self._client) == zmq.POLLIN:
                    reply = self._client.recv_json()
                    if not reply or not isinstance(reply, dict):
                        self._logger_zmq.warning("UNEXPECTED REPLY TYPE: {}".format(reply))
                        break
                    if reply['request_id'] == request_id:
                        self._logger_zmq.debug("RESULT: {}".format(reply['result']))
                        return reply['result']
                    else:
                        self._logger_zmq.warning("UNEXPECTED REPLY: {}".format(reply))
                else:
                    self._logger_zmq.warning("RESPONSE TIMEOUT {}".format(self._server_url))
                    # Socket is confused. Close and remove it.
                    self._disconnect()
                    retries_left -= 1
                    if retries_left == 0:
                        self._logger_zmq.error("RETRY LIMIT REACHED {}".format(self._server_url))
                        break
                    self._logger_zmq.debug("RECONNECT & RETRY ID: {}".format(request_id))
                    # Create new connection
                    self._connect()
            raise ZeroMqClientReplyEx("ZeroMqClient Failed To Receive Reply")


class ZeroMqClientReplyEx(Exception):
    """Reply Failure exception since valid reply results include None"""


def main():
    try:
        client = ZmqClient()
        count = 0
        while True:
            print("Putting: {}".format(count))
            try:
                client.zmq_request(count)
            except ZeroMqClientReplyEx as err:
                print("{}".format(err))
            count += 1
            time.sleep(.5)
    except (KeyboardInterrupt, SystemExit):
        print("Exiting")
        pass


if __name__ == "__main__":
    main()
