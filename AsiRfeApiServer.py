#!/usr/bin/env python
# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

from AsiPhaseCalBoard.AsiPhaseCalBoardServer import AsiPhaseCalBoardServer
from time import sleep


# noinspection PyPep8Naming
class AsiRfeApiServer(AsiPhaseCalBoardServer):

    def __init__(self):
        super().__init__()


if __name__ == '__main__':
    # Main processing runs when this module is run from the command line
    sleep(30)
    s = AsiRfeApiServer()
    s.main(port=5556)
