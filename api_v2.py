from flask import Blueprint
from flask_restplus import Api
from apis.phase_tone import api as pc1

bp_api_v2 = Blueprint('api', __name__, url_prefix='/api/v2')
api = Api(bp_api_v2,
          title='ASI API',
          version='2.0',
          description='SDR Control Interface',
          doc='/doc/',
          # All API metadatas
          )

api.add_namespace(pc1)

