#!/usr/bin/python3
import socket
import sys

if __name__ == '__main__':
    from colorama import Fore
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('host', help="hostname/address", nargs='?', default='')
    parser.add_argument('port', help="part", nargs='?', type=int, default=5002)
    args = parser.parse_args()

    UDP_IP = args.host
    UDP_PORT = args.port
    BUFFER_SIZE = 4096

    try:
        sock = socket.socket(socket.AF_INET,  # Internet
                             socket.SOCK_DGRAM)  # UDP
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
        if hasattr(socket, 'SO_REUSEPORT'):
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, True)
        sock.bind((UDP_IP, UDP_PORT))
    except Exception as err:
        print("{}: {}".format(type(err).__name__, err))
        print("{}:{}".format(UDP_IP, UDP_PORT))
        sys.exit(-1)

    keys = ['Freq', 'Signal ID', 'Type of Tx', 'SG1400 ID', 'Lat/long', 'Date/time', 'SNR', 'Color Code', 'Source ID', 'SG1400']
    try:
        while True:
            data, address = sock.recvfrom(BUFFER_SIZE)
            # print("{}{}:{}\n{}".format(Fore.RED, address, Fore.RESET, data.decode('utf-8')))
            fields = data.decode('utf-8').split('::')
            rx_dict = dict(zip(keys, fields))
            print(rx_dict)
    except (KeyboardInterrupt, SystemExit):
        sock.close()
