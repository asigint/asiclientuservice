#!/usr/bin/python3
# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

import logging
import netifaces
import socket
import sys
from time import sleep

from zeroconf import ServiceInfo, Zeroconf


class ZeroConfManger(object):
    def __init__(self):
        self.zeroconf = Zeroconf()
        self.service_info = dict()
        self.info = None

    def register_service(self, service_type=None, model_name="", service_name=None,
                         address=None, port=None,
                         weight=0, priority=0,
                         properties=None, server=None):
        """

        :param service_type:
        :param model_name:
        :param service_name:
        :param address:
        :param port:
        :param weight:
        :param priority:
        :param properties:
        :param server:
        :return:
        """

        if service_type is None:
            service_type = '_http._tcp.local.'

        if model_name:
            model_name = model_name.strip() + ' '

        if service_name is None:
            service_name = model_name + socket.gethostname() + '.' + service_type

        if server is None:
            server = socket.gethostname() + '.local'

        if address is None:
            address = socket.inet_aton(self.get_default_iface())
        elif isinstance(address, str):
            address = socket.inet_aton(address)

        if port is None:
            port = 80

        if properties is None:
            properties = dict(path='/')

        self.service_info = dict(
            type_=service_type,
            name=service_name,
            port=port,
            weight=weight,
            priority=priority,
            properties=properties,
            server=server
        )

        self.info = ServiceInfo(**self.service_info)
        self.zeroconf.register_service(self.info)

    def get_service_info_human(self):
        human = self.service_info.copy()
        human['address'] = socket.inet_ntoa(self.service_info['address'])
        return human

    def unregister_service(self):
        if self.info is not None:
            self.zeroconf.unregister_service(self.info)
            self.info = None

    def close(self):
        self.zeroconf.close()

    @staticmethod
    def get_default_iface():
        active_interface_ip = \
            netifaces.ifaddresses(netifaces.gateways()['default'][netifaces.AF_INET][1])[netifaces.AF_INET][0]['addr']
        return active_interface_ip


def main():
    logging.basicConfig(level=logging.DEBUG)
    if len(sys.argv) > 1:
        assert sys.argv[1:] == ['--debug']
        logging.getLogger('zeroconf').setLevel(logging.DEBUG)
    zeroconf = ZeroConfManger()
    print("Registration of a service, press Ctrl-C to exit...")
    zeroconf.register_service(model_name='ASI2020DF', port=8080)
    # print("Registered: {}".format(zeroconf.get_service_info_human()))
    try:
        while True:
            sleep(0.1)
    except KeyboardInterrupt:
        pass
    finally:
        print("Unregistering...")
        zeroconf.unregister_service()
        zeroconf.close()


if __name__ == '__main__':
    main()
