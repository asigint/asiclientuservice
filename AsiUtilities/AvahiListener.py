#!/usr/bin/python3
# See: https://github.com/lathiat/avahi/blob/master/avahi-python/avahi-discover/avahi-discover.py


import dbus
from dbus import DBusException
from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GObject as gobject

import avahi_util

TYPE = "_http._tcp"


def service_resolved(interface, protocol, name, stype, domain, host, aprotocol, address, port, txt, flags):
    print("Service data for service '%s' of type '%s' in domain '%s' on %i.%i:" % (
        name, stype, domain, interface, protocol))
    print("\tHost %s (%s), port %i, TXT data: %s" % (
        host, address, port, str(avahi_util.txt_array_to_string_array(txt))))


def print_error(err):
    print('ERROR: ')
    print(err)


def new_service(interface, protocol, name, stype, domain, flags):
    print("Found service '%s' type '%s' domain '%s' on %i.%i." % (name, stype, domain, interface, protocol))

    if flags & avahi_util.LOOKUP_RESULT_LOCAL:
        # local service, skip
        pass

    server.ResolveService(interface, protocol, name, stype,
                          domain, avahi_util.PROTO_UNSPEC, dbus.UInt32(0),
                          reply_handler=service_resolved, error_handler=print_error)


def remove_service(interface, protocol, name, stype, domain, flags):
    print("Service '%s' of type '%s' in domain '%s' on %i.%i disappeared." % (name, stype, domain, interface, protocol))


loop = DBusGMainLoop()

bus = dbus.SystemBus(mainloop=loop)

server = dbus.Interface(bus.get_object(avahi_util.DBUS_NAME, avahi_util.DBUS_PATH_SERVER),
                        avahi_util.DBUS_INTERFACE_SERVER)
browser_path = server.ServiceBrowserNew(avahi_util.IF_UNSPEC,
                                        avahi_util.PROTO_UNSPEC,
                                        TYPE,
                                        '',  # Domain, default to .local
                                        dbus.UInt32(0),  # Flags
                                        )
sbrowser = dbus.Interface(bus.get_object(avahi_util.DBUS_NAME, browser_path),
                          avahi_util.DBUS_INTERFACE_SERVICE_BROWSER)

sbrowser.connect_to_signal("ItemNew", new_service)
sbrowser.connect_to_signal("ItemRemove", remove_service)

try:
    gobject.MainLoop().run()
except (KeyboardInterrupt, SystemExit):
    gobject.MainLoop().quit()
