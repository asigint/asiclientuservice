import matplotlib.pyplot as plt
try:
    plt.style.use('timsStyle')
except OSError:
    pass


class LiveDataDisplay(object):

    def __init__(self, x_range, y_range, x_label='', y_label='', plt_title=''):

        self.fig, self.ax = plt.subplots(num=plt_title)

        self.line = None

        self.set_x_axis(x_range)
        self.set_y_axis(y_range)

        self.set_x_label(x_label)
        self.set_y_label(y_label)

        plt.title(plt_title)

        self.x_data = []
        self.y_data = []
        self.fig.show()

    def add_point(self, x, y):

        self.x_data.append(x)
        self.y_data.append(y)

        if self.line is None:
            self.line, = self.ax.plot(self.x_data, self.y_data, '*')
        else:
            self.line.set_data(self.x_data, self.y_data)
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def clear_data(self):

        self.x_data = []
        self.y_data = []

    def set_x_label(self, x_label):

        self.ax.set_xlabel(x_label)

    def set_y_label(self, y_label):

        self.ax.set_ylabel(y_label)

    def set_x_axis(self, x_range):

        self.ax.set_xlim(x_range[0], x_range[1])

    def set_y_axis(self, y_range):

        self.ax.set_ylim(y_range[0], y_range[1])

    def save_fig(self, filename):
        self.fig.savefig(filename, format="png")
