#!/usr/bin/env python
# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

import json
import logging
import select
import socket
import threading
from datetime import datetime
from pprint import pformat

import colorama

from AsiUtilities.netifaces_macros import get_primary_addresses
from AsiUtilities.socket_tools import create_multicast_rx, add_multicast_if


class MulticastRx(threading.Thread):
    def __init__(self,
                 mcast_addr='239.0.0.1', mcast_port=11000,
                 if_addr=None,
                 ip_show=None, ip_hide=None):
        self.logger = logging.getLogger(name=self.__class__.__name__)
        super(MulticastRx, self).__init__()
        self._stop_request = threading.Event()
        self.select_timeout = 1  # Select timeout in seconds
        self.socket_max_rx_buffer = 65536  # MAXBUFFER=65536
        self.mcast_addr = mcast_addr
        self.mcast_port = mcast_port
        self.mcast_sock = None
        self.mcast_if_ips = set()
        self.if_addr = if_addr
        self.warn_no_socket = True
        if ip_show is None:
            self.ip_show = []
        else:
            self.ip_show = ip_show
        if not isinstance(self.ip_show, list):
            self.ip_show = [self.ip_show]
        if ip_hide is None:
            self.ip_hide = []
        else:
            self.ip_hide = ip_hide
        if not isinstance(self.ip_hide, list):
            self.ip_hide = [self.ip_hide]

    def run(self):
        while not self.is_stopped():
            if self.mcast_sock is None:
                self.mcast_sock = create_multicast_rx(self.mcast_addr, self.mcast_port, suppress_errors=True)
                if self.mcast_sock is None:
                    if self.warn_no_socket:
                        logging.error('FAILED TO CREATE SOCKET: {}:{}'.format(self.mcast_addr, self.mcast_port))
                        self.warn_no_socket = False
                else:
                    self.warn_no_socket = True
                continue

            self.manage_multicast_membership()

            # Check status of sockets
            outputs = []
            inputs = [self.mcast_sock]
            readable, writable, exceptional = select.select(inputs, outputs, inputs, self.select_timeout)

            # Check timeout condition (aka nothing to do)
            if not (readable or writable or exceptional):
                continue

            # Handle inputs
            for active_socket in readable:
                assert isinstance(active_socket, socket.SocketType)
                active_socket_name = active_socket.getsockname()
                message, address = active_socket.recvfrom(self.socket_max_rx_buffer)
                if (self.ip_show and address[0] in self.ip_show) \
                        or (not self.ip_show and address[0] not in self.ip_hide):
                    self.process_data(message, address, active_socket_name)

            for exception_socket in exceptional:
                assert isinstance(exception_socket, socket.SocketType)
                exception_socket_name = exception_socket.getsockname()
                logging.warning('Socket Exception: {}'.format(exception_socket_name))
                exception_socket.close()
                # noinspection PyUnusedLocal
                exception_socket = None  # This should null self.mcast_sock, but untested

    def manage_multicast_membership(self):
        current_if_ips = get_primary_addresses()
        added_if_ips = current_if_ips - self.mcast_if_ips
        # removed_if_ips = self.mcast_if_ips - current_if_ips

        for new_ip in added_if_ips:
            self.mcast_sock = add_multicast_if(self.mcast_sock, self.mcast_addr, new_ip)
            # Drop does not sufficiently consider/handle multiple address on one interface
            # for old_ip in removed_if_ips:
            #     self.mcast_sock = drop_multicast_if(self.mcast_sock, self.mcast_addr, old_ip)

    def process_data(self, message, address=None, local_address=None):
        print("{}{}{}[{}]<-{}{}".format(colorama.Fore.RED,
                                        datetime.now(), pformat(address), len(message), pformat(local_address),
                                        colorama.Fore.RESET))
        print("{}{}{}".format(colorama.Fore.GREEN, pformat(message.decode('utf-8')), colorama.Fore.RESET))

    def stop(self):
        self._stop_request.set()

    def is_stopped(self):
        return self._stop_request.is_set()


class DfStatusRx(MulticastRx):
    def __init__(self, app_config):
        import time
        self.application_config = app_config
        super(DfStatusRx, self).__init__(mcast_addr=app_config['ip_addr'],
                                         mcast_port=app_config['ip_port'],
                                         ip_show=app_config['ip_show'],
                                         ip_hide=app_config['ip_hide'],
                                         if_addr=app_config['if_addr'])
        self.last_ts = time.time()

    def process_data(self, message, address=None, local_address=None):
        display = True

        if self.application_config['ip_show']:
            display = True if address[0] in self.application_config['ip_show'] else False
        elif self.application_config['ip_hide']:
            display = True if address[0] not in self.application_config['ip_hide'] else False
        # ts = json.loads(message.decode())['GEOTAG']['bearing']['time_sys']
        # delta = ts - self.last_ts
        # if delta > .5:
        #     print("{}{}{}".format(colorama.Fore.RED, delta, colorama.Fore.RESET))
        # else:
        #     print(delta)
        # self.last_ts = ts
        # display = False
        if display:
            print("{}{}{}[{}]<-{}{}".format(colorama.Fore.RED,
                                            datetime.now(), pformat(address), len(message), pformat(local_address),
                                            colorama.Fore.RESET))
            if not application_config['info_only']:
                try:
                    msg = json.loads(message.decode('utf-8'))
                    if application_config['keys_only']:
                        print("{}{}{}".format(colorama.Fore.GREEN, pformat(msg.keys()), colorama.Fore.RESET))
                    elif application_config['geotag_only']:
                        print("{}{}{}".format(colorama.Fore.GREEN, pformat(msg['GEOTAG']), colorama.Fore.RESET))
                    elif application_config['status_only']:
                        print("{}{}{}".format(colorama.Fore.GREEN, pformat(msg['STATUS']), colorama.Fore.RESET))
                    else:
                        print("{}{}{}".format(colorama.Fore.GREEN, pformat(msg), colorama.Fore.RESET))
                except ValueError:
                    print("FAILED TO PARSE AS JSON")
                    print(message.decode('utf-8'))
                    self.stop()


def add_args(config):
    from argparse import ArgumentParser
    import os
    """Adds commandline arguments and formatted Help"""
    parser = ArgumentParser(description=os.path.basename(__file__))
    parser.add_argument('--ip_port',
                        help="IP Port (default={})".format(config['ip_port']),
                        type=int,
                        default=config['ip_port'])
    parser.add_argument('--ip_addr',
                        help="IP Address (default={})".format(config['ip_addr']),
                        type=str,
                        default=config['ip_addr'])
    parser.add_argument('--if_addr',
                        help="IF Address (default={})".format(config['if_addr']),
                        type=str,
                        default=config['if_addr'])
    parser.add_argument('--ip_hide',
                        nargs='+',
                        help="Source IP address to hide",
                        type=str,
                        default=config['ip_hide'])
    parser.add_argument('--ip_show',
                        nargs='+',
                        help="Source IP address to show",
                        type=str,
                        default=config['ip_show'])
    parser.add_argument('--raw',
                        help="Show Raw Data",
                        action='store_true',
                        default=config['raw'])
    parser.add_argument('--info_only',
                        help="Show Message Info Only",
                        action='store_true',
                        default=config['info_only'])
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--keys_only',
                       help="Show KEYS only",
                       action='store_true',
                       default=config['keys_only'])
    group.add_argument('--geotag_only',
                       help="Show GEOTAG only",
                       action='store_true',
                       default=config['geotag_only'])
    group.add_argument('--status_only',
                       help="Show STATUS only",
                       action='store_true',
                       default=config['status_only'])
    args = parser.parse_args()
    for option in config.keys():
        config[option] = getattr(args, option, config[option])
    return config


if __name__ == '__main__':
    import signal
    import sys
    from os import environ
    from os.path import basename
    from setproctitle import setproctitle

    from AsiUtilities import log

    setproctitle(basename(__file__))
    # Catch SIGTERM as SystemExit
    signal.signal(signal.SIGTERM, lambda signum, stack_frame: sys.exit(0))

    # Needed for Pycharm support
    if 'PYCHARM_HOSTED' in environ:
        convert = False  # in PyCharm, we should disable convert
        strip = False
    else:
        convert = None
        strip = None
    # Needed for Windows & Pycharm support
    colorama.init(convert=convert, strip=strip)

    log.init_log('DEBUG', force_color=True)
    logger = logging.getLogger(name=basename(__file__))

    application_config = dict(
        ip_addr='239.0.0.1',
        ip_port=11000,
        if_addr=None,
        ip_show=[],
        ip_hide=[],
        raw=False,
        geotag_only=False,
        status_only=False,
        info_only=False,
        keys_only=False,
    )

    application_config = add_args(application_config)
    print(application_config)

    if application_config['raw']:
        status_thread = MulticastRx(application_config['ip_addr'], application_config['ip_port'],
                                    ip_show=application_config['ip_show'],
                                    ip_hide=application_config['ip_hide'])
    else:
        status_thread = DfStatusRx(application_config)
    status_thread.start()

    try:
        while status_thread.is_alive():
            status_thread.join(1)

    except (KeyboardInterrupt, SystemExit):
        status_thread.stop()
        status_thread.join(5)
