# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved
import math


def bearing_mod(a, b, precision=None):
    """
    Perform modulus math (a % b) ensuring positive values.
    Allow for a fixed precision.
    Formula -> (a + b) % b

    :param a:
    :type a: float | int
    :param b:
    :type b: float | int
    :param precision: number of decimal places to retain (0=return int)
    :type precision: int
    :return:
    :rtype: float | int
    """

    # Python docs for mod math say math.fmod for floats and % for ints
    # yet python % seems to only give positive numbers...
    if isinstance(a, int) and isinstance(b, int):
        bearing = (a + b) % b
    else:
        bearing = math.fmod(a + b, b)
    if isinstance(precision, int) and 0 <= precision <= 10:
        if precision == 0:
            # modulo the int to ensure it doesn't round up to b
            return int(round(bearing)) % b
        else:
            return float('{0:.{1}f}'.format(bearing, precision))
    else:
        return bearing
