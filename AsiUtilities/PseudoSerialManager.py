import os
import logging


class PseudoSerialManager(object):
    def __init__(self, logger=None):
        if logger is None:
            logger = logging.getLogger(name=self.__class__.__name__)
        self.logger = logger

        self.master_id = None
        self.slave_id = None
        self.master_port_name = None
        self.slave_port_name = None
        self.open_pty()

    def open_pty(self):
        # Create Virtual Serial Port
        self.master_id, self.slave_id = os.openpty()
        self.master_port_name = os.ttyname(self.master_id)
        self.slave_port_name = os.ttyname(self.slave_id)

    def close_pty(self):
        os.close(self.master_id)
        os.close(self.slave_id)
        self.master_id = None
        self.slave_id = None
        self.master_port_name = None
        self.slave_port_name = None
