from scipy.signal import fftconvolve
import numpy as np
import xmltodict
from AsiUtilities.time_convert import datetime_utc_from_epoch
import logging
import os
import glob


class DetectAIS(object):

    slot_time = 60 / 2250
    default_threshold = 15.85
    default_channel = 'A'
    channel_a = 161.975e6
    channel_b = 162.025e6
    path_to_ais = '/var/redhawk/sdr/dom/data'

    def __init__(self, sample_rate):
        """
        Initialization for AIS processing code
        :param sample_rate: sampling rate of the data that will be processed
        """

        self.logger_ais = logging.getLogger(name='AIS')
        self.fs = sample_rate

        self.slot_samples = int(self.slot_time * self.fs)

        self.signal_filter = np.ones(self.slot_samples) / self.slot_samples

        self.noise_filter = np.zeros(5 * self.slot_samples)
        self.noise_filter[:self.slot_samples] = np.ones(self.slot_samples)
        self.noise_filter[-self.slot_samples:] = np.ones(self.slot_samples)
        self.noise_filter /= (2 * self.slot_samples)
        self.ais_data = []
        self.detection_threshold = self.default_threshold
        self.ais_channel = self.default_channel

    def set_detection_threshold(self, value):
        self.detection_threshold = value
        return self.detection_threshold

    def get_detection_threshold(self):
        return self.detection_threshold

    def set_ais_channel(self, channel):
        """
        Set the AIS channel.
        :param channel: AIS channel. Permissible values are A or B
        :type channel: str
        :return: The set value of ais_channel
        """
        if channel.upper() in ['A', 'B']:
            self.ais_channel = channel.upper()
        else:
            self.logger_ais.warning("Only permissible channel values are 'A' and 'B'")
        return self.ais_channel

    def get_ais_channel(self):
        return self.ais_channel

    def load_ais_log(self):

        fn = os.path.join(self.path_to_ais, '.ais_messages*')
        list_of_files = glob.glob(fn)
        rv = False
        if len(list_of_files) > 0:
            latest_file = max(list_of_files, key=os.path.getctime)

            rv = True
            times = []
            channels = []
            self.ais_data = []
            try:
                with open(latest_file, 'r') as fd:
                    for line in fd.readlines():
                        try:
                            self.ais_data.append(xmltodict.parse(line))
                        except xmltodict.expat.ExpatError:
                            pass

            except IOError:
                self.logger_ais.warning("Invalid AIS file: {}".format(file_name))
                rv = False
        else:
            self.logger_ais.warning("No AIS logs found")

        return rv

    def load_ais_data(self, ais_data=None):

        if ais_data is not None:
            self.ais_data = ais_data

        times = [float(item['aisEvent']['wsec']) + float(item['aisEvent']['fsec']) for item in self.ais_data]
        times = np.array(times)
        channels = [item['aisEvent']['channelID'] for item in self.ais_data]
        channels = np.array(channels)
        return times, channels

    def find_ais_in_data(self, data):

        data2 = abs(data) ** 2
        signal = fftconvolve(data2, self.signal_filter, 'same')
        noise = fftconvolve(data2, self.noise_filter, 'same')

        ratio = signal / noise

        index = np.where(ratio > self.detection_threshold)[0]
        index_length = len(index)

        indices = []
        if index_length > 0:
            d_index = np.diff(index)

            id2 = np.where(d_index > 1)[0]

            half_samples = int((2 * self.slot_samples) // 5)

            start = 0
            for m in range(len(id2)):
                center = (start + id2[m]) // 2
                length = id2[m] - start + 1
                if length >= 2 * half_samples + 1:
                    hs = half_samples
                else:
                    hs = int((length - 2) // 2)
                indices.append(index[center - hs: center + hs])
                start = id2[m]
            center = (start + index_length) // 2
            length = index_length - start + 1
            if length >= 2 * half_samples + 1:
                hs = half_samples
            else:
                hs = int((length - 2) // 2)
            indices.append(index[center - hs: center + hs])

        return indices
