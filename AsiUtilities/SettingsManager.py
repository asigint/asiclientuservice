#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2017 Applied Signals Intelligence, Inc. -- All Rights Reserved

import logging
import os
import sys
import time
import traceback
from copy import deepcopy
from datetime import datetime
from pprint import pformat

import yaml

from AsiUtilities.EnhancedValidator import EnhancedValidator


class ExplicitDumper(yaml.Dumper):
    """
    A dumper that will never emit aliases.
    """

    def ignore_aliases(self, data):
        return True


def validate_file_read(filename=None):
    """
    Test file exists and is readable.

    :param filename: str
    :return: True/False
    :rtype: bool
    """
    if isinstance(filename, str) and os.path.isfile(filename) and os.access(filename, os.R_OK):
        return True
    return False


def validate_file_write(filename=None):
    """
    Test file exists and is writeable.

    :param filename: str
    :return: True/False
    :rtype: bool
    """
    if isinstance(filename, str) and os.path.isfile(filename) and os.access(filename, os.W_OK):
        return True
    return False


def get_root_and_call_file():
    """
    Return the root file in the calling stack...either the script with __main__
    or the file containing the highest class/function in the calling stack.

    :return: fully qualified file name
    :rtype: str | None
    """
    import inspect
    try:
        # exclude debugger and ipython files
        excludes = ('pydev', 'ipython', '<string>', __file__.lower())  # use only lower case here
        # get all the files in the call stack comparing lower-cased names against excludes
        files = [item.filename for item in inspect.stack() if not any(x in item.filename.lower() for x in excludes)]
        return (None, None) if not files else (os.path.abspath(files[-1]), os.path.abspath(files[0]))
    except Exception as err:
        logging.warning("{}: {}".format(type(err).__name__, err))
        return None, None


class SettingsManager(object):
    def __init__(self,
                 factory_settings=None,
                 factory_file=None,
                 factory_file_name=None, factory_file_path=None,
                 schema_file=None, schema_version=1,
                 schema_file_name=None, schema_file_path=None,
                 user_file=None,
                 user_file_name=None, user_file_path=None,
                 app_file_name=None, app_file_path=None,
                 ):
        """
        Manage settings from input arg or file, keeping track
        of factory defaults vs user changes.

        factory_settings input argument defines the allowed set
        of keys if present, else the entire factory settings
        file is loaded.

        factory_file and user_file take precedence over separate
        name/path arguments for the same.

        factory_file defaults to same calling application directory
        and file name with '.yml' extension.

        user_file defaults to same calling application directory
        and file name with '.yml' extension. Note this is the same
        as the factory_file default, since the expectation is the
        user_file will be stored elsewhere and minimally
        user_file_path will be set.

        app_file_name/path force a calling app_file when calling on behalf
        of another file (ie using for Navigation from ApiServer)

        :param factory_settings: factory settings dictionary
        :type factory_settings: dict | None
        :param factory_file: file (path and name) for factory settings yaml
        :type factory_file: str | None
        :param factory_file_name: file name (defaults to application name)
        :type factory_file_name: str | None
        :param factory_file_path: file path (defaults to application path)
        :type factory_file_path: str | None
        :param schema_version: schema version number
        :type schema_version: int
        :param schema_file: file (path and name) for factory schema yaml
        :type schema_file: str | None
        :param schema_file_name: file name (defaults to application name + _schema_v#)
        :type schema_file_name: str | None
        :param schema_file_path: file path (defaults to application path)
        :type schema_file_path: str | None
        :param user_file: file (path and name) for factory settings yaml
        :type user_file: str | None
        :param user_file_name: file name (defaults to application name)
        :type user_file_name: str | None
        :param user_file_path: file path (defaults to application path)
        :type user_file_path: str | None
        :param app_file_name: file name (defaults to application name)
        :type app_file_name: str | None
        :param app_file_path: file path (defaults to application path)
        :type app_file_path: str | None
        """
        # *** Setup Logger ***
        if app_file_name is not None:
            logger_name = "SM-" + app_file_name
        else:
            logger_name = self.__class__.__name__
        self.logger = logging.getLogger(name=logger_name)
        self.set_logger_level('INFO')  # Set Default Level for Module
        self.logger.debug("Initialized: {}".format(str(datetime.now())))
        start = time.time()

        # Application Awareness
        self._app_filename, self._call_filename = get_root_and_call_file()
        if self._app_filename:
            self._app_path, self._app_name = os.path.split(self._app_filename)
        else:
            self._app_path, self._app_name = os.getcwd(), 'settings.py'
        # Override with init params
        if app_file_name is not None:
            self._app_name = app_file_name
        if app_file_path is not None:
            self._app_path = app_file_path

        # List of custom validators
        self.custom_validators = {}

        # Init settings holders
        self._settings_factory = {}
        self._settings_user = {}

        # *** Schema Settings ***
        self._schema_factory = {}
        self._schema_user = {}
        self._filename_schema = None
        # Construct Schema Defaults Full File Name
        if isinstance(schema_file, str):  # Priority to explicit file
            self._filename_schema = schema_file
        else:
            if isinstance(schema_file_name, str):
                schema_name = schema_file_name
            else:
                schema_name = os.path.basename(self._app_name)
            schema_name, _ = os.path.splitext(schema_name)
            schema_name += '_schema_v' + str(schema_version)
            if isinstance(schema_file_path, str):
                schema_path = schema_file_path
            else:
                schema_path = self._app_path
            self._filename_schema = os.path.join(schema_path, schema_name + '.yml')
        schema_loaded = self.load_schema()

        if self._settings_factory:
            self.logger.info("Factory Schema: {}\n{}".format(self._filename_schema, pformat(self._settings_factory)))
        else:
            # *** Traditional Factory Settings ***
            # Construct Factory Defaults Full File Name
            if isinstance(factory_file, str):  # Priority to explicit file
                self._filename_factory = factory_file
            else:
                if isinstance(factory_file_name, str):
                    factory_name = factory_file_name
                else:
                    factory_name = os.path.basename(self._app_name)
                factory_name, _ = os.path.splitext(factory_name)
                if isinstance(factory_file_path, str):
                    factory_path = factory_file_path
                else:
                    factory_path = self._app_path
                self._filename_factory = os.path.join(factory_path, factory_name + '.yml')
            # Prepare factory settings
            self._settings_factory = {}
            # Load any file factory settings
            file_factory_settings = self._load_settings(self._filename_factory)
            if isinstance(factory_settings, dict):
                self._settings_factory = factory_settings
                self.logger.debug("Factory Settings Loaded: {}".format(self._call_filename))
            if file_factory_settings:
                self.logger.debug("Factory Settings Loaded: {}".format(self._filename_factory))
            # Update provided settings with settings from file
            if self._settings_factory and file_factory_settings:
                self._settings_factory.update((key, file_factory_settings[key]) for key in file_factory_settings.keys()
                                              if key in self._settings_factory.keys())
            # Or simply load file if no provided settings
            elif file_factory_settings and not self._settings_factory:
                self._settings_factory = file_factory_settings
            self.logger.debug("Factory Settings:\n{}".format(pformat(self._settings_factory)))

        # Build a default schema from factory settings if needed
        if not schema_loaded:
            self.logger.warning("No Schema File: generating from factory settings")
            self._schema_factory = {k: {'default': v} for k, v in self._settings_factory.items()}

        # *** User Settings ***

        # Construct User Defaults Full File Name
        if isinstance(user_file, str):  # Priority to explicit file
            self._filename_user = user_file
        else:
            if isinstance(user_file_name, str):
                user_name = user_file_name
            else:
                user_name = os.path.basename(self._app_name)
            user_name, _ = os.path.splitext(user_name)
            if isinstance(user_file_path, str):
                user_path = user_file_path
            else:
                user_path = self._app_path
            self._filename_user = os.path.join(user_path, user_name + '.yml')
        # Load any user settings
        self._settings_user_file = {}
        self._settings_user = {}
        self.apply_settings_file()
        if self._settings_user:
            self.logger.info("User Settings Applied: {}\n{}".format(self._filename_user, pformat(self._settings_user)))

        duration = time.time() - start
        self.logger.debug("Ready ({:.2f}s): {}".format(duration, str(datetime.now())))

    def set_logger_level(self, level=None):
        if isinstance(level, str):
            try:
                level = getattr(logging, level.upper())
            except AttributeError:
                pass
        if isinstance(level, int) and 0 <= level <= 50:
            self.logger.setLevel(level)
        return logging.getLevelName(self.logger.getEffectiveLevel())

    def get_logger_level(self):
        return self.set_logger_level()

    def load_schema(self, schema_filename=None):
        if schema_filename is None:
            schema_filename = self._filename_schema

        # Load the Schema
        try:
            with open(schema_filename, 'r') as stream:
                try:
                    schema = yaml.safe_load(stream)
                except yaml.YAMLError as msg:
                    self.logger.warning('Data Validation YAML LOAD ERROR: {}'.format(msg))
                    for err_data in traceback.extract_tb(sys.exc_info()[2], limit=3):
                        filename, line_no, function_name, line_text = err_data
                        self.logger.debug("{}:{} in {}".format(os.path.basename(filename), line_no, function_name))
                    return False
            self._schema_factory = deepcopy(schema)
            self._settings_factory = {k: v['default'] for k, v in schema.items()}
            self.update_schema()
            return True
        except IOError as msg:
            self.logger.warning('Data Validation IOError: {}'.format(msg))
            for err_data in traceback.extract_tb(sys.exc_info()[2], limit=3):
                filename, line_no, function_name, line_text = err_data
                self.logger.debug("{}:{} in {}".format(os.path.basename(filename), line_no, function_name))
        return False

    def update_schema(self):
        self._schema_user = deepcopy(self._schema_factory)
        for k in self._settings_user:
            self._schema_user[k]['default'] = self._settings_user[k]

    def _load_settings(self, settings_file=None):
        """
        Load a settings yml returning dictionary without updating class variables

        :param settings_file: file name
        :type settings_file: str | None
        :return: dictionary from settings file
        :rtype: dict
        """
        try:
            with open(settings_file, 'r') as stream:
                yaml_config = yaml.safe_load(stream)
                if yaml_config is not None:
                    return yaml_config
                else:
                    self.logger.debug("Config File '{}' exists but is empty.".format(settings_file))
        except TypeError as err:  # Catch invalid filename
            self.logger.debug("TypeError: {}".format(err))
        except IOError as err:
            self.logger.debug("IOError: {}".format(err))
        return {}

    def load_settings_file(self, settings_file=None):
        """
        Load a settings yml.

        :param settings_file: file name
        :type settings_file: str | None
        :return: dictionary of current user settings from file
        :rtype: dict
        """
        if settings_file is None:
            settings_file = self._filename_user
        self._settings_user_file = self._load_settings(settings_file)
        self._settings_user_file = self.validate_settings(**self._settings_user_file)
        return self._settings_user_file, settings_file

    def apply_settings_file(self, settings_file=None):
        """
        Load a settings yml and apply the loaded settings.

        :param settings_file: file name
        :type settings_file: str | None
        :return: dictionary of current user settings
        :rtype: dict
        """
        _, settings_file = self.load_settings_file(settings_file=settings_file)
        _ = self.set_settings(_settings_file=settings_file, **self._settings_user_file)
        return self._settings_user

    def validate_settings(self, **kwargs):
        """
        If a user schema exists, return only valid coerced kwargs,
        else return unchanged kwargs.
        """
        # Coerce and Remove invalid data
        if self._schema_user:
            validator = EnhancedValidator(self._schema_user, purge_unknown=True)
            norm = validator.normalized(kwargs, always_return_document=True)
            # norm contains all keys in schema, so only use it to update kwargs
            kwargs = {k: v for k, v in norm.items() if k in kwargs}
            errors = validator.errors
            """:type: dict"""
            validator.validate(kwargs)
            errors.update(validator.errors)
            # Remove any error parameters
            if isinstance(errors, dict) and errors:
                self.logger.warning(errors)
                for k in errors:
                    kwargs.pop(k)
        return kwargs

    def set_settings(self, **kwargs):
        """
        Apply a dictionary of settings passed as kwargs, only accepting updates
        to the loaded factory settings, and warning about unexpected
        entries.

        Returns dict containing value of keys provided

        :keyword _settings_file (str):
        :return: dictionary of current user settings
        :rtype: dict
        """
        settings_file = kwargs.pop('_settings_file', "DYNAMIC INPUT")
        # run custom validators first
        kwargs = self._custom_validate(**kwargs)
        # update only existing items
        identity_dict = {}
        restored_dict = {}
        applied_dict = {}
        unchanged_dict = {}
        valid_settings = self.validate_settings(**kwargs)
        extras_dict = {k: kwargs[k] for k in set(kwargs) - set(valid_settings)}
        for k, v in valid_settings.items():
            if k in self._settings_factory:
                if v != self._settings_factory[k]:
                    if k in self._settings_user and v == self._settings_user[k]:
                        unchanged_dict[k] = v
                    else:
                        applied_dict[k] = v
                    self._settings_user[k] = v  # set it regardless because == != is
                else:
                    try:
                        restored_dict[k] = self._settings_user.pop(k)
                    except KeyError:
                        identity_dict[k] = v
            else:
                extras_dict[k] = v
        if restored_dict:
            self.logger.debug("{}: Removed User Settings: {}".format(settings_file, pformat(restored_dict)))
        if applied_dict:
            self.logger.debug("{}: Applied Settings: {}".format(settings_file, pformat(applied_dict)))
        if unchanged_dict:
            self.logger.debug("{}: Ignored Unchanged Settings: {}".format(settings_file, pformat(unchanged_dict)))
        if identity_dict:
            self.logger.debug(
                "{}: Ignored Factory Defaults for Settings: {}".format(settings_file, pformat(identity_dict)))
        if extras_dict:
            self.logger.warning("{}: Ignored Unknown Settings: {}".format(settings_file, pformat(extras_dict)))
        self.update_schema()
        return {k: v for k, v in self.get_settings().items() if k in kwargs.keys()}

    def get_settings(self, *args):
        """
        Return settings dictionary (factory updated with user).
        If args supplied, return only the specified settings
        as value if string or dict if tuple.

        :return: settings dictionary
        :rtype: dict
        """
        all_settings = deepcopy(self._settings_factory)
        all_settings.update(self._settings_user)
        try:
            if len(args) == 1:
                try:
                    return all_settings.get(args[0], None)
                except AttributeError:
                    return None
            elif args:
                return {k: v for k, v in all_settings.items() if k in args}
        except TypeError:
            self.logger.warning("Invalid settings keys: {}".format(args))
        return all_settings

    def get_setting(self, key):
        """
        Return the specified setting value

        Function signature prevents warnings when only returning single result

        :return:
        :rtype:
        """
        return self.get_settings(key)

    def get_settings_user(self):
        """
        Return only user modified settings

        :return: settings dictionary
        :rtype: dict
        """
        return self._settings_user

    def get_settings_user_file(self):
        """
        Return only user settings from file

        :return: settings dictionary
        :rtype: dict
        """
        return self._settings_user_file

    def get_settings_factory(self, *args):
        """
        Return only factory default settings

        :return: settings dictionary
        :rtype: dict
        """
        return_settings = deepcopy(self._settings_factory)
        try:
            if len(args) == 1:
                try:
                    return return_settings.get(args[0], None)
                except AttributeError:
                    return None
            elif args:
                return {k: v for k, v in return_settings.items() if k in args}
        except TypeError:
            self.logger.warning("Invalid settings keys: {}".format(args))
        return return_settings

    def clear_settings_runtime(self):
        """
        Clear runtime changes, restoring to stored user settings
        :return:
        """
        self._settings_user = deepcopy(self._settings_user_file)
        return self._settings_user

    def clear_settings(self, *args):
        """
        Clear all user settings (restoring factory defaults).
        If args specified, only clear/restore those keys.

        :return: current combined settings | cleared keys dict
        :rtype: dict
        """
        if args:
            cleared = {}
            for k in args:
                if k in self._settings_factory:
                    cleared[k] = self._settings_user.pop(k, None)
                    self.logger.info("Resetting {} to {}".format(k, self._settings_factory[k]))
            return cleared
        else:
            self._settings_user = {}
            self.logger.info("All User Settings Cleared")
        return self.get_settings()

    def factory_reset(self):
        """
        Clear all user settings and delete the user settings file.

        :return: True/False based on successful user settings file removal
        :rtype: bool
        """
        self.clear_settings()
        try:
            os.remove(self._filename_user)
            self._settings_user_file = {}
            self.logger.warning("RESTORING FACTORY SETTINGS. REMOVED: {}".format(self._filename_user))
            return True
        except (IOError, OSError) as err:
            if os.path.isfile(self._filename_user):
                self.logger.error("FACTORY RESET FAILED. UNABLE TO REMOVE {} <- {}".format(self._filename_user, err))
            else:
                self.logger.warning("FACTORY RESET: User file does not exist: {}".format(self._filename_user))
        return False

    def save_settings(self, *args, settings_file=None):
        """
        Save user settings to file.
        If args specified, save only the provided keys.

        :param settings_file: defaults to user file name from init
        :type settings_file: str | None
        :return: True/False based on successful file write
        :rtype: bool
        """
        if settings_file is None:
            settings_file = self._filename_user
        if args:
            settings_to_store = self.load_settings_file(settings_file)
            for key in args:
                if key in self._settings_factory:
                    settings_to_store[key] = self._settings_user[key]
        else:
            settings_to_store = self.get_settings_user()
        if not settings_to_store:
            self.logger.info("No user settings to save.")
            if not args and os.path.isfile(settings_file):
                self.logger.info("Removing user settings file: {}".format(settings_file))
                try:
                    os.remove(settings_file)
                    self._settings_user_file = {}
                except OSError as err:
                    self.logger.warning("Failed to remove: {} -> {}".format(settings_file, err))
        else:
            try:
                os.makedirs(os.path.dirname(settings_file), exist_ok=True)
                with open(settings_file, 'w') as stream:
                    yaml.safe_dump(settings_to_store, stream, default_flow_style=False)
                self.logger.info("Wrote user settings to {}: {}".format(settings_file, pformat(self._settings_user)))
                self.load_settings_file(settings_file)
                return True
            except IOError as err:
                self.logger.warning("{} IOError: {}".format(settings_file, err))
        return False

    def save_settings_all(self, settings_file=None):
        """
        Save all settings (user and factory) to file.

        :param settings_file: defaults to user file name from init
        :type settings_file: str | None
        :return: True/False based on successful file write
        :rtype: bool
        """
        if settings_file is None:
            settings_file = self._filename_user
        if validate_file_write(settings_file):
            try:
                os.makedirs(os.path.dirname(settings_file), exist_ok=True)
                with open(settings_file, 'w') as stream:
                    yaml.safe_dump(self._settings_factory, stream, default_flow_style=False)
                self.logger.info("Wrote all settings to {}: {}".format(settings_file, pformat(self._settings_user)))
                return True
            except IOError as err:
                self.logger.warning("{} IOError: {}".format(settings_file, err))
        else:
            self.logger.warning("Unable to access/write: {}".format(settings_file))
        return False

    def save_factory_defaults(self, settings_file=None):
        """
        Save factory settings to file.

        :param settings_file: defaults to factory file name from init
        :type settings_file: str | None
        :return: True/False based on successful file write
        :rtype: bool
        """
        if self._schema_user:
            if settings_file is None:
                settings_file = self._filename_schema
            if validate_file_write(settings_file):
                try:
                    os.makedirs(os.path.dirname(settings_file), exist_ok=True)
                    with open(settings_file, 'w') as stream:
                        yaml.safe_dump(self._schema_factory, stream,
                                       Dumper=ExplicitDumper,
                                       default_flow_style=False)
                    self.logger.info(
                        "Wrote factory schema: {}:\n{}".format(settings_file, pformat(self._schema_factory)))
                    return True
                except IOError as err:
                    self.logger.warning("{} IOError: {}".format(settings_file, err))
            return False
        if settings_file is None:
            settings_file = self._filename_factory
        if validate_file_write(settings_file):
            try:
                os.makedirs(os.path.dirname(settings_file), exist_ok=True)
                with open(settings_file, 'w') as stream:
                    yaml.safe_dump(self._settings_factory, stream, default_flow_style=False)
                self.logger.info(
                    "Wrote factory settings: {}:\n{}".format(settings_file, pformat(self._settings_factory)))
                return True
            except IOError as err:
                self.logger.warning("{} IOError: {}".format(settings_file, err))
        return False

    def get_filename_factory(self):
        """
        Return the factory defaults file name.

        :return: filename
        :rtype: str
        """
        return self._filename_factory

    def get_filename_user(self):
        """
        Return the user defaults file name.

        :return: filename
        :rtype: str
        """
        return self._filename_user

    def set_factory_default(self, **kwargs):
        """
        Update factory defaults. Only update specified kwargs.
        Specify save_factory_defaults=True to save after update.

        :param kwargs: key=value listing for updates
        :keyword save_factory_defaults: optional parameter to save factory defaults
        :return: key(s)=value(s) updated
        :rtype: dict
        """
        save_factory_defaults = kwargs.pop('save_factory_defaults', False)
        valid_settings = self.validate_settings(**kwargs)
        for k in valid_settings:
            self._schema_factory[k]['default'] = valid_settings[k]
            self._settings_factory[k] = valid_settings[k]
            self.logger.info("Updated factory setting: {}".format({k: valid_settings[k]}))
        if save_factory_defaults:
            self.save_factory_defaults()
        return {k: v for k, v in valid_settings.items()}

    def _custom_validate(self, **kwargs):
        """
        Replace kwargs with validated kwargs using custom validator function
        :param kwargs:
        :return:
        """
        out = {}
        if self.custom_validators:  # Skip if no validators
            for k, v in kwargs.items():
                if k in self.custom_validators.keys() and k not in out.keys():
                    match = self._get_matching_pair(k)
                    if match in kwargs:
                        valid_dict = self.custom_validators[k](**{k: v, match: kwargs[match]})
                    else:
                        valid_dict = self.custom_validators[k](**{k: v})
                    out.update(valid_dict)
                else:
                    out[k] = v
        else:
            return kwargs
        return out

    def _get_matching_pair(self, key):
        """
        Look for a key with a matching function reference.

        :param key:
        :return:
        """
        func = self.custom_validators[key]
        for k, v in self.custom_validators.items():
            if k != key and v == func:
                return k
        return None

    def set_custom_validator(self, func, *args):
        """
        Provide a custom validator function for a list of interdependent keys.
        Currently only supports 1 or 2 keys.

        :param func: function reference
        :param args: keys
        :return: list of keys with a validator
        """

        if len(args) > 2:
            raise TypeError("Only 2 keys supported, yet {} provided".format(len(args)))

        for k in args:
            if k in self.custom_validators:
                raise ValueError("{} <- key already exists in custom validators".format(k))
            self.custom_validators[k] = func

        # Validate user_settings file against schema
        current_user = {}
        current_file = {}
        for k in args:
            try:
                current_user[k] = self._settings_user.pop(k)
            except KeyError:
                pass
            try:
                current_file[k] = self._settings_user_file.pop(k)
            except KeyError:
                pass
        if current_file:
            result = func(**current_file)
            for k in current_file:
                if current_file[k] == result[k]:
                    self._settings_user_file[k] = current_file[k]
                else:
                    self.logger.debug("Removed Invalid File Key: {}={}".format(k, current_file[k]))
        if current_user:
            result = func(**current_user)
            for k in current_user:
                if current_user[k] == result[k]:
                    self._settings_user[k] = current_file[k]
                else:
                    self.logger.debug("Removed Invalid User Key: {}={}".format(k, current_file[k]))
        return list(self.custom_validators.keys())

    def get_custom_validator_list(self):
        """
        Return the current list of keys with custom validators

        :return:
        """
        return list(self.custom_validators.keys())

    def clear_custom_validator_list(self):
        """
        Clear the list of custom validators

        :return:
        """
        self.custom_validators = {}
        return True


def _test_cases():
    ds = SettingsManager(factory_settings={'param1': 123, 'param2': 456})
    print(ds.get_settings())
    print(ds.set_settings(param1=123, param2=780, param3='abc'))
    print(ds.get_settings_user())
    print(ds.save_settings())
    print(ds.set_settings(param2=456))
    print(ds.save_settings())
    # print(ds.clear_settings('param2'))
    print(ds.get_filename_factory())
    print(ds.get_filename_user())


if __name__ == "__main__":
    _test_cases()
