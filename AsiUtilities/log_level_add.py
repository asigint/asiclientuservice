# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved
# Based on http://stackoverflow.com/a/35804945/3571110
# Modified with http://stackoverflow.com/a/40720015/3571110

import logging


def log_level_add(level_name, level_number, method_name=None):
    if not method_name:
        method_name = level_name.lower()

    def log_for_level(self, message, *args, **kwargs):
        if self.isEnabledFor(level_number):
            self._log(level_number, message, args, **kwargs)

    attributes = {method_name: log_for_level,
                  level_name: level_number}
    # noinspection PyPep8Naming
    MyLogger = type('MyLogger', (logging.getLoggerClass(),), attributes)

    # Set the logging class and add the level
    logging.setLoggerClass(MyLogger)
    logging.addLevelName(level_number, level_name)
