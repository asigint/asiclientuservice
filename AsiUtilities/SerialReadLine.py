import re
import logging


class SerialReadLine(object):
    
    def __init__(self, serial_device):
        """

        :type serial_device: serial.Serial
        :return:
        """
        self.dataResidual = None
        self.serial_device = serial_device
        self.SUPPRESS_READ_FAILURE = False
        self.loggerSRP = logging.getLogger(self.__class__.__name__)
    
    def __read_buffer(self):
        try:
            data = self.serial_device.read(size=1)
            n = self.serial_device.in_waiting
            if n:
                data = data + self.serial_device.read(n)
            return data
        except Exception as err:
            # print "readBuffer ERROR: ", e
            # sys.exit(1)
            return None

    def read_line(self):

        line_buffer = b''

        # If we have new data from the data CRLF split, then
        # it's the start + data of our next sentence.
        # Have it be the start of the new line
        if self.dataResidual is not None:
            line_buffer += self.dataResidual

        # Read from the input buffer and append it to our line
        # being constructed
        try:
            line_buffer += self.__read_buffer()
            self.SUPPRESS_READ_FAILURE = False
        except TypeError:   # Likely the readBuffer() failed
            if not self.SUPPRESS_READ_FAILURE:
                self.loggerSRP.debug("COMPASS: READ FAILURE")
                self.SUPPRESS_READ_FAILURE = True
            return None

        # Look for  \x0d\x0a or \r\n at the end of the line (CRLF)
        # after each input buffer read so we can find the end of our
        # line being constructed
        sentence = None
        self.dataResidual = None

        # print "STARTING WITH NEW BUFFER"
        # print "------------------------"
        # print repr(line_buffer)

        # noinspection PyBroadException
        try:
            if re.search(b'\r\n', line_buffer):
                # Since we found a CRLF, split it out
                if not re.search(b'\r\n$', line_buffer):
                    # Partial line at end...save and start next read with this
                    self.dataResidual = line_buffer.rsplit(b'\r\n', 1)[-1]
                    # Remove the residual from future operations
                    line_buffer = line_buffer[:-len(self.dataResidual)]
                # Remove ending CRLF
                line_buffer = line_buffer[:-2]
                sentence = line_buffer.split(b'\r\n')
            else:
                # No CRLF...potential partial read
                self.dataResidual = line_buffer
        except Exception as err:
            logging.debug("COMPASS PARSING ERROR: {0}".format(err))
            pass

        # if sentence is not None:
        #     return sentence[-1]
        # return None     # No Valid Sentence
        return sentence
