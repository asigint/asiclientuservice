#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2017 Applied Signals Intelligence, Inc. -- All Rights Reserved

import errno
import logging
import socket


def addr_tuple_to_str(addr_tuple):
    return "{}:{}".format(*addr_tuple)


def addr_str_to_tuple(ip_str):
    """
    Convert a string in the form '192.168.0.1:7000' into a (string, int) tuple ('192.168.0.1', 7000)
    Validate the address in the process returning None for invalid address

    :param ip_str: 'host:port' string ('host' only or ':port' only accepted)
    :type ip_str: str | tuple(str, int)
    :return: host, port
    :rtype: string_types | None, int | None
    """
    host = None
    port = None

    # Validate a tuple by converting to ip_str and validating
    if isinstance(ip_str, tuple):
        # cast as str() to protect against unicode/string/bytestring troubles
        ip_str = addr_tuple_to_str(ip_str)

    if isinstance(ip_str, str):
        # cast as str() to protect against unicode/string/bytestring troubles
        addr_pair = str(ip_str).split(':')
        try:
            try:
                socket.inet_pton(socket.AF_INET, addr_pair[0])
            except AttributeError:
                # IPv6 inet_pton() not supported
                socket.inet_aton(addr_pair[0])
            if addr_pair[0].count('.') == 3:
                # inet_pton/aton will accept partial address
                host = addr_pair[0]
        except IOError:
            pass
        try:
            p = int(addr_pair[1])
            if 0 <= p <= 65535:
                port = p
                if p < 1024:
                    print('PRIVILEGED PORT SPECIFIED')
        except (IndexError, AttributeError, ValueError):
            pass
    return host, port


def create_multicast_rx(mcast_addr, mcast_port, if_addr='0.0.0.0', suppress_errors=False):
    """
    Create a generic multicast receive socket and bind to it.
    Assume INADDR_ANY='0.0.0.0'=''

    :param mcast_addr: INET4 address in the form 0.0.0.0
    :type mcast_addr: str
    :param mcast_port: Port Number
    :type mcast_port: int
    :param if_addr: INET4 address in the form 0.0.0.0
    :type if_addr: str
    :param suppress_errors: Suppress errors to handle at a higher level
    :type suppress_errors: bool
    :return: socket
    :rtype: socket.SocketType
    """

    if if_addr == '':
        # socket.inet_aton() does not work for ''
        if_addr = '0.0.0.0'

    # create a UDP socket
    mcast_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # See https://stackoverflow.com/a/14388707/3571110
    # These should be equivalent for multicast, but not every platform supports both
    try:
        mcast_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
    except AttributeError:
        pass
    try:
        mcast_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, True)
    except AttributeError:
        pass

    mreq = socket.inet_aton(mcast_addr) + socket.inet_aton(if_addr)
    # The below is equivalent when using if_addr='0.0.0.0'
    # mreq = struct.pack("4sl", socket.inet_aton(mcast_addr), socket.INADDR_ANY)

    # Note SOL_IP is equivalent to IPPROTO_IP
    try:
        mcast_sock.setsockopt(socket.SOL_IP, socket.IP_ADD_MEMBERSHIP, mreq)
    except OSError as err:
        logging.error('FAILED TO ADD MEMBERSHIP: {}'.format(if_addr))

    # finally bind the socket to start getting data into your socket
    try:
        mcast_sock.bind((mcast_addr, mcast_port))
    except OSError as err:
        if err.errno == 10049:  # WSAEADDRNOTAVAIL
            # Windows won't allow binding to mcast_addr, so bind to if_addr
            # Note: binding to if_addr doesn't work on Linux, only mcast_addr or ''/'0.0.0.0'
            mcast_sock.bind((if_addr, mcast_port))
        elif err.errno == errno.EADDRINUSE:
            if not suppress_errors:
                logging.error('FAILED TO CREATE SOCKET: Address already in use: {}:{}'.format(mcast_addr, mcast_port))
            mcast_sock.close()
            mcast_sock = None
        else:
            logging.error("create_multicast_rx(): {}".format(err))

    return mcast_sock


def add_multicast_if(mcast_sock, mcast_addr, if_addr):
    """
    Add Membership to a Multicast on a specific interface.
    WARNING: An interface can have multiple address, so adding one
    interface address add the entire interface (ie. all interface addresses).
    This condition is silently ignored.

    :param mcast_sock: socket to modify
    :type mcast_sock: socket.SocketType
    :param mcast_addr: INET4 address in the form 0.0.0.0
    :type mcast_addr: str
    :param if_addr: INET4 address in the form 0.0.0.0
    :type if_addr: str
    :return: modified socket
    :rtype: socket.SocketType
    """
    mreq = socket.inet_aton(mcast_addr) + socket.inet_aton(if_addr)
    try:
        mcast_sock.setsockopt(socket.SOL_IP, socket.IP_ADD_MEMBERSHIP, mreq)
    except OSError as err:
        if err.errno == errno.EADDRINUSE:
            # This interface is already a member (potentially via a different ip address)
            pass
        elif err.errno == 10022:  # WSAEINVAL -> Invalid argument
            # Windows does not allow changes after bind, but binding to '0.0.0.0' seems to work
            pass
        else:
            logging.warning("add_multicast_if({}, {}): {}".format(mcast_addr, if_addr, err))
    return mcast_sock


def drop_multicast_if(mcast_sock, mcast_addr, if_addr):
    """
    Drop Membership from a Multicast on a specific interface.
    WARNING: An interface can have multiple address, so if removing one
    interface address will remove the entire interface (ie. all interface addresses)

    :param mcast_sock: socket to modify
    :type mcast_sock: socket.SocketType
    :param mcast_addr: INET4 address in the form 0.0.0.0
    :type mcast_addr: str
    :param if_addr: INET4 address in the form 0.0.0.0
    :type if_addr: str
    :return: modified socket
    :rtype: socket.SocketType
    """
    mreq = socket.inet_aton(mcast_addr) + socket.inet_aton(if_addr)
    try:
        mcast_sock.setsockopt(socket.SOL_IP, socket.IP_DROP_MEMBERSHIP, mreq)
    except OSError as err:
        logging.warning(err)
    return mcast_sock


def socket_open(address, net_address, ttl=1):
    """
    Manage all the socket settings when opening a socket.
    Currently only supports Multicast

    :param address:
    :param net_address:
    :param ttl:
    :return:
    """
    if isinstance(address, str):
        try:
            # VALIDATE ADDRESS
            socket.inet_aton(address)
            if int(address.split('.')[0]) > 224:
                sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
                sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
                if hasattr(socket, 'SO_REUSEPORT'):
                    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, True)
                sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)
                # sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_LOOP, True)
                sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_IF, socket.inet_aton(net_address))
                return sock
        except IOError as err:
            print("SOCKET OPEN ERROR - {}: {}".format(type(err).__name__, err))
        except Exception as err:
            print("{}: {}".format(type(err).__name__, err))
    return None


def socket_close(sock):
    try:
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
    except IOError:
        pass
