#!/usr/bin/env python
# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved
import os
import glob
from AsiUtilities.data_file_io import read_mp
import numpy as np
from AsiDfEngine.AsiDfE import AsiDfE, DfTarget
from SignalProcessing.communications import phase_unwrap_degrees
from AsiUtilities.CalibrationUtilities import compute_correction_vector, write_simple_table, CalibratePlatform


class FrequencyResult(object):
    def __init__(self, yaw, pitch, roll, timestamps, filenames, peaks, frequency, df_frequency,
                 cal_data, mag_tweak, phase_tweak):

        self.number_of_positions = len(yaw)
        self.results = {'yaw': yaw,
                        'pitch': pitch,
                        'roll': roll,
                        'timestamps': timestamps,
                        'frequency': frequency,
                        'df_frequency': df_frequency,
                        'filenames': filenames,
                        'peaks': peaks,
                        'e_fields': None,
                        'h_fields': None,
                        'AoA': np.zeros(self.number_of_positions),
                        'elev': np.zeros(self.number_of_positions),
                        'snr': np.zeros(self.number_of_positions),
                        'cal_data': cal_data,
                        'monotonic_aoa': True,
                        'mag_tweak': mag_tweak,
                        'phase_tweak': phase_tweak,
                        'phase_diff': np.zeros((self.number_of_positions, 6)),
                        'Ex': np.zeros(self.number_of_positions),
                        'Ey': np.zeros(self.number_of_positions),
                        'Ea': np.zeros(self.number_of_positions),
                        'Eb': np.zeros(self.number_of_positions),
                        'Er': np.zeros(self.number_of_positions),
                        'El': np.zeros(self.number_of_positions),
                        }

    def compute_aoa(self, configuration_file=None, target_bw=15e3, array_index=0,
                    technique=DfTarget.Techniques.frequency_domain, average_aoa=True):

        if configuration_file is None:
            dfe = AsiDfE()
        else:
            dfe = AsiDfE(configuration_file)
        dfe.add_target(self.results['df_frequency'], target_bw, technique)

        data, data_nav, radio_parameters, _ = read_mp(self.results['filenames'][0])
        channels, b = data.shape
        # shift_vector = (-1) ** (np.arange(b))
        # spectrum = np.fft.fft(data * shift_vector)
        # spectrum = np.fft.fftshift(np.fft.fft(data), axes=1)

        spectrum_frequency = np.arange(-b / 2, b / 2) * radio_parameters['sample_rate'] / b + \
                             radio_parameters['frequency'][0]

        self.results['e_fields'] = np.zeros((self.number_of_positions, channels // 2), np.complex128)
        self.results['h_fields'] = np.zeros((self.number_of_positions, channels // 2), np.complex128)
        window = np.hanning(b) ** 2
        # data2 = np.zeros(data.shape, np.complex128)
        for m in range(self.number_of_positions):
            data, data_nav, radio_parameters, _ = read_mp(self.results['filenames'][m])
            # data2[:] = data[:]
            spectrum = np.fft.fftshift(np.fft.fft(data * window), axes=1)
            try:
                output = dfe.get_df_results(spectrum, spectrum_frequency, data_nav, array_index)[0]
                self.results['AoA'][m] = output['AoA']
                self.results['snr'][m] = output['snr']
                self.results['elev'][m] = output['dep']
                self.results['e_fields'][m], self.results['h_fields'][m] = self._compute_fields(spectrum,
                                                                                                spectrum_frequency)
                self.results['Ex'][m] = output['Ex']
                self.results['Ey'][m] = output['Ey']
                self.results['Ea'][m] = output['Ea']
                self.results['Eb'][m] = output['Eb']
                self.results['Er'][m] = output['Er']
                self.results['El'][m] = output['El']
                for wwp in range(6):
                    self.results['phase_diff'][m, wwp] = np.angle(np.inner(data[0].conj(), data[wwp]), deg=True)
            except TypeError:
                pass

        try:
            g = self.results['AoA'].copy()
            g = phase_unwrap_degrees(g)
            if average_aoa:
                g1 = np.convolve(g, np.ones(3) / 3, 'same')
                g1[0] = g[0]
                g1[-1] = g[-1]
            else:
                g1 = g
            x = np.sign(np.diff(g1))
            if len(np.where(x != x[0])[0]) != 0:
                self.results['monotonic_aoa'] = False
            else:
                self.results['monotonic_aoa'] = True
            if len(self.results['AoA']) > 6:
                self.results['AoA'] = g1 % 360
        except:
            pass

        return self.results['AoA']

    def get_spectrum(self, window=False):

        if self.number_of_positions > 0:
            data, data_nav, radio_parameters, _ = read_mp(self.results['filenames'][0])
        else:
            return None

        spectrum = np.zeros((self.number_of_positions, data.shape[0], data.shape[1]), np.complex128)
        b = spectrum.shape[-1]
        spectrum_frequency = np.arange(-b / 2, b / 2) * radio_parameters['sample_rate'] / b + \
                             radio_parameters['frequency'][0]
        sv = (-1) ** (np.arange(b))

        if window:
            win = np.hanning(b)
            shift_vector = sv * win
        else:
            shift_vector = sv

        spectrum[0] = np.fft.fft(data * shift_vector)
        for m in range(1, self.number_of_positions):
            data, data_nav, radio_parameters, _ = read_mp(self.results['filenames'][m])
            spectrum[m] = np.fft.fft(data * shift_vector)

        return spectrum_frequency, spectrum

    def get_time_domain_data(self):

        if self.number_of_positions > 0:
            data, data_nav, radio_parameters, _ = read_mp(self.results['filenames'][0])
        else:
            return None

        data_out = np.zeros((self.number_of_positions, data.shape[0], data.shape[1]), np.complex128)
        data_out[0] = data.copy()
        for m in range(1, self.number_of_positions):
            data, data_nav, radio_parameters, _ = read_mp(self.results['filenames'][m])
            data_out[m] = data.copy()

        return data_out

    def _compute_fields(self, spectrum, spectrum_frequency):

        index = abs(spectrum_frequency - self.results['df_frequency']).argmin()
        e = spectrum[1::2, index] + spectrum[0::2, index]
        h = spectrum[1::2, index] - spectrum[0::2, index]

        return e, h


def analyze_data(path_to_data='', date_of_collection='*', time_of_collection='*', cal_flag=False, average_aoa=True):

    filename = os.path.join(path_to_data, '*-{0}_{1}*.dat'.format(date_of_collection, time_of_collection))

    file_list = glob.glob(filename)
    n = len(file_list)
    data, data_nav, radio_parameters, _ = read_mp(file_list[0])

    peaks = np.zeros((n, data.shape[0]), np.complex128)
    freqs = np.zeros(n)
    center_freqs = np.zeros(n)
    timestamps = np.zeros(n)
    yaw = np.zeros(n)
    pitch = np.zeros(n)
    roll = np.zeros(n)
    cal_data = np.zeros(n, np.bool)
    df_frequencies = np.zeros(n)
    phase_tweaks = np.zeros((n, data.shape[0]))
    mag_tweaks = np.zeros((n, data.shape[0]))
    lats = np.zeros(n)
    lons = np.zeros(n)

    shift_vector = None
    for m in range(n):
        data, data_nav, radio_parameters, _ = read_mp(file_list[m])
        lats[m] = data_nav['GPS']['latitude']
        lons[m] = data_nav['GPS']['longitude']
        timestamps[m] = data_nav['COMPASS']['systime']
        yaw[m] = data_nav['COMPASS']['yaw_corrected']
        pitch[m] = data_nav['COMPASS']['pitch_corrected']
        roll[m] = data_nav['COMPASS']['roll_corrected']
        if 'cal_data' in radio_parameters:
            cal_data[m] = radio_parameters['cal_data']
        else:
            cal_data[m] = False
        df_frequencies[m] = radio_parameters['df_frequency']
        if 'mag_tweak' in radio_parameters:
            mag_tweaks[m] = radio_parameters['mag_tweak']
        else:
            mag_tweaks[m] = 0
        if 'phase_tweak' in radio_parameters:
            phase_tweaks[m] = radio_parameters['phase_tweak']
        else:
            phase_tweaks[m] = 0
        b = data.shape[-1]
        if shift_vector is None:
            shift_vector = (-1) ** (np.arange(b))
        spectrum = np.fft.fft(data * shift_vector)
        spectrum_frequency = np.arange(-b / 2, b / 2) * radio_parameters['sample_rate'] / b + \
                             radio_parameters['frequency'][0]
        # peaks[m] = abs(spectrum).max(1)
        # index = abs(spectrum[0]).argmax()
        # freqs[m] = spectrum_frequency[index]
        index = abs(spectrum_frequency - df_frequencies[m]).argmin()
        peaks[m] = spectrum[:, index]
        freqs[m] = spectrum_frequency[index]
        center_freqs[m] = radio_parameters['frequency'][0]

    frequency_list = np.unique(freqs)
    number_frequencies = len(frequency_list)
    list_of_results = []
    for m in range(number_frequencies):
        index = np.where(freqs == frequency_list[m])[0]
        if cal_flag:
            index2 = np.where(cal_data[index])[0]
        else:
            index2 = np.where(~cal_data[index])[0]
        if len(index2) > 0:
            index_time = timestamps[index[index2]].argsort()
            index = index[index2[index_time]]
            # list_of_results.append(FrequencyResult(yaw[index], pitch[index], roll[index], timestamps[index],
            #                                        np.array(file_list)[index], peaks[index], frequency_list[m],
            #                                        df_frequencies[index][0], cal_data[index], mag_tweaks[index],
            #                                        phase_tweaks[index]))
            list_of_results.append(FrequencyResult(yaw[index], pitch[index], roll[index], timestamps[index],
                                                   np.array(file_list)[index], peaks[index], center_freqs[index][0],
                                                   df_frequencies[index][0], cal_data[index], mag_tweaks[index],
                                                   phase_tweaks[index]))
            list_of_results[-1].results.update({'latitudes': lats[index], 'longitudes': lons[index]})
    for result in list_of_results:
        result.compute_aoa(average_aoa=average_aoa)

    return list_of_results


def find_best_match(dfe2, spectrum, spectrum_freq, data_nav):

    r1 = dfe2.get_df_results(spectrum, spectrum_freq, data_nav)
    snr_max = -10000
    f_out = 0
    for r in r1:
        if r['snr'] > snr_max:
            snr_max = r['snr']
            f_out = r['frequency']
    return f_out, snr_max


def analyze_data_bad_freqs(path_to_data='', date_of_collection='*', time_of_collection='*', cal_flag=False,
                           frequency_list=None):

    filename = os.path.join(path_to_data, '*-{0}_{1}*.dat'.format(date_of_collection, time_of_collection))

    file_list = glob.glob(filename)
    n = len(file_list)
    file_list = np.array(file_list)
    file_list.sort()

    data, data_nav, radio_parameters, _ = read_mp(file_list[0])

    peaks = np.zeros((n, data.shape[0]), np.complex128)
    center_freqs = np.zeros(n)
    timestamps = np.zeros(n)
    yaw = np.zeros(n)
    pitch = np.zeros(n)
    roll = np.zeros(n)
    cal_data = np.zeros(n, np.bool)
    df_frequencies = np.zeros(n)
    phase_tweaks = np.zeros((n, data.shape[0]))
    mag_tweaks = np.zeros((n, data.shape[0]))
    lats = np.zeros(n)
    lons = np.zeros(n)

    for m in range(n):
        data, data_nav, radio_parameters, _ = read_mp(file_list[m])
        lats[m] = data_nav['GPS']['latitude']
        lons[m] = data_nav['GPS']['longitude']
        timestamps[m] = data_nav['COMPASS']['systime']
        yaw[m] = data_nav['COMPASS']['yaw_corrected']
        pitch[m] = data_nav['COMPASS']['pitch_corrected']
        roll[m] = data_nav['COMPASS']['roll_corrected']
        cal_data[m] = radio_parameters['cal_data']
        mag_tweaks[m] = radio_parameters['mag_tweak']
        phase_tweaks[m] = radio_parameters['phase_tweak']
        df_frequencies[m] = radio_parameters['df_frequency']
        # b = data.shape[-1]
        # if shift_vector is None:
        #     shift_vector = (-1) ** (np.arange(b))
        # spectrum = np.fft.fft(data * shift_vector)
        # spectrum_frequency = np.arange(-b / 2, b / 2) * radio_parameters['sample_rate'] / b + \
        #                      radio_parameters['frequency'][0]
        # peaks[m] = abs(spectrum).max(1)
        # index = abs(spectrum[0]).argmax()
        # freqs[m] = spectrum_frequency[index]
        # index = abs(spectrum_frequency - df_frequencies[m]).argmin()
        # peaks[m] = spectrum[:, index]
        # freqs[m] = spectrum_frequency[index]
        center_freqs[m] = radio_parameters['frequency'][0]

    cf = np.unique(center_freqs)
    step_size = len(frequency_list) + len(cf)

    list_of_results = []
    cnt = 0
    for m in range(step_size):
        index = np.arange(m, len(center_freqs), step_size)
        if cal_flag:
            if cal_data[index[0]]:
                list_of_results.append(FrequencyResult(yaw[index], pitch[index], roll[index], timestamps[index],
                                                       np.array(file_list)[index], peaks[index], center_freqs[index][0],
                                                       df_frequencies[index][0], cal_data[index], mag_tweaks[index],
                                                       phase_tweaks[index]))
        else:
            if not cal_data[index[0]]:
                list_of_results.append(FrequencyResult(yaw[index], pitch[index], roll[index], timestamps[index],
                                                       np.array(file_list)[index], peaks[index], center_freqs[index][0],
                                                       frequency_list[cnt], cal_data[index], mag_tweaks[index],
                                                       phase_tweaks[index]))
                cnt += 1
        if len(list_of_results) > 0:
            list_of_results[-1].results.update({'latitudes': lats[index], 'longitudes': lons[index]})

    for result in list_of_results:
        result.compute_aoa()

    return list_of_results


def write_calibration_files(list_of_results, tx_lat, tx_lon, antenna='4inCyl002', platform='pedestal',
                            technique='linear'):

    pos_est = CalibratePlatform(platform, antenna, tx_lat, tx_lon)
    t_aoa = None
    for m, result in enumerate(list_of_results):
        if result.results['monotonic_aoa']:
            t_aoa, _ = pos_est.compute_true_aoa(list_of_results[m].results['latitudes'],
                                                list_of_results[m].results['longitudes'],
                                                list_of_results[m].results['yaw'])
            aoa1 = list_of_results[m].results['AoA'].copy()
            err = aoa1 - t_aoa
            err[err < -180] += 360
            err[err > 180] -= 360
            if abs(err).max() < 90:
                freqs = np.array([list_of_results[m].results['df_frequency']] * len(aoa1))
                f1, cv = compute_correction_vector(freqs, aoa1, t_aoa, technique)
                lut = cv[0](np.arange(360)) + np.arange(360)
                filename = '{0}_{1:5.3f}_{2}.yml'.format(antenna, (f1[0] / 1e6), platform)
                write_simple_table(filename, 'angleCal', lut)
            else:
                print("BAD CAL MATCH with Freq: {}, Max Error: {}\deg".format(result.results['df_frequency'],
                                                                              abs(err.max())))
        else:
            print("AoAs are not monotonic at Freq: {}".format(result.results['df_frequency']))

    return t_aoa


def vna(list_of_results, position, port, reference):

    n = len(list_of_results)
    vector = np.zeros(n, np.complex128)
    f = np.zeros(n)

    for m, result in enumerate(list_of_results):
        try:
            vector[m] = result.results['peaks'][position][port]
            ref = result.results['peaks'][position][reference]
        except IndexError:
            vector[m] = 0
            ref = np.ones(1, np.complex128)
        vector[m] *= ref.conj() / abs(ref)
        f[m] = result.results['df_frequency']

    return f, vector


def get_impulse_response(list_of_results, position, port, reference, fs, df):

    f1 = np.zeros(len(list_of_results))
    for m in range(len(list_of_results)):
        f1[m] = list_of_results[m].results['df_frequency']

    f2 = np.arange(0, fs, df)
    n = len(f2)
    index = abs(f2 - f1[0]).argmin()
    i2 = np.arange(index, index + len(f1))

    data = np.zeros(n, np.complex128)
    _, v = vna(list_of_results, position, port, reference)
    data[i2] = v
    data[n // 2 + 1:] = np.flipud(data[1:n // 2].conj())
    imp = np.fft.ifft(data)

    return imp


def time_gated_frequency_domain(list_of_results, ports, reference, fs, df):

    f1 = np.zeros(len(list_of_results))
    for m in range(len(list_of_results)):
        f1[m] = list_of_results[m].results['df_frequency']

    f2 = np.arange(0, fs, df)
    n = len(f2)
    index = abs(f2 - f1[0]).argmin()
    i2 = np.arange(index, index + len(f1))

    x = len(list_of_results[0].results['AoA'])
    data = np.zeros(n, np.complex128)
    f_domain = np.zeros((len(ports), x, 16000), np.complex128)
    for m in range(x):
        for id1, port in enumerate(ports):
            print("position: {0}, port: {1}".format(m, port))
            _, v = vna(list_of_results, m, port, reference)
            data[i2] = v  # * np.hanning(len(v))
            data[n // 2 + 1:] = np.flipud(data[1:n // 2].conj())
            owt = np.fft.ifft(data)
            i1 = abs(owt).argmax()
            my_time = np.zeros(n)
            my_time[i1 - 12: i1 + 13] = owt[i1 - 12:i1 + 13].real * np.hanning(25)
            f_domain[id1, m] = np.fft.fft(my_time)

    return f2, f_domain





if __name__ == "__main__":
    # frequency_list = 30e6 * pow(1.05, np.arange(72))
    frequency_list = np.array([30.1e6, 33.1e6, 36.1e6, 40.1e6, 44.1e6, 48.1e6, 53.1e6, 58.1e6, 64.1e6, 71.1e6,
                               78.1e6, 86.1e6, 103.9e6, 114.5e6, 127.9e6, 138.25e6, 151.9e6, 167.25e6,
                               182.9e6,199.9e6])
    results = analyze_data_bad_freqs('/home/tim/dfData/APG_STRYKER/2018-11-07_16-15-34/raw_data',
                                     frequency_list=frequency_list)
    # results = analyze_data('/home/tim/workspace/dfData2/G3_1.1inch_park1_9_7_18/')
    # aoa_s = results[1].compute_aoa()
    # aoa_s2 = results[1].compute_aoa(array_index=1)  # Top 2 elements only
