
"""Loggers and utilities related to logging."""

import collections
import html as pyhtml
import json
import logging
import os
import sys

# Optional imports
try:
    import colorama
except ImportError:
    # noinspection SpellCheckingInspection
    colorama = None

COLORS = ['black', 'red', 'green', 'yellow', 'blue', 'purple', 'cyan', 'white']
COLOR_ESCAPES = {color: '\033[{}m'.format(i)
                 for i, color in enumerate(COLORS, start=30)}
RESET_ESCAPE = '\033[0m'


# Log formats to use.
# noinspection SpellCheckingInspection
SIMPLE_FMT = ('{green}{asctime:8}{reset} {log_color}{levelname}{reset}: '
              '{message}')
# noinspection SpellCheckingInspection
EXTENDED_FMT = ('\r'
                '{green}{asctime:8}.{msecs:03.0f}{reset}|'
                '{log_color}{levelname:1.1}{reset}|'
                '{cyan}'
                '{name:>19.19}'
                # '{module}:{funcName}:{lineno}'
                '{reset}|'
                '{log_color}{message}{reset}')
# noinspection SpellCheckingInspection
EXTENDED_FMT_HTML = (
    '<tr>'
    '<td><pre>%(green)s%(asctime)-8s%(reset)s</pre></td>'
    '<td><pre>%(log_color)s%(levelname)-8s%(reset)s</pre></td>'
    '<td></pre>%(cyan)s%(name)-10s</pre></td>'
    '<td><pre>%(cyan)s%(module)s:%(funcName)s:%(lineno)s%(reset)s</pre></td>'
    '<td><pre>%(log_color)s%(message)s%(reset)s</pre></td>'
    '</tr>'
)
# noinspection SpellCheckingInspection
DATEFMT = '%H:%M:%S'
LOG_COLORS = {
    'DEBUG': 'blue',
    'INFO': 'green',
    'WARNING': 'yellow',
    'ERROR': 'red',
    'CRITICAL': 'purple',
}


ram_handler = None  # type: logging.Handler
console_handler = None  # type: logging.Handler
console_filter = None  # type: logging.Filter


def init_log(level=None, color=True, force_color=None,
             json_logging=None, max_lines=2000, log_filter=None):
    root = logging.getLogger()
    if not len(root.handlers):
        log_level = level.upper() if level else 'INFO'
        try:
            numeric_level = getattr(logging, log_level)
        except AttributeError:
            raise ValueError("Invalid log level: {}".format(level))

        console, ram = _init_handlers(numeric_level, color, force_color,
                                      json_logging, max_lines)
        global console_filter
        if console is not None:
            if log_filter is not None:
                console_filter = LogFilter(log_filter.split(','))
                console.addFilter(console_filter)
            root.addHandler(console)
        if ram is not None:
            root.addHandler(ram)
        root.setLevel(logging.NOTSET)
        return True
    return False


def _init_handlers(level, color, force_color, json_logging, ram_capacity):
    """Init log handlers.

    Args:
        level: The numeric logging level.
        color: Whether to use color if available.
        force_color: Force colored output.
        json_logging: Output log lines in JSON (this disables all colors).
    """
    global ram_handler
    global console_handler
    console_fmt, ram_fmt, html_fmt, use_colorama = _init_formatters(
        level, color, force_color, json_logging)

    if sys.stderr is None:
        console_handler = None
    else:
        strip = False if force_color else None
        if use_colorama:
            stream = colorama.AnsiToWin32(sys.stderr, strip=strip)
        elif check_pycharm():
            stream = sys.stdout
        else:
            stream = sys.stderr
        console_handler = logging.StreamHandler(stream)
        console_handler.setLevel(level)
        console_handler.setFormatter(console_fmt)

    if ram_capacity == 0:
        ram_handler = None
    else:
        ram_handler = RAMHandler(capacity=ram_capacity)
        ram_handler.setLevel(logging.NOTSET)
        ram_handler.setFormatter(ram_fmt)
        ram_handler.html_formatter = html_fmt

    return console_handler, ram_handler


def get_console_format(level):
    """Get the log format the console logger should use.

    Args:
        level: The numeric logging level.

    Return:
        Format of the requested level.
    """
    return EXTENDED_FMT if level <= logging.INFO else SIMPLE_FMT


def set_console_level(level):
    set_console_formatter(level)
    console_handler.setLevel(level)


def get_console_level():
    return console_handler.level


def set_console_formatter(level):
    """Change console formatter based on level.

    Args:
        level: The numeric logging level
    """
    use_colors = console_handler.formatter.use_colors
    console_fmt = get_console_format(level)
    console_formatter = ColoredFormatter(console_fmt, DATEFMT, '{',
                                         use_colors=use_colors)
    console_handler.setFormatter(console_formatter)


def _init_formatters(level, color, force_color, json_logging):
    """Init log formatters.

    Args:
        level: The numeric logging level.
        color: Whether to use color if available.
        force_color: Force colored output.
        json_logging: Format lines as JSON (disables all color).

    Return:
        A (console_formatter, ram_formatter, use_colorama) tuple.
        console_formatter/ram_formatter: logging.Formatter instances.
        use_colorama: Whether to use colorama.
    """
    console_fmt = get_console_format(level)
    ram_formatter = ColoredFormatter(EXTENDED_FMT, DATEFMT, '{',
                                     use_colors=False)
    html_formatter = HTMLFormatter(EXTENDED_FMT_HTML, DATEFMT,
                                   log_colors=LOG_COLORS)
    if sys.stderr is None:
        return None, ram_formatter, html_formatter, False

    if json_logging:
        console_formatter = JSONFormatter()
        return console_formatter, ram_formatter, html_formatter, False

    use_colorama = False
    color_supported = os.name == 'posix' or colorama

    if color_supported and (sys.stderr.isatty() or force_color or check_pycharm()) and color:
        use_colors = True
        if colorama and os.name != 'posix':
            use_colorama = True
    else:
        use_colors = False

    console_formatter = ColoredFormatter(console_fmt, DATEFMT, '{',
                                         use_colors=use_colors)
    return console_formatter, ram_formatter, html_formatter, use_colorama


class LogFilter(logging.Filter):

    """Filter to filter log records based on the commandline argument.

    The default Filter only supports one name to show - we support a
    comma-separated list instead.

    Attributes:
        names: A list of names that should be logged.
    """

    def __init__(self, names):
        super().__init__()
        self.names = names

    def filter(self, record):
        """Determine if the specified record is to be logged."""
        if self.names is None:
            return True
        if record.levelno > logging.DEBUG:
            # More important than DEBUG, so we won't filter at all
            return True
        for name in self.names:
            if record.name == name:
                return True
            elif not record.name.startswith(name):
                continue
            elif record.name[len(name)] == '.':
                return True
        return False


class RAMHandler(logging.Handler):

    """Logging handler which keeps the messages in a deque in RAM.

    Loosely based on logging.BufferingHandler which is unsuitable because it
    uses a simple list rather than a deque.

    Attributes:
        _data: A deque containing the logging records.
    """

    def __init__(self, capacity):
        super().__init__()
        self.html_formatter = None
        if capacity != -1:
            self._data = collections.deque(maxlen=capacity)
        else:
            self._data = collections.deque()

    def emit(self, record):
        if record.levelno >= logging.DEBUG:
            self._data.append(record)

    def dump_log(self, html=False, level=None):
        """Dump the complete formatted log data as string.

        """
        try:
            min_level = getattr(logging, level.upper())
        except AttributeError:
            min_level = logging.INFO
        lines = []
        fmt = self.html_formatter.format if html else self.format
        self.acquire()
        try:
            records = list(self._data)
        finally:
            self.release()
        for record in records:
            if record.levelno >= min_level:
                lines.append(fmt(record))
        return '\n'.join(lines)

    def change_log_capacity(self, capacity):
        self._data = collections.deque(self._data, maxlen=capacity)


class ColoredFormatter(logging.Formatter):

    """Logging formatter to output colored logs.

    Attributes:
        use_colors: Whether to do colored logging or not.
    """
    # '*' sputs syntax error 
    # def __init__(self, fmt, datefmt, style, *, use_colors):
    def __init__(self, fmt, datefmt, style, use_colors):
        super(ColoredFormatter, self).__init__(fmt, datefmt, style)
        self.use_colors = use_colors

    def format(self, record):
        if self.use_colors:
            color_dict = dict(COLOR_ESCAPES)
            color_dict['reset'] = RESET_ESCAPE
            log_color = LOG_COLORS[record.levelname]
            color_dict['log_color'] = COLOR_ESCAPES[log_color]
        else:
            color_dict = {color: '' for color in COLOR_ESCAPES}
            color_dict['reset'] = ''
            color_dict['log_color'] = ''
        record.__dict__.update(color_dict)
        return super(ColoredFormatter, self).format(record)


class HTMLFormatter(logging.Formatter):

    """Formatter for HTML-colored log messages.

    Attributes:
        _log_colors: The colors to use for logging levels.
        _colordict: The colordict passed to the logger.
    """

    def __init__(self, fmt, datefmt, log_colors):
        """Constructor.

        Args:
            fmt: The format string to use.
            datefmt: The date format to use.
            log_colors: The colors to use for logging levels.
        """
        super().__init__(fmt, datefmt)
        self._log_colors = log_colors
        self._colordict = {}
        # We could solve this nicer by using CSS, but for this simple case this
        # works.
        for color in COLORS:
            self._colordict[color] = '<font color="{}">'.format(color)
        self._colordict['reset'] = '</font>'

    def format(self, record):
        record.__dict__.update(self._colordict)
        if record.levelname in self._log_colors:
            color = self._log_colors[record.levelname]
            record.log_color = self._colordict[color]
        else:
            record.log_color = ''
        for field in ['msg', 'filename', 'funcName', 'levelname', 'module',
                      'name', 'pathname', 'processName', 'threadName']:
            data = str(getattr(record, field))
            setattr(record, field, pyhtml.escape(data))
        msg = super().format(record)
        if not msg.endswith(self._colordict['reset']):
            msg += self._colordict['reset']
        return msg

    def formatTime(self, record, datefmt=None):
        out = super().formatTime(record, datefmt)
        return pyhtml.escape(out)


class JSONFormatter(logging.Formatter):

    """Formatter for JSON-encoded log messages."""

    def format(self, record):
        obj = {}
        for field in ['created', 'msecs', 'levelname', 'name', 'module',
                      'funcName', 'lineno', 'levelno']:
            obj[field] = getattr(record, field)
        obj['message'] = record.getMessage()
        if record.exc_info is not None:
            obj['traceback'] = super().formatException(record.exc_info)
        return json.dumps(obj)


def check_pycharm():
    # Pycharm makes all stderr RED...so change to stdout...Requires setting ENV:
    # http://stackoverflow.com/a/29782618/3571110
    return 'PYCHARM_HOSTED' in os.environ
