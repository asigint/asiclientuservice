# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

import socket
import errno
from AsiUtilities.JsonStdTypesEncoder import JsonStdTypesEncoder
from six import string_types
import logging


class SocketManagerUDP(object):
    def __init__(self, logger=None):
        if logger is None:
            self.logger_sock = logging.getLogger(name='SocketManagerUDP')
        else:
            self.logger_sock = logger

        self.socket_dict = {}
        """:type: dict[tuple, socket.socket]"""

    def socket_add(self, addr, ttl=1):
        """
        Add a socket connection to the manager
        :param addr: (address, port) or "address:port" format
        :type addr: tuple | string_types
        :param ttl: time-to-live for multicast (default=1)
        :type ttl: int
        :return: Success or Failure for add operation
        :rtype: bool
        """
        # Validate address is in address family format
        addr = self._to_addr_family(addr)
        if addr is not None and addr not in self.socket_dict:
            sock = self._socket_open(addr[0], ttl=ttl)
            if sock is not None:
                self.socket_dict[addr] = sock
                return True
            return False
        else:
            # Already in dict...nothing to do
            return True

    def socket_remove(self, addr):
        # Validate address is in address family format
        addr = self._to_addr_family(addr)
        try:
            self._socket_close(self.socket_dict.pop(addr))
            return True
        except Exception as err:
            self.logger_sock.warning("Failed graceful close of {} - {}: {}".format(addr, type(err).__name__, err))
            return False

    def socket_remove_all(self):
        for addr in self.socket_dict.keys():
            self.socket_remove(addr)

    def socket_list(self):
        return self.socket_dict.keys()

    def socket_sendto(self, data):
        if not isinstance(data, string_types):
            data = JsonStdTypesEncoder.dumps(data).encode()
        for dest, sock in self.socket_dict.items():
            try:
                sock.sendto(data, dest)
            except TypeError as err:
                self.logger_sock.warning("SOCKET TypeError: {}".format(err))
            except IOError as err:
                if err.errno == errno.ENETUNREACH:
                    self.logger_sock.warning("SOCKET ERROR - NETWORK UNREACHABLE: {}".format(dest))
                    self.logger_sock.warning("ENABLING LOCALHOST MULTICAST")
                    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_IF, socket.inet_aton("127.0.0.1"))
                else:
                    self.logger_sock.warning("SOCKET ERROR: {}".format(err))

    def socket_enable_loopback_multicast(self):
        self.logger_sock.warning("ENABLING LOCALHOST MULTICAST")
        socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP).setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_IF, socket.inet_aton("127.0.0.1"))

    def _socket_open(self, address, ttl=1):
        if isinstance(address, str):
            try:
                # VALIDATE ADDRESS
                socket.inet_aton(address)

                # Create the new socket (UDP Generic)
                sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
                sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
                if hasattr(socket, 'SO_REUSEPORT'):
                    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, True)
                # Type Specific
                if address.split('.')[3] == '255':
                    # BROADCAST
                    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, True)
                elif int(address.split('.')[0]) > 224:
                    # MULTICAST
                    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)
                    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_LOOP, True)
                else:
                    # UNICAST
                    pass
                return sock
            except IOError as err:
                self.logger_sock.warning("SOCKET ERROR - {}: {}".format(type(err).__name__, err))
            except Exception as err:
                self.logger_sock.warning("{}: {}".format(type(err).__name__, err))
        return None

    @staticmethod
    def _socket_close(sock):
        try:
            sock.shutdown(socket.SHUT_RDWR)
            sock.close()
        except IOError:
            pass

    def _to_addr_family(self, dest):
        """
        Return only a valid ('host', port) tuple or None
        """
        # Validate input type
        if isinstance(dest, tuple):
            if len(dest) == 2:
                dest = '{}:{}'.format(dest[0], dest[1])
            else:
                dest = None
        if isinstance(dest, string_types):
            host, port = SocketManagerUDP.addr_str_to_tuple(dest)
        else:
            self.logger_sock.warning("INVALID DESTINATION ADDRESS")
            return None
        if host and port:
            return host, port
        else:
            return None

    @staticmethod
    def addr_str_to_tuple(ip_str):
        """
        Convert a string in the form '192.168.0.1:7000' into a string, int tuple ('192.168.0.1', 7000)
        Validate the address in the process returning None for invalid address

        :param ip_str: 'host:port' string ('host' only or ':port' only accepted)
        :type ip_str: string_types
        :return: host, port
        :rtype: string_types | None, int | None
        """
        host = None
        port = None
        if isinstance(ip_str, string_types):
            # cast as str() to protect against unicode/string/bytestring troubles
            addr_pair = str(ip_str).split(':')
            try:
                try:
                    socket.inet_pton(socket.AF_INET, addr_pair[0])
                except AttributeError:
                    # IPv6 inet_pton() not supported
                    socket.inet_aton(addr_pair[0])
                if addr_pair[0].count('.') == 3:
                    # inet_pton/aton will accept partial address
                    host = addr_pair[0]
            except IOError:
                pass
            try:
                p = int(addr_pair[1])
                if 0 <= p <= 65535:
                    port = p
                    if p < 1024:
                        print('PRIVILEGED PORT SPECIFIED')
            except (IndexError, AttributeError, ValueError):
                pass
        return host, port


if __name__ == '__main__':
    from time import sleep
    from datetime import datetime

    s = SocketManagerUDP()
    try:
        while True:
            s.socket_sendto(str(datetime.now()))
            sleep(.1)
    except (KeyboardInterrupt, SystemExit):
        print("FINISHING")
