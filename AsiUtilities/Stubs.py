#!/usr/bin/env python3

import numpy as np
from os import getenv
from AsiUtilities.SettingsManager import SettingsManager
from RadioControl.AsiRadioApiAbc import AsiRadioApiAbc


# noinspection PyPep8Naming,SpellCheckingInspection
class RadioStub(AsiRadioApiAbc):
    """Execute Radio functionality in a stub process.

    Replaces the same named class in df_calibration.py. Provides stubbed methods simulating control of the radio.

    Methods exposed: set_radio_host, set_radio_port, set_frequency, get_frequency, get_time_domain_data and
        get_frequency_domain_data.
    """

    def __init__(self, host=None, port=None):
        """
        Constructed with no parameters
        """
        if host is not None:
            self.radio_host = host
            self.radio_port = port
        else:
            # Load Defaults
            self.settings_user_dir = getenv('ASI_USER_SETTINGS_DIR', '/opt/asi/settings')
            self.settings_manager = SettingsManager(user_file_path=self.settings_user_dir)
            self.radio_host = self.settings_manager.get_settings('radio_host')
            self.radio_port = self.settings_manager.get_settings('radio_port')

        self.frequency = 100.0e6
        self.number_of_channels = 8

        self.radio_parameters = dict(data_rate=51.2e6, bandwidth=40e6,
                                     frequency=np.full(self.number_of_channels, self.frequency, np.float32),
                                     calibration_freq=None, collect_time=0.00016,
                                     attenuation=[0] * self.number_of_channels, serial_number=None, model_number=None,
                                     hf_flag=np.full(self.number_of_channels, False), cal_data=False)

        print(self.radio_host)
        print(self.radio_port)

    def get_serial_number(self):
        """ Get the radio serial number

        :return: Serial number
        """
        return 'S/N 111222333'

    def get_model_number(self):
        """ Get the radio model number

        :return: model number
        """
        return 'MXX777 Receiver'

    def set_radio_host(self, h):
        """ Set the radio address

        :param h: The radio host name or address
        :return: None
        """
        self.radio_host = h
        self.settings_manager.set_settings(**dict(radio_host=h))

    def set_radio_port(self, p):
        """ Set the radio port

        :param p: The radio port number
        :return: None
        """
        self.radio_port = p
        self.settings_manager.set_settings(**dict(radio_port=p))

    def set_frequency(self, f, force_hf=False):
        """ Set the radio tuner frequency

        :param f: frequency in Hz
        :return: True on success or False
        """
        self.frequency = f
        self.force_hf = force_hf
        return self.frequency

    def get_frequency(self):
        """ Get the current radio tuned frequency

        :return: Local tuned frequency
        """
        return self.frequency

    def get_iq_data(self):
        """ Get the current radio tuned frequency

        :return: Local tuned frequency
        """
        pass

    def get_time_domain_data(self):
        """ Return dataset of current digitized rf in the time domain

        :return: someTDD
        """
        someTDD = "Some Time Domain Data"
        return someTDD

    def get_frequency_domain_data(self):
        """ Return dataset of current digitized rf in the frequency domain

        :return: someFDD
        """
        someFDD = "Some Frequency Domain Data"
        val1 = 'Value 1'
        return val1, someFDD

    def get_radio_state(self):
        """ Return radio state

        :return: radio_state
        """
        return self.radio_parameters

    def get_resolution_bandwidth(self):
        return (1/.0016)

    # def setDataAutoLog(self, flag): # At app layer

    # def getDataAutoLog(self): # At app layer

    # def getDFresults(self): # DF Engine

    # def getNavigation(self): # DF Engine


# noinspection PyPep8Naming
class Signal_generator_stub(object):
    """ Execute SignalGenerator functionality in a faux process.

    Replaces the same named class in df_calibration.py. Provides stubbed methods simulating control of the synthesizer
    and on board attenuators.

    Methods exposed: signal_off, signal_on, set_attenuation, set_frequency

    """

    def __init__(self):
        """
        Constructed with no parameters
        """
        self.sg_parameters = dict(powerOn=False, attenuation=0, frequency=100e6, dac_index=None)

    def initialize(self):
        pass

    @staticmethod
    def is_initialized():
        return True

    def signal_off(self):
        """Turn transmit power off

        :return: The parameter just set
        """
        self.sg_parameters['powerOn'] = False
        return self.sg_parameters['powerOn']

    def signal_on(self):
        """Turn transmit power on

        :return: The parameter just set
        """
        self.sg_parameters['powerOn'] = True
        return self.sg_parameters['powerOn']

    def set_attenuation(self, attenuation=0):
        """Set the 2 onboard attenuators

        :param attenuation: (default 0)
        :return: the just set attenuation parameter
        """
        if attenuation > 0:
            attenuation = 0

        self.sg_parameters['attenuation'] = attenuation
        return self.sg_parameters['attenuation']

    def set_frequency(self, frequency=100e6):
        """Set the synthesizer frequency

        :param frequency: (default 100MHz
        :return: The just set frequency parameter
        """
        self.sg_parameters['frequency'] = frequency
        return self.sg_parameters['frequency']

    def set_frequency_atten(self, freq=10e6, atten=0):
        """ Set the frequency and attenuation

        :param freq:  (default 10MHz)
        :param atten: (default 0 dB)
        :return: True if parameter is set
        """
        self.signal_off()
        self.set_frequency(freq)
        self.set_attenuation(atten)
        self.signal_on()
        return True

    def set_receiver_cal(self, flags=True):
        self.sg_parameters['receiver_cal'] = flags
        return self.sg_parameters['receiver_cal']

    def close_interfaces(self):
        return True


# noinspection PyPep8Naming
class Turn_table_stub(object):

    def __init__(self):
        self.turn_table_parameters = dict(angle=360, moving=False, clockwise=True)

    def mrt10setAngle(self, value=360):
        self.turn_table_parameters['angle'] = value
        return self.turn_table_parameters['angle']

    def mrt10move(self):
        self.turn_table_parameters['moving'] = True
        self.turn_table_parameters['moving'] = False
        return True

    def mrt10setClockwise(self):
        self.turn_table_parameters['clockwise'] = True
        return self.turn_table_parameters['clockwise']

    def mrt10setCounterClockwise(self):
        self.turn_table_parameters['clockwise'] = False
        return self.turn_table_parameters['clockwise']
