#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

import errno
import json
import logging
import os
import queue
import select
import socket
import threading
import traceback
from pprint import pformat

from colorama import Fore
from AsiUtilities.netstring import ns_decode


class SimpleSocketClient(threading.Thread):
    def __init__(self, queue_from_server=None, queue_to_server=None,
                 ip_addr='', ip_port=10000, netstring=False):
        """
        Simple Socket Client

        :param queue_from_server:
        :type queue_from_server: queue.Queue | None
        :param queue_to_server:
        :type queue_to_server: queue.Queue | None
        :param ip_addr: Server IP address
        :type ip_addr: str
        :param ip_port: Server IP port number for incoming connections (default=10000)
        :type ip_port: int
        """
        self.logger = logging.getLogger(name=self.__class__.__name__)
        super(SimpleSocketClient, self).__init__()
        self._stop_request = threading.Event()

        self.queue_from_server = queue_from_server
        self.queue_to_server = queue_to_server

        self.client = None
        """:type : socket.socket | None"""
        self.server_max_rx_buffer = 65536  # MAXBUFFER=65536 for IP Packets
        # set address and port...prevent values being None
        self.server_ip_addr = ip_addr if ip_addr else ''
        self.server_ip_port = ip_port if ip_port else 10000

        self.select_timeout = 1  # Timeout in seconds
        self.client_failed_log_suppress = False  # Used to suppress repeat failure logs

        self.netstring = netstring
        self.data = ''

    def _create_client(self):
        # Create a TCP/IP socket
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
        self.client.settimeout(5)
        try:
            self.client.connect((self.server_ip_addr, self.server_ip_port))
            self.client_failed_log_suppress = False
            self.logger.info("{}:{} <- Started".format(*self.client.getsockname()))
        except (IOError, OSError) as err:
            if not self.client_failed_log_suppress:
                if err.errno == errno.ECONNREFUSED:
                    msg = 'REFUSED'
                else:
                    msg = 'FAILED'
                self.logger.critical("{}:{} <- {}".format(self.server_ip_addr,
                                                          self.server_ip_port,
                                                          msg))
                self.client_failed_log_suppress = True
            self._close_client()
            # Wait 5 sec before trying again
            self._stop_request.wait(5)

    def _close_client(self):
        if self.client is not None:
            self.client.close()
            self.client = None

    def run(self):
        while not self._stop_request.is_set():
            if self.client is None:
                self._create_client()
                continue
            try:
                while not self._stop_request.is_set() and self.client is not None:
                    # Check status of sockets
                    outputs = [self.client]
                    inputs = outputs
                    readable, writable, exceptional = select.select(inputs, outputs, inputs, self.select_timeout)

                    # Check timeout condition (aka nothing to do)
                    if not (readable or writable or exceptional):
                        self._stop_request.wait(0.01)
                        continue

                    # Handle inputs
                    if readable:
                        data = None
                        try:
                            data = self.client.recv(self.server_max_rx_buffer)
                        except (IOError, OSError) as err:
                            if err.errno == errno.ECONNRESET or err.errno == errno.EINVAL \
                                    or err.errno == errno.EHOSTUNREACH:
                                # Windows causes a Connection Reset on Telnet Quit
                                # Suspend/Resume can cause Invalid Argument
                                # Host Disconnect can cause No Route to Host (aka Host Unreachable)
                                self.logger.warning("{0}:{1} <- Connection Reset".format(self.server_ip_addr,
                                                                                         self.server_ip_port))
                            else:
                                self.logger.error("Socket Error: {}".format(err))
                                for err_data in traceback.extract_tb(sys.exc_info()[2], limit=3):
                                    filename, line_no, function_name, line_text = err_data
                                    self.logger.debug(
                                        "{}:{} in {}".format(os.path.basename(filename), line_no, function_name))
                        if data:
                            data = data.decode('utf-8')
                            if self.netstring:
                                data = self.process_netstring(data)
                            else:
                                data = [data]
                            for rx_string in data:
                                self.process_data(rx_string)
                        else:
                            # Interpret empty result as closed connection
                            self._close_client()

                    # Handle outputs
                    try:
                        next_msg = self.queue_to_server.get_nowait()
                        if not isinstance(next_msg, str):
                            next_msg = str(next_msg)
                    except (queue.Empty, AttributeError):
                        pass
                    else:
                        if writable:
                            try:
                                if self.client is not None:
                                    self.client.send(next_msg.encode())
                            except (IOError, OSError) as err:
                                if err.errno == errno.EHOSTUNREACH:
                                    self.logger.warning("{0}:{1} <- No route to host".format(self.server_ip_addr,
                                                                                             self.server_ip_port))
                                if err.errno == errno.EBADF:
                                    self.logger.warning("{0}:{1} <- Socket Lost".format(self.server_ip_addr,
                                                                                        self.server_ip_port))
                                else:
                                    self.logger.critical("Socket Error: {}".format(err))
                                    for err_data in traceback.extract_tb(sys.exc_info()[2], limit=3):
                                        filename, line_no, function_name, line_text = err_data
                                        self.logger.debug(
                                            "{}:{} in {}".format(os.path.basename(filename), line_no, function_name))
                                self._close_client()

                    # Handle "exceptional conditions"
                    if exceptional:
                        self._close_client()
                        self.logger.warning("{0}:{1} <- Socket Exception".format(self.server_ip_addr,
                                                                                 self.server_ip_port))
                    self._stop_request.wait(0.001)

            except (IOError, OSError) as err:
                self.logger.critical("Socket Error: {}".format(err))
                for err_data in traceback.extract_tb(sys.exc_info()[2], limit=3):
                    filename, line_no, function_name, line_text = err_data
                    self.logger.debug("{}:{} in {}".format(os.path.basename(filename), line_no, function_name))
                self._close_client()
        self._close_client()

    def process_netstring(self, message):
        rx_string = self.data + message
        netstrings, residual = ns_decode(rx_string)
        self.data = residual
        return netstrings

    def process_data(self, message):
        if self.queue_from_server is not None:
            self.queue_from_server.put(message)
        else:
            print("{}{}{}".format(Fore.RED, pformat(self.server_ip_addr), Fore.RESET))
            print("{}{}{}".format(Fore.GREEN, pformat(message), Fore.RESET))

    def stop(self):
        self._stop_request.set()

    def is_stopped(self):
        return self._stop_request.is_set()


class DfStatusReceiver(SimpleSocketClient):
    def __init__(self, app_config):
        self.application_config = app_config
        super(DfStatusReceiver, self).__init__(ip_addr=app_config['ip_addr'],
                                               ip_port=app_config['ip_port'],
                                               netstring=True)

    def process_data(self, message):
        print("{}{}{}".format(Fore.RED, pformat(self.server_ip_addr), Fore.RESET))
        try:
            msg = json.loads(message)
            if self.application_config['geotag_only']:
                print("{}{}{}".format(Fore.GREEN, pformat(msg['GEOTAG']), Fore.RESET))
            elif self.application_config['status_only']:
                print("{}{}{}".format(Fore.GREEN, pformat(msg['STATUS']), Fore.RESET))
            else:
                print("{}{}{}".format(Fore.GREEN, pformat(msg), Fore.RESET))
        except ValueError:
            print("FAILED TO PARSE AS JSON:")
            print(message.decode('utf-8'))
            self.stop()


def add_args(config):
    from argparse import ArgumentParser
    """Adds commandline arguments and formatted Help"""
    parser = ArgumentParser(description=os.path.basename(__file__))
    parser.add_argument('--ip_port',
                        help="CarteNav Server Port (default={})".format(config['ip_port']),
                        type=int,
                        default=config['ip_port'])
    parser.add_argument('--ip_addr',
                        help="CarteNav Server Address (default={})".format(config['ip_addr']),
                        type=str,
                        default=config['ip_addr'])
    parser.add_argument('--raw',
                        help="Show Raw Data",
                        action='store_true',
                        default=config['raw'])
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--geotag_only',
                       help="Show GEOTAG only",
                       action='store_true',
                       default=config['geotag_only'])
    group.add_argument('--status_only',
                       help="Show STATUS only",
                       action='store_true',
                       default=config['status_only'])
    args = parser.parse_args()
    for option in config.keys():
        config[option] = getattr(args, option, config[option])
    return config


def main():
    import pprint
    import os
    from AsiUtilities import log

    application_config = dict(
        ip_addr='127.0.0.1',
        ip_port=12000,
        raw=False,
        geotag_only=False,
        status_only=False,
    )

    log.init_log('DEBUG')
    logger = logging.getLogger(name=os.path.basename(__file__))
    application_config = add_args(application_config)
    logger.debug('CONFIG:\n' + pprint.pformat(application_config))

    if application_config['raw']:
        server = SimpleSocketClient(ip_addr=application_config['ip_addr'],
                                    ip_port=application_config['ip_port'])
    else:
        server = DfStatusReceiver(application_config)
    server.start()

    try:
        while server.is_alive():
            server.join(1)
    except (KeyboardInterrupt, SystemExit):
        server.stop()
        server.join(5)


if __name__ == '__main__':
    import signal
    import sys
    from os.path import basename

    from setproctitle import setproctitle

    setproctitle(basename(__file__))
    # Catch SIGTERM as SystemExit
    signal.signal(signal.SIGTERM, lambda signum, stack_frame: sys.exit(0))

    main()
