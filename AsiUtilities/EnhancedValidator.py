# -*- coding: utf-8 -*-
# Copyright (C) 2017 Applied Signals Intelligence, Inc. -- All Rights Reserved

from cerberus import Validator


# noinspection PyMethodMayBeStatic
class EnhancedValidator(Validator):
    def __init__(self, *args, **kwargs):
        super(EnhancedValidator, self).__init__(*args, **kwargs)

    def _normalize_coerce_list(self, variable):
        if isinstance(variable, (str, float, int, bool)):
            variable = [variable]
        if isinstance(variable, list):
            return variable
        return []

    # The below are to allow yaml.safe_load
    def _normalize_coerce_str(self, variable):
        return str(variable)

    def _normalize_coerce_int(self, variable):
        return int(variable)

    def _normalize_coerce_float(self, variable):
        return float(variable)

    def _normalize_coerce_bool(self, variable):
        return bool(variable)