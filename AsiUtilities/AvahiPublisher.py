import dbus
import avahi_util


class AvahiPublisher(object):
    """A simple class to publish a network service with zeroconf using
    

    """

    def __init__(self, name, port, stype="_http._tcp",
                 domain="", host="", text=""):
        self.name = name
        self.stype = stype
        self.domain = domain
        self.host = host
        self.port = port
        self.text = text

    def publish(self):
        bus = dbus.SystemBus()
        avahi = bus.get_object(avahi_util.DBUS_NAME,
                               avahi_util.DBUS_PATH_SERVER)
        server = dbus.Interface(avahi, avahi_util.DBUS_INTERFACE_SERVER)

        g = dbus.Interface(
            bus.get_object(avahi_util.DBUS_NAME,
                           server.EntryGroupNew()),
            avahi_util.DBUS_INTERFACE_ENTRY_GROUP)

        g.AddService(avahi_util.IF_UNSPEC, avahi_util.PROTO_UNSPEC, dbus.UInt32(0),
                     self.name, self.stype, self.domain, self.host,
                     dbus.UInt16(self.port), self.text)

        g.Commit()
        self.group = g

    def unpublish(self):
        self.group.Reset()


def test():
    service = AvahiPublisher(name="TestService", port=3000)
    service.publish()
    input("Press any key to unpublish the service ")
    service.unpublish()


if __name__ == "__main__":
    test()
