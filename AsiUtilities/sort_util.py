import re


def natural_key(items):
    if isinstance(items, (tuple, list)):
        items=items[0]
    """See http://www.codinghorror.com/blog/archives/001018.html"""
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', items)]

if __name__ == "__main__":
    result = {'tun': {'ham0': {'ipv4': {'gateway': None, 'addresses': {0: {'prefix': 8, 'address': '25.72.163.67', 'family': 2}}, 'method': 'manual', 'dns': []}, 'hw_address': None}}, 'vpn': {'home_fios': {'ipv4': {'gateway': None, 'addresses': {}, 'method': 'auto', 'dns': []}, 'hw_address': None}}, 'bridge': {'virbr0': {'ipv4': {'gateway': None, 'addresses': {0: {'prefix': 24, 'address': '192.168.122.1', 'family': 2}}, 'method': 'manual', 'dns': []}, 'hw_address': None}}, '802-3-ethernet': {'Wired connection 1': {'ipv4': {'gateway': '192.168.50.1', 'addresses': {0: {'prefix': 24, 'address': '192.168.50.240', 'family': 2}}, 'method': 'manual', 'dns': []}, 'hw_address': 'D0:67:E5:47:50:3A'}}, '802-11-wireless': {'Auto LibraryWiFi': {'ipv4': {'gateway': None, 'addresses': {}, 'method': 'auto', 'dns': []}, 'hw_address': '24:77:03:5D:AB:FC'}, 'Auto Audi Chantilly - Guest': {'ipv4': {'gateway': None, 'addresses': {}, 'method': 'auto', 'dns': []}, 'hw_address': '24:77:03:5D:AB:FC'}, 'Auto Valhalla': {'ipv4': {'gateway': None, 'addresses': {}, 'method': 'auto', 'dns': []}, 'hw_address': '24:77:03:5D:AB:FC'}, 'Auto RAGNAROK': {'ipv4': {'gateway': None, 'addresses': {}, 'method': 'auto', 'dns': []}, 'hw_address': '24:77:03:5D:AB:FC'}, 'Auto Sheraton Harrisburg': {'ipv4': {'gateway': None, 'addresses': {}, 'method': 'auto', 'dns': []}, 'hw_address': '24:77:03:5D:AB:FC'}, 'Auto DPIwireless': {'ipv4': {'gateway': None, 'addresses': {}, 'method': 'auto', 'dns': []}, 'hw_address': '24:77:03:5D:AB:FC'}, 'Auto ASI': {'ipv4': {'gateway': None, 'addresses': {}, 'method': 'auto', 'dns': []}, 'hw_address': '24:77:03:5D:AB:FC'}}}
    print(list(result.items()))
    print(sorted(result.items()))
    print(sorted(result.items(), key=natural_key))
