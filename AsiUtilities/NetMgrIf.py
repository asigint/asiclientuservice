# !/usr/bin/env python
#
# http://lazka.github.io/pgi-docs/#NM-1.0
# https://developer.gnome.org/NetworkManager/1.0/ref-settings.html
# Sample code at https://github.com/rhinstaller/anaconda/blob/master/pyanaconda/ui/gui/spokes/network.py
# https://cgit.freedesktop.org/NetworkManager/NetworkManager/tree/examples/python/gi

import socket
import sys
import logging
from threading import Thread

import gevent
import gi
from AsiUtilities import log

gi.require_version('NM', '1.0')
from gi.repository import GLib, NM, GObject  # noqa


# Enable GLib to work with gevent
# https://stackoverflow.com/a/13741120/3571110
def _trigger_loop():
    GLib.timeout_add(10, gevent_loop, priority=GLib.PRIORITY_HIGH)


def gevent_loop():
    gevent.sleep(0.001)
    _trigger_loop()
    return False


def prefix_to_netmask(prefix):
    return '.'.join([str((0xffffffff << (32 - prefix) >> i) & 0xff) for i in [24, 16, 8, 0]])


def netmask_to_prefix(netmask):
    """
    :param netmask: netmask ip addr (eg: 255.255.255.0)
    :return: equivalent cidr number to given netmask ip (eg: 22)
    """
    return sum([bin(int(x)).count('1') for x in netmask.split('.')])


def get_device_wifi(nm_device_wifi):
    """

    :param nm_device_wifi:
    :type nm_device_wifi: NM.DeviceWifi
    :return:
    """

    if not isinstance(nm_device_wifi, NM.DeviceWifi):
        return None
    else:
        active_ap = get_access_point(nm_device_wifi.get_active_access_point())
        return {
            'active_access_point': active_ap,
        }


def get_access_point(nm_access_point):
    """

    :param nm_access_point:
    :type nm_access_point: NM.AccessPoint
    :return:
    """
    if not isinstance(nm_access_point, NM.AccessPoint):
        return None
    else:
        return {
            'bssid': nm_access_point.get_bssid(),
            'frequency': nm_access_point.get_frequency(),
            'last_seen': nm_access_point.get_last_seen(),
            'max_bitrate': nm_access_point.get_max_bitrate(),
            'mode': nm_access_point.get_mode().value_nick,
            'ssid': nm_access_point.get_ssid().get_data().decode(),
            'strength': nm_access_point.get_strength(),
        }


def get_dev_ip_info(dev, family):
    """

    :param dev:
    :type dev: NM.Device
    :param family:
    :type family: int
    :return:
    :rtype: dict
    """
    if family == socket.AF_INET:
        ip_cfg = dev.get_ip4_config()
    else:
        ip_cfg = dev.get_ip6_config()

    if ip_cfg is None:
        return {}

    addresses = []
    nm_addresses = ip_cfg.get_addresses()
    for idx, nm_address in enumerate(nm_addresses):
        addr = nm_address.get_address()
        prefix = nm_address.get_prefix()
        netmask = prefix_to_netmask(prefix)
        addresses.append({
            'address': addr,
            'prefix': prefix,
            'netmask': netmask,
        })
    gateway = ip_cfg.get_gateway()
    domains = ip_cfg.get_domains()
    nameservers = ip_cfg.get_nameservers()
    routes = [get_route_info(r) for r in ip_cfg.get_routes()]
    searches = ip_cfg.get_searches()
    wins = ip_cfg.get_wins_servers()
    details = {
        'gateway': gateway,
        'domains': domains,
        'dns': nameservers,
        'routes': routes,
        'searches': searches,
        'wins': wins,
        'addresses': addresses,
    }
    return details


def get_route_info(nm_iproute):
    """

    :param nm_iproute:
    :type nm_iproute: NM.IPRoute
    :return:
    """

    dst = nm_iproute.get_dest()
    prefix = nm_iproute.get_prefix()
    nh = nm_iproute.get_next_hop()
    mt = nm_iproute.get_metric()

    return {
        'dst': dst,
        'prefix': prefix,
        'nh': nh,
        'mt': mt,
    }


def get_con_ip_cfg(nm_ip_cfg):
    """

    :param nm_ip_cfg:
    :type nm_ip_cfg: NM.SettingIPConfig
    :return:
    :rtype: dict
    """
    method = nm_ip_cfg.get_method()
    addrs = []
    for idx in range(nm_ip_cfg.get_num_addresses()):
        addr = nm_ip_cfg.get_address(idx)  # type: NM.IPAddress
        address = addr.get_address()
        prefix = addr.get_prefix()
        family = addr.get_family()
        addrs.append({
            'address': address,
            'prefix': prefix,
            'family': family,
        })
    gateway = nm_ip_cfg.get_gateway()
    dns = []
    for idx in range(nm_ip_cfg.get_num_dns()):
        nameserver = nm_ip_cfg.get_dns(idx)
        dns.append(nameserver)
    searches = []
    for idx in range(nm_ip_cfg.get_num_dns_searches()):
        item = nm_ip_cfg.get_dns_search(idx)
        searches.append(item)
    routes = []
    for idx in range(nm_ip_cfg.get_num_routes()):
        item = nm_ip_cfg.get_route(idx)
        routes.append(get_route_info(item))

    return {
        'method': method,
        'addresses': addrs,
        'gateway': gateway,
        'dns': dns,
        'dns_searches': searches,
        'routes': routes,
    }


def get_con_wired(nm_con):
    """

    :param nm_con:
    :type nm_con: NM.Connection
    :return:
    """
    if nm_con.get_connection_type() == NM.SETTING_WIRED_SETTING_NAME:
        wired = nm_con.get_setting_wired()
        ''':type: NM.SettingWired'''
        mac = wired.get_mac_address()
        duplex = wired.get_duplex()
        auto = wired.get_auto_negotiate()
        name = wired.get_name()
        port = wired.get_port()
        speed = wired.get_speed()
        mtu = wired.get_mtu()
        return {
            'mac': mac,
            'duplex': duplex,
            'auto': auto,
            'name': name,
            'port': port,
            'speed': speed,
            'mtu': mtu
        }
    else:
        return {}


def get_con_wireless(nm_con):
    """

    :param nm_con:
    :type nm_con: NM.Connection
    :return:
    """
    if nm_con.get_connection_type() == NM.SETTING_WIRELESS_SETTING_NAME:
        wireless = nm_con.get_setting_wireless()
        ''':type: NM.SettingWireless'''
        bssid = wireless.get_bssid()
        mac = wireless.get_mac_address()
        mode = wireless.get_mode()
        ssid = NM.utils_ssid_to_utf8(wireless.get_ssid().get_data())
        name = wireless.get_name()
        return {
            'bssid': bssid,
            'mac': mac,
            'mode': mode,
            'name': name,
            'ssid': ssid,
        }
    else:
        return {}


def get_con_tun(nm_con):
    """

    :param nm_con:
    :type nm_con: NM.Connection
    :return:
    """
    if nm_con.get_connection_type() == NM.SETTING_TUN_SETTING_NAME:
        tun = nm_con.get_setting_tun()
        ''':type: NM.SettingTun'''
        return {
            'mac': tun
        }
    else:
        return {}


def get_ap_props(ap):
    """
    http://lazka.github.io/pgi-docs/#NM-1.0/classes/AccessPoint.html
    :param ap:
    :type ap: NM.AccessPoint
    :return:
    """
    return {
        'bssid': ap.get_bssid(),
        'frequency': ap.get_frequency(),
        'last-seen': ap.get_last_seen(),
        'max-bitrate': ap.get_max_bitrate(),
        'strength': ap.get_strength(),
        'ssid': NM.utils_ssid_to_utf8(ap.get_ssid().get_data()),
    }


def get_iface_from_hw_addr(nm_client, hw_addr):
    """
    Returns the first matching interface with the specified hardware address.

    Hardware addresses should be unique, but this is not guaranteed.

    :param nm_client: NetworkManager Client Object
    :type nm_client: NM.Client
    :param hw_addr: Hardware Address (i.e. D0:67:E5:47:50:3A)
    :type hw_addr: str
    :return: Interface Name (i.e. eth0 | eno1 | wlp3s0)
    :rtype: str | None
    """
    return next((d.get_iface() for d in nm_client.get_devices() if d.get_hw_address() == hw_addr.upper().strip()), None)


def get_device_general(nm_device):
    """

    :param nm_device:
    :type nm_device: NM.Device
    :return:
    """
    return {
        'device': nm_device.get_iface(),
        'type': nm_device.get_type_description(),
        'hw_addr': nm_device.get_hw_address(),
        'mtu': nm_device.get_mtu(),
    }


def get_device_con_info(nm_device):
    """

    :param nm_device:
    :type nm_device: NM.Device
    :return:
    """
    if isinstance(nm_device, NM.Device):
        return {
            'state': nm_device.get_state().value_nick,
            'state_reason': nm_device.get_state_reason().value_nick,
            'connection_active': None if nm_device.get_active_connection() is None
            else nm_device.get_active_connection().get_id(),
            'managed': nm_device.get_managed(),
            'metered': nm_device.get_metered().value_nick,
        }
    return None


def get_device_hw_info(nm_device):
    """

    :param nm_device:
    :type nm_device: NM.Device
    :return:
    """
    return {
        'description': nm_device.get_description(),
        'driver': nm_device.get_driver(),
        'driver_version': nm_device.get_driver_version(),
        'firmware_version': nm_device.get_firmware_version(),
        'product': nm_device.get_product(),
        'vendor': nm_device.get_vendor(),
        'real': nm_device.is_real(),
        'virtual': nm_device.is_software(),
    }


def get_device_info(nm_device):
    """

    :param nm_device:
    :type nm_device: NM.Device
    :return:
    """
    d = get_device_general(nm_device)
    d['hw'] = get_device_hw_info(nm_device)
    d['con'] = get_device_con_info(nm_device)
    d['dhcp4'] = get_device_dhcp(nm_device.get_dhcp4_config())
    d['dhcp6'] = get_device_dhcp(nm_device.get_dhcp6_config())
    d['ipv4'] = get_device_ip(nm_device.get_ip4_config())
    d['ipv6'] = get_device_ip(nm_device.get_ip6_config())
    return d


def get_device_ip(nm_ip_config):
    """

    :param nm_ip_config:
    :type nm_ip_config: NM.IPConfig
    :return:
    """
    if nm_ip_config is None:
        return {
            'addresses': [],
            'domains': [],
            'family': 0,
            'gateway': "",
            'nameservers': [],
            'routes': [],
            'searches': [],
            'wins': [],
        }
    return {
        'addresses': [{
            'address': a.get_address(),
            'prefix': a.get_prefix(),
            'dhcp': False
        }
            for a in nm_ip_config.get_addresses()
        ],
        'domains': nm_ip_config.get_domains(),
        'family': nm_ip_config.get_family(),
        'gateway': nm_ip_config.get_gateway(),
        'nameservers': nm_ip_config.get_nameservers(),
        'routes': [get_route_info(r) for r in nm_ip_config.get_routes()],
        'searches': nm_ip_config.get_searches(),
        'wins': nm_ip_config.get_wins_servers(),
    }


def get_device_dhcp(nm_dhcp_config):
    """

    :param nm_dhcp_config:
    :type nm_dhcp_config: NM.DhcpConfig
    :return:
    :rtype: dict
    """
    if nm_dhcp_config is None:
        return {}
    return nm_dhcp_config.get_options()


def get_devices_by_iface(nm_client):
    """

    :param nm_client:
    :type nm_client: NM.Client
    :return:
    """
    all_devices = nm_client.get_devices()
    """:type: list[NM.Device]"""
    devices = {}
    for d in all_devices:
        devices[d.get_iface()] = get_device_info(d)

        if d.get_device_type() == NM.DeviceType.WIFI:
            wifi_props = {
                'hw_addr_active': d.get_hw_address(),
                'hw_addr_perm': d.get_permanent_hw_address(),
                'bitrate': d.get_bitrate(),
                'ap_active': d.get_active_access_point(),
            }
            if wifi_props['ap_active']:
                wifi_props['ap_active'] = get_ap_props(wifi_props['ap_active'])
            devices[d.get_iface()]['wifi'] = wifi_props
        elif d.get_device_type() == NM.DeviceType.ETHERNET:
            # http://lazka.github.io/pgi-docs/#NM-1.0/classes/DeviceEthernet.html
            eth_props = {
                'carrier': d.get_carrier(),
                'hw_addr_active': d.get_hw_address(),
                'hw_addr_perm': d.get_permanent_hw_address(),
                'speed': d.get_speed(),
            }
            devices[d.get_iface()]['eth'] = eth_props

        elif d.get_device_type() == NM.DeviceType.TUN:
            eth_props = {
                'hw_addr_active': d.get_hw_address(),
            }
            devices[d.get_iface()]['eth'] = eth_props
        elif d.get_device_type() == NM.DeviceType.BRIDGE:
            eth_props = {
                'hw_addr_active': d.get_hw_address(),
            }
            devices[d.get_iface()]['eth'] = eth_props

    return devices


def get_cons_by_iface(nm_client):
    """

    :param nm_client:
    :type nm_client: NM.Client
    :return:
    """
    all_cons = nm_client.get_connections()
    """:type: list[NM.RemoteConnection]"""
    cons = {}
    for c in all_cons:
        c_d = get_con_details(c, nm_client)
        d = c_d[list(c_d)[0]]
        # Connections like VPN don't have an iface
        if 'iface' in d:
            cons.setdefault(d['iface'], []).append(c_d)
        else:
            cons.setdefault(d['type'], []).append(c_d)
    return cons


def get_all_connections(nm_client):
    all_connections = nm_client.get_connections()
    status = {}
    for c in all_connections:
        status.update(get_con_details(c, nm_client))
    return status


def get_con_details(nm_con, nm_client=None):
    """

    :param nm_con:
    :type nm_con: NM.RemoteConnection
    :param nm_client:
    :type nm_client: NM.Client | None
    :return:
    :rtype: dict
    """
    if not isinstance(nm_con, NM.RemoteConnection):
        logging.warning("get_con_details(): Valid Connection Required")
        return None

    status = {}
    c_id = nm_con.get_id()
    c_uuid = nm_con.get_uuid()
    c_type = nm_con.get_connection_type()
    c_iface = nm_con.get_interface_name()
    status[c_id] = {
        'uuid': c_uuid,
        'type': c_type,
        'name': nm_con.get_interface_name(),
    }
    status[c_id]['wired'] = get_con_wired(nm_con)
    status[c_id]['wireless'] = get_con_wireless(nm_con)
    # status[c_id]['tunnel'] = get_con_tun(nm_con)
    if 'mac' in status[c_id]['wired']:
        status[c_id]['hw_addr'] = status[c_id]['wired']['mac']
    elif 'mac' in status[c_id]['wireless']:
        status[c_id]['hw_addr'] = status[c_id]['wireless']['mac']
    if isinstance(nm_client, NM.Client) and 'hw_addr' in status[c_id]:
        status[c_id]['iface'] = get_iface_from_hw_addr(nm_client, status[c_id]['hw_addr'])
    elif c_iface:
        status[c_id]['iface'] = c_iface
    elif nm_con.get_connection_type() == NM.SETTING_VPN_SETTING_NAME:
        status[c_id]['iface'] = NM.SETTING_VPN_SETTING_NAME
    con = get_device_con_info(nm_client.get_device_by_iface(status[c_id]['iface']))
    # add IPv4 setting if it doesn't yet exist
    if con and con['connection_active'] == c_id:
        status[c_id]['active'] = True
    else:
        status[c_id]['active'] = False
    status[c_id]['ipv4'] = get_con_ip_cfg(nm_con.get_setting_ip4_config())
    return status


def get_con_details_by_id(nm_client, id):
    """

    :param nm_client:
    :type nm_client: NM.Client
    :param id:
    :return:
    """

    nm_con = nm_client.get_connection_by_id(id)
    details = get_con_details(nm_con)
    details['iface'] = get_iface_from_hw_addr(nm_client, details['hw_addr'])
    return details


def con_change_ipv4(con, method='auto', addresses=[], gateway=None, dns=[]):
    """

    :param con:
    :type con: NM.RemoteConnection
    :param method:
    :param addresses:
    :param gateway:
    :return:
    """

    if not isinstance(con, NM.RemoteConnection):
        logging.warning("con_change_ipv4(): Valid Connection Required")
        return None

    # add IPv4 setting if it doesn't yet exist
    s_ip4 = con.get_setting_ip4_config()  # type: NM.SettingIP4Config
    if not s_ip4:
        s_ip4 = NM.SettingIP4Config.new()
        con.add_setting(s_ip4)

    # set the method and change properties
    s_ip4.set_property(NM.SETTING_IP_CONFIG_METHOD, method)
    # if method == "auto":
    #     # remove addresses and gateway
    #     s_ip4.clear_addresses()
    #     s_ip4.props.gateway = None

    # Add the static IP address, prefix, and (optional) gateway
    s_ip4.clear_addresses()
    s_ip4.props.gateway = None
    for item in addresses:
        address = item['address']
        prefix = item['prefix']
        if prefix is None:
            prefix = 24
            logging.warning("WARNING: NO PREFIX...USING 24")
        addr = NM.IPAddress.new(socket.AF_INET, address, prefix)
        s_ip4.add_address(addr)
    if gateway:
        s_ip4.props.gateway = gateway

    s_ip4.clear_dns()
    for item in dns:
        s_ip4.add_dns(item)

    try:
        con.commit_changes(True, None)
        logging.info("The connection profile has been updated.")

    except Exception as e:
        sys.stderr.write("Error: %s\n" % e)


def con_change_id(con, new_id):
    """

    :param con:
    :type con: NM.Connection
    :param new_id:
    :type new_id: str
    :return:
    """
    sc = con.get_setting_connection()
    ''':type: NM.SettingConnection'''
    old_id = sc.get_property('id')
    sc.set_property('id', new_id)

    try:
        con.commit_changes(True, None)
        logging.info("Changed ID from {} to {}".format(old_id, new_id))
    except Exception as e:
        sys.stderr.write("Error: {}".format(e))


def get_802_3_connections(nm_c):
    return {con.get_id(): get_iface_from_hw_addr(nm_c, con.get_setting_wired().get_mac_address()) for con in
            nm_c.get_connections() if con.get_connection_type() == NM.SETTING_WIRED_SETTING_NAME}


def get_802_11_connections(nm_c):
    return {con.get_id(): get_iface_from_hw_addr(nm_c, con.get_setting_wireless().get_mac_address()) for con in
            nm_c.get_connections() if con.get_connection_type() == NM.SETTING_WIRELESS_SETTING_NAME}


def get_active_connections(nm_c):
    return {con.get_id(): con.get_connection_type() for con in nm_c.get_active_connections()}


class NetMgr(object):
    def __init__(self):
        self.main_loop = None
        """:type: GLib.MainLoop"""
        self.client = None
        """:type: NM.Client"""
        self.thread = None
        """:type: Thread"""
        self.active_dict = {}
        self.cons_dict = {}
        self.devs_dict = {}
        self.init()
        self.run()

    def init(self):
        _trigger_loop()
        self.main_loop = GLib.MainLoop()
        GLib.threads_init()
        self.client = NM.Client.new(None)

    def run(self):
        self.thread = Thread(target=self.main_loop.run, daemon=True)
        self.thread.start()

    def get_client(self):
        """

        :return:
        :rtype: NM.Client
        """
        return self.client

    def get_cons(self):
        return get_all_connections(self.client)

    def get_devs_managed(self):
        return {k: v for k, v in get_devices_by_iface(self.client).items() if v['con']['managed']}

    def get_cons_by_dev(self):
        return get_cons_by_iface(self.client)

    def load_all(self):
        self.active_dict = [probe_nm_attributes(p) for p in self.client.get_active_connections()]
        self.cons_dict = [probe_nm_attributes(p) for p in self.client.get_connections()]
        self.devs_dict = [probe_nm_attributes(p) for p in self.client.get_devices()]

    def get_probed_devs(self):
        return [probe_nm_attributes(p) for p in self.client.get_devices()]

    def get_probed_cons(self):
        return [probe_nm_attributes(p) for p in self.client.get_connections()]

    def get_probed_active(self):
        return [probe_nm_attributes(p) for p in self.client.get_active_connections()]

    def get_cons_by_type(self):
        cons = self.client.get_connections()  # type: List[NM.RemoteConnection]
        result = {}
        for c in cons:  # type: NM.RemoteConnection
            id = c.get_id()
            uuid = c.get_uuid()
            con_type = c.get_connection_type()
            hw_addr = None
            if con_type == '802-11-wireless':
                hw_addr = c.get_setting_wireless().get_mac_address()
            elif con_type == "802-3-ethernet":
                hw_addr = c.get_setting_wired().get_mac_address()
            ipv4 = get_con_ip_cfg(c.get_setting_ip4_config())
            id_data = {id: {
                'hw_address': hw_addr,
                'ipv4': ipv4,
                'uuid': uuid,
            }}
            result.setdefault(con_type, {}).update(id_data)
        return result

    def get_devs_by_type(self):
        devices = self.client.get_devices()
        result = {}
        for d in devices:
            state = d.get_state()
            """:type: NM.DeviceState"""
            if state in (NM.DeviceState.ACTIVATED, NM.DeviceState.UNAVAILABLE):
                device_type = d.get_device_type()
                """:type: NM.DeviceType"""
                ipv4 = get_device_ip(d.get_ip4_config())
                dhcp = get_device_dhcp(d.get_dhcp4_config())
                if 'ip_address' in dhcp:
                    idx = next(
                        (index for (index, d) in enumerate(ipv4['addresses']) if d['address'] == dhcp['ip_address']),
                        None)
                    if idx is not None:
                        ipv4['addresses'][idx]['dhcp'] = True
                if state == NM.DeviceState.ACTIVATED:
                    con = d.get_active_connection().get_id()
                else:
                    con = None
                iface_data = {d.get_iface(): {
                    'state': state.value_nick,
                    'hw_address': d.get_hw_address(),
                    'ipv4': ipv4,
                    'active_connection_id': con,
                },
                }
                if device_type == NM.DeviceType.WIFI:
                    wifi = get_device_wifi(d)
                    iface_data[d.get_iface()].update(wifi)
                result.setdefault(device_type.value_nick, {}).update(iface_data)
        return result

    def set_con_by_id_ipv4(self, id, method, addresses, gateway, dns):
        nm_con = self.client.get_connection_by_id(id)
        con_change_ipv4(nm_con, method, addresses, gateway, dns)
        # self.client.reload_connections()
        return get_con_ip_cfg(self.client.get_connection_by_id(id).get_setting_ip4_config())

    def set_con_by_id_activate(self, id):
        self.client.activate_connection_async(self.client.get_connection_by_id(id))
        return True

    def quit(self):
        self.main_loop.quit()
        self.main_loop = None
        self.client = None


def probe_nm_attributes(nm_class, max_recursion=4):
    """
    Traverse all "get_" and "is_" attributes to the max_recursion
    building a dict with results.

    :param nm_class: NM Class
    :param max_recursion: Maximum recursion depth
    :type max_recursion: int
    :return: attributes dictionary
    :rtype: dict
    """

    attr_dict = {}
    for f in dir(nm_class):
        if f.startswith('get_') or f.startswith('is_'):
            attr_name = f[4:] if f.startswith('get_') else f[3:]
            attr = getattr(nm_class, f)
            try:
                result = attr()
                if result is not None:
                    attr_dict[attr_name] = result
                    if isinstance(result, bytes):
                        attr_dict[attr_name] = result.decode()
                    if max_recursion > 0:
                        if isinstance(result, GObject.GEnum):
                            attr_dict[attr_name] = result.value_nick
                        elif isinstance(result, GObject.GType):
                            attr_dict[attr_name] = result.name
                        elif hasattr(result, '__dict__'):
                            attr_dict[attr_name] = probe_nm_attributes(result, max_recursion)
                        elif isinstance(result, list):
                            try:
                                for idx, val in enumerate(result):
                                    if isinstance(val, GObject.GEnum):
                                        attr_dict[attr_name][idx] = val.value_nick
                                    elif hasattr(val, '__dict__'):
                                        attr_dict[attr_name][idx] = probe_nm_attributes(val, max_recursion - 1)
                            except Exception as ex:
                                logging.error(type(ex).__name__, end=":[I] ")
                                logging.error(ex)
                    else:
                        attr_dict[attr_name] = result.__class__.__name__
            except (TypeError, RuntimeError):
                pass
            except Exception as ex:
                logging.error(type(ex).__name__, end=":[O] ")
                logging.error(ex)
    return attr_dict


if __name__ == "__main__":
    import signal
    from datetime import datetime
    from os.path import basename
    from setproctitle import setproctitle

    from colorama import Back

    setproctitle(basename(__file__))
    # Catch SIGTERM as SystemExit
    signal.signal(signal.SIGTERM, lambda signum, stack_frame: sys.exit(0))

    log.init_log('DEBUG', force_color=True)
    logger = logging.getLogger(name=basename(__file__))
    logger.info(Back.RED + "STARTING:    {}".format(str(datetime.now())) + Back.RESET)

    nm = NetMgr()

    client = nm.get_client()
    t = nm.get_cons_by_type()

    devs = get_devices_by_iface(client)
    cons = get_all_connections(client)
    d_c = get_cons_by_iface(client)
    a = [probe_nm_attributes(p) for p in client.get_active_connections()]
    b = [probe_nm_attributes(p) for p in client.get_connections()]
    c = [probe_nm_attributes(p) for p in client.get_devices()]

    eth = get_802_3_connections(client)
    wifi = get_802_11_connections(client)

    c_eth = client.get_connection_by_id(next(iter(eth.keys())))
    ''':type: NM.RemoteConnection'''

    method = 'manual'
    address = '192.168.50.240'
    prefix = 24
    gateway = '192.168.50.1'

    # con_change_ipv4(c_eth, method, address, prefix, gateway)
