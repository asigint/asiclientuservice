#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2017 Applied Signals Intelligence, Inc. -- All Rights Reserved

import errno
import logging

from AsiUtilities.socket_tools import addr_str_to_tuple, addr_tuple_to_str, socket_open, socket_close
from AsiUtilities.netifaces_macros import get_primary_addresses_and_gw_set
from AsiUtilities.JsonStdTypesEncoder import JsonStdTypesEncoder


class MulticastTx(object):
    def __init__(self, dest_tuple=None, ttl=1):
        self.logger_sock = logging.getLogger(name=self.__class__.__name__)

        self._dest_tuple = dest_tuple
        """:type: tuple | None"""
        self._ttl = ttl
        self._socket_dict = {}
        """:type: dict[tuple, socket.socket]"""

        self._suppress_no_gateway_warning = False

    def set_destination(self, dest_tuple, ttl=1):
        """
        Set the multicast destination
        :param dest_tuple: (host, port) or "address:port" format
        :type dest_tuple: tuple | str
        :param ttl: time-to-live for multicast (default=1)
        :type ttl: int
        :return: Success or Failure for add operation
        :rtype: bool
        """

        # Validate address is in address family format
        dest_tuple = addr_str_to_tuple(dest_tuple)
        if dest_tuple is not None:
            self._dest_tuple = dest_tuple
            self._ttl = ttl
            self.get_sockets()

    def get_sockets(self):
        primary_addresses, dr = get_primary_addresses_and_gw_set(
            suppress_no_gateway_warning=self._suppress_no_gateway_warning)
        self._suppress_no_gateway_warning = not dr
        for net_address in primary_addresses:
            if net_address not in self._socket_dict:
                sock = socket_open(self._dest_tuple[0], net_address, self._ttl)
                if sock is not None:
                    self._socket_dict[net_address] = sock
                    self.logger_sock.info(
                        "{}->{}".format(addr_tuple_to_str(self._dest_tuple), net_address))
        return self._socket_dict.copy().items()

    def send_str(self, data, nan_to_null=False, nan_to_zero=False):
        if not isinstance(data, str):
            if nan_to_null and nan_to_zero:
                self.logger_sock.warning("NaN to Null & Zero not valid, using Null")
                nan_to_zero = False
            data = JsonStdTypesEncoder.dumps(data, ignore_nan=nan_to_null)
            if nan_to_zero:
                data = data.replace('NaN', '0')
        for dest, sock in self.get_sockets():
            try:
                sock.sendto(data.encode(), self._dest_tuple)
            except TypeError as err:
                self.logger_sock.warning("SOCKET TypeError: {}".format(err))
            except IOError as err:
                if err.errno == errno.ENETUNREACH or err.errno == errno.EINVAL:
                    self.logger_sock.warning("{}: NETWORK UNREACHABLE".format(dest))
                else:
                    self.logger_sock.warning("SOCKET SEND ERROR: {}".format(err))
                self.remove_net(dest)

    def remove_net(self, dest):
        if dest in self._socket_dict:
            self.logger_sock.warning("{}✘✘{}".format(addr_tuple_to_str(self._dest_tuple), dest))
            try:
                socket_close(self._socket_dict.pop(dest))
                return True
            except Exception as err:
                self.logger_sock.warning("Failed graceful close of {} - {}: {}".format(dest, type(err).__name__, err))
                return False

    def remove_all(self):
        for dest in self._socket_dict.copy().keys():
            self.remove_net(dest)

    def close(self):
        for dest, sock in self.get_sockets():
            try:
                sock.close()
            except (IOError, OSError) as err:
                self.logger_sock.warning("Failed to close multicast {}: {}".format(dest, err))
            except AttributeError:
                pass


def main():
    import os
    from AsiUtilities import log
    from time import sleep
    from datetime import datetime
    import sys

    log.init_log('DEBUG')
    logger = logging.getLogger(name=os.path.basename(__file__))

    s = MulticastTx()
    s.set_destination('239.0.0.1:11000')
    try:
        while True:
            ts = str(datetime.now())
            print('\r' + ts, end="")
            sys.stdout.flush()
            s.send_str(ts)
            sleep(.1)
    except (KeyboardInterrupt, SystemExit):
        logger.info("EXITING")
    finally:
        pass


if __name__ == '__main__':
    import signal
    import sys
    from os.path import basename

    from setproctitle import setproctitle

    setproctitle(basename(__file__))
    # Catch SIGTERM as SystemExit
    signal.signal(signal.SIGTERM, lambda signum, stack_frame: sys.exit(0))

    main()
