import gzip
import json


def json_file_loader(filename):
    data = []
    with gzip.GzipFile(filename=filename, mode='r') as stream:
        print("LOADED FILE: {}".format(filename))

        for line in stream:
            try:
                single_dict = json.loads(line.decode())
            except Exception:
                pass
            else:
                data.append(single_dict)
    return data
