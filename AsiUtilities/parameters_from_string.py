# -*- coding: utf-8 -*-
# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved
import re
# http://stackoverflow.com/a/26809170/3571110
r = re.compile(',\s*(?![^\[\]{}]*[\]|}])')
# Consider: http://stackoverflow.com/a/36327468/3571110
# TODO: Currently only parses lists '[' and '{'...should be improved for '('


def parameters_from_string(str_args):
    """

    :param str_args:
    :type str_args: str
    :return:
    """

    # Split on comma, unless comma is in square bracket
    cmd_args = r.split(str_args)
    cmd_args = tuple([correct_data_type(cmd_arg) for cmd_arg in cmd_args])
    if cmd_args[0] == '':
        return (), {}
    args = [arg for arg in cmd_args if not isinstance(arg, dict)]
    kwargs_list = [kwarg for kwarg in cmd_args if isinstance(kwarg, dict)]
    kwargs = {k: v for d in kwargs_list for k, v in d.items()}
    return args, kwargs


def correct_data_type(value):
    """

    :param value:
    :type value: str
    :return:
    """
    from ast import literal_eval
    value = value.strip()

    # Assume kwarg and process
    try:
        k, v = value.split('=')
    except ValueError:  # No '=', not a kwarg
        pass
    else:  # Found '=', it's a kwarg
        v = v.replace('true', 'True')
        v = v.replace('false', 'False')
        try:
            return {k: literal_eval(v)}
        except (ValueError, SyntaxError):  # value was a string
            return {k: v}

    # arg, not kwarg
    value = value.replace('true', 'True')
    value = value.replace('false', 'False')
    try:
        return literal_eval(value)
    except (ValueError, SyntaxError):
        pass  # string, just fall through and return

    # return unprocessed result
    return value
