#!/usr/bin/python3
import socket
import sys

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('host', help="hostname/address", nargs='?', default='')
    parser.add_argument('port', help="part", nargs='?', type=int, default=5002)
    args = parser.parse_args()

    UDP_IP = args.host
    UDP_PORT = args.port

    try:
        sock = socket.socket(socket.AF_INET,  # Internet
                             socket.SOCK_DGRAM)  # UDP
    except Exception as err:
        print("{}: {}".format(type(err).__name__, err))
        print("{}:{}".format(UDP_IP, UDP_PORT))
        sys.exit(-1)

    message_list = [
        'Freq::Signal ID::Type of Tx::SG1400 ID::Lat/long::Date/time::SNR::Color Code::Source ID::SG1400',
        '495586760::DMR::BS_DATA::1007::N3910.734208,W07652.228194::2015-08-07 12:31:43::12.62::1::1001::PROBE',
        '148000000::ENERGY::FM::1021::N0000.000000,E00000.000000::2016-08-04 21:05:06::13.32::0::0::TARGET',
        '148000000::ENERGY::FM::1021::N0000.000000,E00000.000000::2016-08-04 21:05:06::13.28::0::0::TARGET',
        '148000000::ENERGY::AM::1021::N0000.000000,E00000.000000::2016-08-04 21:05:06::20.99::0::0::TARGET',
    ]

    try:
        while True:
            for message in message_list:
                sock.sendto(message.encode('utf-8'), (UDP_IP, UDP_PORT))
    except (KeyboardInterrupt, SystemExit):
        sock.close()
