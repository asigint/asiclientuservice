def float_silent(value):
    """
    Return a float if possible, else return the same value
    :param value:
    :return:
    """
    try:
        return float(value)
    except ValueError:
        return value
