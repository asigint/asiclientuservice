#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2017 Applied Signals Intelligence, Inc. -- All Rights Reserved

import logging
import re

# (?:) Match but don't keep result
# ^|, Match start of string or comma
# (\d+) Match number (at least one digit) <- GROUP 1
# : Match colon separator
# (.*?) Match anything non-greedy <- GROUP 2
# (?=,$|,\d+:) Look Ahead
#   ,$|,\d+: Match comma then end of line, or match comma, number, colon
pattern = re.compile(r'(?:^|,)(\d+):(.*?)(?=,$|,\d+:)')
netstring = '{:d}:{:s},'


def ns_encode(message):
    """
    Encode a netstring...so simple probably not worth using as function

    :param message:
    :return:
    """
    return netstring.format(len(message), message)


def ns_decode(message, suppress_warnings=False):
    """
    Decode a netstring
    https://en.wikipedia.org/wiki/Netstring

    :param message: string to process
    :type message: str
    :param suppress_warnings:
    :type suppress_warnings: bool
    :return: List of Netstrings and the residual string
    :rtype: (list, str)
    """

    netstrings = []
    residual = ''
    for match in pattern.finditer(message):
        ns_len = int(match.group(1))
        data = match.group(2)
        data_len = len(data)
        if ns_len == data_len:
            netstrings.append(data)
        elif ns_len < data_len and data[ns_len] == ',':
            netstrings.append(data[:ns_len])
            if not suppress_warnings:
                logging.warning("Corrupt Netstring: {}".format(data[ns_len + 1:]))
        else:
            if not suppress_warnings:
                logging.warning("Corrupt Netstring: {}:{}".format(ns_len, data))
        ns_end = match.end()
        residual = message[ns_end:]
    if residual == ',':
        residual = ''
    return netstrings, residual


if __name__ == '__main__':
    s = ('blah,187:{"COMPASS": {"pitch": 0.391, "temperature": NaN, "systime": '
         '1503076468.0724216, "yaw_mag": NaN, "device": "VECTORNAV", "roll": -1.008, '
         '"port": "/dev/compass_serial", "yaw_true": 110.128}},380:{"GPS": '
         '{"declination": -10.657126410232577, "utm": "18S 296752.527319998 '
         '4314031.890403098", "satellites": 0, "systime": 1503076468.0724216, '
         '"device": "VECTORNAV", "port": "/dev/compass_serial", "utctime": '
         '"2017-08-18T17:14:46.048463Z", "mgrs": "18STJ9675214031", "mode": 3, '
         '"longitude": -77.34545911, "gpsepoch": 1503076486.048463, "latitude": '
         '38.95169176, "altitude": 117.664}},186:{"COMPASS": {"pitch": 0.391, '
         '"temperature": NaN, "systime": 1503076468.1025498, "yaw_mag": NaN, "device": '
         '"VECTORNAV", "roll": -1.008, "port": "/dev/compass_serial", "yaw_true": '
         '110.13}},382:{"GPS": {"declination": -10.657126402118585, "utm": "18S '
         '296752.52642475825 4314031.889315516", "satellites": 0, "systime": '
         '1503076468.1025498, "device": "VECTORNAV", "port": "/dev/compass_serial", '
         '"utctime": "2017-08-18T17:14:46.073463Z", "mgrs": "18STJ9675214031", "mode": '
         '3, "longitude": -77.34545912, "gpsepoch": 1503076486.073463, "latitude": '
         '38.95169175, "altitude": 117.655}},')

    s += ('x82:{"GPS": {"declination": -10.657126402118585, "utm": "18S 296752.52642475825 4314031.889315516", '
          '"satellites": 0, "systime": 1503076468.1025498, "device": "VECTORNAV", "port": "/dev/compass_serial", '
          '"utctime": "2017-08-18T17:14:46.073463Z", "mgrs": "18STJ9675214031", "mode": 3, '
          '"longitude": -77.34545912, "gpsepoch": 1503076486.073463, "latitude": 38.95169175, '
          '"altitude": 117.655}},186:{"COMPASS": {"pitch": 0.391, "temperature": NaN, '
          '"systime": 1503076468.1025498, "yaw_mag": NaN, "device": "VECTORNAV", "roll": -1.008, '
          '"port": "/dev/compass_serial", "yaw_true": 110.13}},')
    s += '382:{"GPS": {"declination": -1'
    s += 'asdfasdf,10:1234567890,'

    o, r = ns_decode(s)
    import json

    p = [json.loads(x) for x in o]
    print(s)
