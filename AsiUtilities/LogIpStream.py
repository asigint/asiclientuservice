from __future__ import print_function
import socket
import sys
import threading
from AsiBroker.ApiClient import ApiClient


def display_data(filename, frequency, fs=None, size=131072):

    import matplotlib.pyplot as plt
    from numpy import fromstring, float32, hanning, arange, log10
    from numpy.fft import fft, fftshift

    # Turn on interactive plotting
    plt.ion()

    # Read in the data and convert it to a usable format
    try:
        with open(filename, 'rb') as fid:
            x = fid.read()
            x = fromstring(x, float32)
            l = len(x) / 2
            x = x.reshape(l, 2).T
            # x = x[-size:].T
            x = x[0] - 1j * x[1]
    except IOError:
        return False

    if fs is None:
        fs = 125e6 / 12 / 10

    f = arange(-size / 2, size / 2) * fs / size + frequency
    window = hanning(size)
    x_fft = fftshift(fft(x[-size:] * window))
    plt.figure(1)
    plt.ion()
    plt.hold(True)
    plt.plot(f / 1e6, 20 * log10(abs(x_fft)))
    plt.xlabel('Frequency (MHz)', fontsize=14)
    plt.ylabel('Signal level (dB)', fontsize=14)
    plt.title('Frequency Spectrum', fontsize=14)
    plt.grid(True)
    plt.pause(0.01)

    t = arange(l) / fs
    plt.figure(2)
    plt.ion()
    plt.hold(False)
    abs_x = abs(x)
    plt.plot(t, abs_x)
    plt.grid(True)
    plt.xlabel('Time (s)', fontsize=14)
    plt.ylabel('Signal Level', fontsize=14)
    plt.title('Signal Envelop at Frequency: %5.4e' % frequency, fontsize=14)
    plt.axis([t[0], t[-1], abs_x.min(), abs_x.max()])
    plt.pause(0.01)


class LogIpStream(object):

    def __init__(self):

        self.log_thread = None  # type: PerformLogging

    def __del__(self):
        if self.log_thread is not None:
            self.stop_record_stream()

    def start_record_stream(self, filename, ip_address='', ip_port=2007):
        if self.log_thread is None:
            self.log_thread = PerformLogging(filename, ip_address, ip_port)
            self.log_thread.daemon = True
            self.log_thread.config['thread_on'] = True
            self.log_thread.start()
        return True

    def stop_record_stream(self):

        if self.log_thread is not None:
            self.log_thread.config['thread_on'] = False
            self.log_thread.join(2)
            if not self.log_thread.isAlive():
                self.log_thread = None
            else:
                print('Failed to stop IP stream logging thread', file=sys.stderr)

        return False

    def collect_frequency_sweep(self, file_basename, radio_ip_address, frequencies, ip_port=2007,
                                duration=1, display=False):

        from time import sleep

        if not isinstance(frequencies, list):
            frequencies = [frequencies]

        radio = ApiClient(host=radio_ip_address)

        for frequency in frequencies:
            # Set the filename
            filename = file_basename + '_%5.4e.dat' % frequency
            # Set the frequency
            # noinspection PyUnresolvedReferences
            radio.setFrequency(frequency)
            # Start logging data
            self.start_record_stream(filename, ip_port=ip_port)
            # Wait for specified time duration
            sleep(duration)
            # Stop recording data
            self.stop_record_stream()
            # Display if desired
            if display:
                display_data(filename, frequency)


class PerformLogging(threading.Thread):
    def __init__(self, filename, ip_address='', ip_port=2007):
        """
        Initialize the thread that will write data to the file
        :param filename:
        :param ip_address:
        :param ip_port:
        """
        threading.Thread.__init__(self)
        self.config = dict(ip_addr=ip_address, ip_port=ip_port, filename=filename, thread_on=True)

    def run(self):

        sock = None
        try:
            sock = socket.socket(socket.AF_INET,  # Internet
                                 socket.SOCK_DGRAM)  # UDP
            sock.bind((self.config['ip_addr'], self.config['ip_port']))
        except socket.error as msg:
            print("SOCKET ERROR for {}:{}: {}".format(self.config['ip_addr'], self.config['ip_port'], msg[1]))

        if sock:
            with open(self.config['filename'], 'wb') as fid:
                try:
                    while self.config['thread_on']:
                        data, _ = sock.recvfrom(4096)  # buffer size is 1024 bytes
                        fid.write(data)
                except IOError:
                    pass

            sock.close()
