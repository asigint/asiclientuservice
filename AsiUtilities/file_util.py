# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved
import errno
import os


def makedirs(path):
    """
    wrapper for os.makedirs to silently handle existing directories
    see: http://stackoverflow.com/a/600612/190597 (tzot)

    :param path: directory to create
    :type path: str
    """
    try:
        os.makedirs(path, exist_ok=True)  # Python>3.2
    except TypeError:
        try:
            os.makedirs(path)
        except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise


def validate_path_writeable(file_path):
    """
    Returns a valid absolution (retaining symlinks) writeable path or None

    :param file_path: directory to test (None->Current Working Directory)
    :type file_path: str | None
    :return: str | None
    """
    # Use working directory if none specified
    if file_path is None:
        file_path = os.getcwd()
    # Get full path retaining symbolic links
    file_path = os.path.abspath(file_path)
    # Validate directory writeable (NOTE: can be inaccurate due to SUID/SGID)
    if not os.access(file_path, os.W_OK):
        return None
    return file_path
