#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

from __future__ import unicode_literals

import ipaddress
import logging
import netifaces

HOST_ONLY_NETWORK_MASK = '255.255.255.255'


def get_all_addresses(address_family=netifaces.AF_INET):
    return {
        addr['addr']
        for iface in netifaces.interfaces()
        for addr in netifaces.ifaddresses(iface).get(address_family, [])
        if addr.get('netmask') != HOST_ONLY_NETWORK_MASK
        }


def get_all_addresses_external(address_family=netifaces.AF_INET):
    return get_all_addresses(address_family) - {ip_settings.get('addr') for ip_settings in
                                                netifaces.ifaddresses('lo').get(netifaces.AF_INET, [])}


def get_all_broadcast(address_family=netifaces.AF_INET):
    return {
        addr['broadcast']
        for iface in netifaces.interfaces()
        for addr in netifaces.ifaddresses(iface).get(address_family, [])
        if addr.get('netmask') != HOST_ONLY_NETWORK_MASK and 'broadcast' in addr
        }


def get_all_address_and_masks(address_family=netifaces.AF_INET):
    return {
        addr['addr']: addr['netmask']
        for iface in netifaces.interfaces()
        for addr in netifaces.ifaddresses(iface).get(address_family, [])
        if addr.get('netmask') != HOST_ONLY_NETWORK_MASK
        }


def get_primary_addresses(address_family=netifaces.AF_INET):
    primary_addresses, _ = get_primary_addresses_and_gw_set(address_family)
    return primary_addresses


def get_primary_addresses_and_gw_set(address_family=netifaces.AF_INET, suppress_no_gateway_warning=False):
    """
    Return all address (IPv4 by default) available.
    Exclude Host-Only addresses
    Exclude address in the default network but not the default route (aka gateway)

    :param address_family: [default=AF_INET]
    :param suppress_no_gateway_warning: Don't warn if no default gateway [default=False]
    :type suppress_no_gateway_warning: bool
    :return: set of ip address, default_gateway_set
    :rtype: set, bool
    """
    _, default_net, default_iface, _, ag = get_default_gateway(address_family,
                                                               suppress_no_gateway_warning=suppress_no_gateway_warning)
    try:
        return {
            ip_settings['addr']
            for iface in netifaces.interfaces()
            for ip_settings in netifaces.ifaddresses(iface).get(address_family, [])
            if ip_settings.get('netmask') != HOST_ONLY_NETWORK_MASK and 'netmask' in ip_settings and not
            (ipaddress.IPv4Network(ip_settings['addr'] + '/' + ip_settings['netmask'], strict=False) == default_net) &
            (iface != default_iface)
            }, ag
    except Exception as err:
        logging.warning("get_primary_addresses() -> {}: {}".format(type(err).__name__, err))
        return set(), False


def get_default_gateway(address_family=netifaces.AF_INET, suppress_no_gateway_warning=False):
    """
    Get the relevant properties of the default network.
    ex. ('192.168.50.1', IPv4Network('192.168.50.0/24'), 'eno1', '192.168.50.172', True)

    :param address_family: [default=AF_INET]
    :param suppress_no_gateway_warning: Don't warn if no default gateway [default=False]
    :type suppress_no_gateway_warning: bool
    :return: default_route, network, default_iface, ip_addr, available_gateway,
    :rtype: str, ipaddress.IPv4Network, str, str, bool
    """
    try:
        default_route, default_iface = netifaces.gateways()['default'][address_family]
    except KeyError:
        if not suppress_no_gateway_warning:
            logging.warning("get_default_gateway() -> No Default Gateway")
        return None, None, None, None, False
    else:
        try:
            for ip_settings in netifaces.ifaddresses(default_iface).get(address_family, []):
                network = ipaddress.IPv4Network(ip_settings['addr'] + '/' + ip_settings['netmask'], strict=False)
                if ipaddress.IPv4Address(u'' + default_route) in network:
                    return default_route, network, default_iface, ip_settings['addr'], True
        except Exception as err:
            logging.warning("get_default_gateway() -> {}: {}".format(type(err).__name__, err))
            return None, None, None, None, False


def get_iface_up(iface):
    import pyroute2
    ip = pyroute2.IPRoute()
    state = ip.get_links(ip.link_lookup(ifname=iface))[0].get_attr('IFLA_OPERSTATE')
    ip.close()
    return state == 'UP'


def main():
    from AsiUtilities import log

    log.init_log('DEBUG', force_color=True)

    print(get_default_gateway())
    print(get_all_broadcast())
    print(get_all_addresses())
    print(get_primary_addresses())


if __name__ == '__main__':
    main()
