# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

import datetime
import errno
import hashlib
import json
import logging
import os
import re
import sys
import traceback
from enum import Enum

import msgpack
import msgpack_numpy
import numpy
import numpy as np

# from data_validator import data_validator

_logger_DFIO = logging.getLogger(name='data_file_io')


class DataType(Enum):
    Raw = 0
    NarrowBand = 15000
    DDC2ndStage = 80000
    DDC1stStage = 800000
    Wideband = 8000000
    FortyMeg = 40000000


DataTypeAbbr = {
    DataType.Raw: 'raw',
    DataType.NarrowBand: 'nb',
    DataType.DDC2ndStage: 'ddc2',
    DataType.DDC1stStage: 'ddc1',
    DataType.Wideband: 'wb',
    DataType.FortyMeg: 'm40',
}


# noinspection PyPep8Naming
def write_mp(data, nav, radio_state, data_type=DataType.NarrowBand, path=None, filename=None, auto_basename=None,
             FILENAME_GPS_TIME=False, SUPPRESS_WRITE_NOTIFICATION=False, SIMULATE_WRITE=False,
             METADATA_VALIDATE=False, ):
    """

    :param data:
    :type data: np.multiarray.ndarray
    :param nav:
    :type nav: dict
    :param radio_state:
    :type radio_state: dict
    :param data_type:
    :type data_type: Enum
    :param path:
    :type path: str | None
    :param filename:
    :type filename: str | None
    :param auto_basename:
    :type auto_basename: str | None
    :param FILENAME_GPS_TIME:
    :type FILENAME_GPS_TIME: bool
    :param SUPPRESS_WRITE_NOTIFICATION:
    :type SUPPRESS_WRITE_NOTIFICATION: bool
    :param SIMULATE_WRITE:
    :type SIMULATE_WRITE: bool
    :param METADATA_VALIDATE:
    :type METADATA_VALIDATE: bool
    :return:
    :rtype: str | None
    """

    if data is None or nav is None or radio_state is None:
        _logger_DFIO.warning("File Write inputs == None: data({}), nav({}), radio_state({})"
                             .format(data is None, nav is None, radio_state is None))
        return None

    # Establish Version
    version = 6

    # Ensure reference to data isn't being changed
    data.flags.writeable = False

    # Validate filename
    if filename is not None:
        filename_path, filename_only = os.path.split(filename)
        if filename_path != '':
            try:
                mkdir_p(filename_path)
                if path is not None:
                    _logger_DFIO.warning("Path passed in with filename overrides separate path.")
                    path = None
            except (AttributeError, OSError) as msg:
                _logger_DFIO.warning("File Write Path {}:{} for FILENAME={}, PATH={}"
                                     .format(type(msg).__name__, msg, filename, path))
                _logger_DFIO.debug("{}".format(traceback.format_exc()))
                filename = filename_only

    # Validate Path
    if path is not None:
        try:
            mkdir_p(path)
            if filename is not None:
                filename = os.path.join(path, filename)
        except (AttributeError, OSError) as msg:
            _logger_DFIO.warning("File Write Path {}:{} for FILENAME={}, PATH={}"
                                 .format(type(msg).__name__, msg, filename, path))
            _logger_DFIO.debug("{}".format(traceback.format_exc()))
            path = None

    # Generate Filename
    if filename is None:
        timestamp = None
        if FILENAME_GPS_TIME:
            timestamp = nav['GPS']['utctime']
        if timestamp is None:
            timestamp = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f%z')
        # Make the name DOS/Windows compatible
        filename = timestamp.replace(':', '-').replace('.', '-').replace('T', '_').replace(' ', '_') + '.dat'
        # Prepend Data Type
        filename = DataTypeAbbr[data_type] + '-' + filename
        if auto_basename is not None:
            filename = auto_basename + '-' + filename
        if path is not None:
            filename = os.path.join(path, filename)
        # Validate Filename (increment the 3-digit ms by 1 if necessary)
        while os.path.exists(filename):
            filename = re.sub(r'(\d+)(?!.*\d)',
                              lambda x: '{:0{width}}'.format(int(x.group(0)) + 1, width=len(x.group(0))),
                              filename)

    # Validate Dictionaries
    # if METADATA_VALIDATE:
    if False:
        data_radio_state = None
        data_nav = None
        # try:
        #     data_radio_state = data_validator('data_radio_state', radio_state, version=version)
        #     data_nav = data_validator('data_nav', nav, version=version)
        # except Exception as err:
        #     _logger_DFIO.warning("Dictionary Validation Error -> {}:{}".format(type(err).__name__, err))
        #     _logger_DFIO.debug("{}".format(traceback.format_exc()))
    else:
        data_radio_state = radio_state
        data_nav = nav
        # Need to remove numpy from dict
        for k, v in data_radio_state.items():
            if isinstance(v, (numpy.generic, numpy.ndarray)):
                data_radio_state[k] = v.tolist()

    # Create Header
    header = {
        'version': version,
        'data_radio_state_md5': hashlib.md5(json.dumps(data_radio_state, sort_keys=True).encode('utf-8')).hexdigest(),
        'data_nav_md5': hashlib.md5(json.dumps(data_nav, sort_keys=True).encode('utf-8')).hexdigest(),
        'data_type': data_type.value,
        'data_md5': hashlib.md5(data).hexdigest(),
    }

    if not SIMULATE_WRITE:
        # Write File
        try:
            with open(filename, 'wb') as stream:
                msgpack.pack(header, stream, use_bin_type=True)
                msgpack.pack(data_radio_state, stream, use_bin_type=True)
                msgpack.pack(data_nav, stream, use_bin_type=True)
                msgpack.pack(data, stream, default=msgpack_numpy.encode, use_bin_type=True)
            if not SUPPRESS_WRITE_NOTIFICATION:
                _logger_DFIO.info("Wrote Data to {}".format(filename))
            return filename
        except IOError as io_err:
            _logger_DFIO.warning("Data Write Error -> {}:{}".format(type(io_err).__name__, io_err))
            _logger_DFIO.debug("{}".format(traceback.format_exc()))
            return None
        except Exception as err:
            _logger_DFIO.warning("Data Packing Error -> {}:{}".format(type(err).__name__, err))
            _logger_DFIO.debug("{}".format(traceback.format_exc()))
            return None
    else:
        if not SUPPRESS_WRITE_NOTIFICATION:
            _logger_DFIO.info("Wrote Data to {} [SIMULATED]".format(filename))
        return filename


def read_mp(filename):
    """

    :param filename:
    :type filename: str
    :return: data, data_nav, radio_state_nav
    :rtype: np.multiarray.ndarray | None, dict | None, dict | None, DataType | None
    """

    # TODO: Hackish solution to string/bytes Python2/3 issues
    # the below works if the file was created with use_bin_type=True
    # unpacker = msgpack.Unpacker(stream, object_hook=msgpack_numpy.decode, encoding='utf-8')
    try:
        with open(filename, 'rb') as stream:
            unpacker = msgpack.Unpacker(stream, encoding='utf-8', object_hook=msgpack_numpy.decode)
            unpacked_list = []
            try:
                for unpacked in unpacker:
                    unpacked_list.append(unpacked)
            except UnicodeDecodeError:
                stream.seek(0)
                count = 0
                unpacker = msgpack.Unpacker(stream, object_hook=msgpack_numpy.decode)
                for unpacked in unpacker:
                    count += 1
                    if count > len(unpacked_list):
                        unpacked_list.append(unpacked)

            # Yet another string/unicode hack!!
            if isinstance(unpacked_list[3], dict):
                stream.seek(0)
                count = 0
                unpacker = msgpack.Unpacker(stream, object_hook=msgpack_numpy.decode)
                for unpacked in unpacker:
                    count += 1
                    if count == 4:
                        unpacked_list[3] = unpacked

        if len(unpacked_list) == 0:
            print(filename)
            raise ValueError('Bah')

        header = unpacked_list[0]
        """:type: dict"""
        data_radio_state = unpacked_list[1]
        """:type: dict"""
        data_nav = unpacked_list[2]
        """:type: dict"""
        data = unpacked_list[3]

        """:type: np.multiarray.ndarray"""
    except (TypeError, ValueError, IOError) as e:
        _logger_DFIO.warning("Read filename={} -> {}: {}".format(filename, type(e).__name__, e))
        for err_data in traceback.extract_tb(sys.exc_info()[2], limit=3):
            filename, line_no, function_name, line_text = err_data
            _logger_DFIO.debug("{}:{} in {}".format(os.path.basename(filename), line_no, function_name))
        return None, None, None, None
    except Exception as e:
        _logger_DFIO.warning("Read filename={} -> {}: {}".format(filename, type(e).__name__, e))
        for err_data in traceback.extract_tb(sys.exc_info()[2], limit=3):
            filename, line_no, function_name, line_text = err_data
            _logger_DFIO.debug("{}:{} in {}".format(os.path.basename(filename), line_no, function_name))
        return None, None, None, None

    # Validate Read
    try:
        header['data_type'] = DataType(header['data_type'])
        data_check = header['data_md5'] == hashlib.md5(data).hexdigest()
        data_radio_state_check = header['data_radio_state_md5'] == hashlib.md5(
            json.dumps(data_radio_state, sort_keys=True).encode()).hexdigest()
        data_nav_check = header['data_nav_md5'] == hashlib.md5(
            json.dumps(data_nav, sort_keys=True).encode()).hexdigest()
        # data_radio_state_valid = data_validator('data_radio_state', data_radio_state, header['version']) is not None
        data_radio_state_valid = True
        # data_nav_valid = data_validator('data_nav', data_nav, header['version']) is not None
        data_nav_valid = True

        if 'df_frequency' not in data_radio_state:
            data_radio_state['df_frequency'] = data_radio_state['frequency'][0]

        if 'sample_rate' not in data_radio_state:
            if header['data_type'] is DataType.NarrowBand:
                data_radio_state['sample_rate'] = 125e6 / 12 / 100 / 7
            elif header['data_type'] is DataType.DDC2ndStage:
                data_radio_state['sample_rate'] = 125e6 / 12 / 100
            elif header['data_type'] is DataType.DDC1stStage:
                data_radio_state['sample_rate'] = 125e6 / 12 / 10
            elif header['data_type'] is DataType.Wideband:
                data_radio_state['sample_rate'] = 125e6 / 12

        if data_check and data_radio_state_check and data_nav_check and data_radio_state_valid and data_nav_valid:
            _logger_DFIO.info("Read:{}".format(filename))
            return data, data_nav, data_radio_state, header['data_type']
        else:
            if not data_check:
                _logger_DFIO.warning('data MD5 mismatch for file: {}'.format(filename))
            if not data_radio_state_check or not data_nav_check:
                _logger_DFIO.warning('data properties MD5 mismatch for file {}'.format(filename))
            if not data_radio_state_valid or not data_nav_valid:
                _logger_DFIO.warning('data properties failed validation for file {}'.format(filename))
            return None, None, None, None
    except Exception as err:
        _logger_DFIO.warning("read_mp() -> {}:{}".format(type(err).__name__, err))
        for err_data in traceback.extract_tb(sys.exc_info()[2], limit=3):
            filename, line_no, function_name, line_text = err_data
            _logger_DFIO.debug("{}:{} in {}".format(os.path.basename(filename), line_no, function_name))
        _logger_DFIO.warning("read_mp() Failed to Read File: {}".format(filename))
        return None, None, None, None


# http://stackoverflow.com/a/600612/190597 (tzot)
def mkdir_p(path):
    """

    :param path:
    :type path: str
    :return:
    :rtype: bool
    """
    try:
        os.makedirs(path, exist_ok=True)  # Python>3.2
    except TypeError:
        try:
            os.makedirs(path)
        except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            elif path == '':
                raise AttributeError
            else:
                raise
    return True


# if __name__ == '__main__':
#     # Setup Paths
#     # fn = '/home/jerry/DAHLGREN/datafile-nb-2016-11-18_15-24-43-312024.dat'
#     # fn = '/opt/asi/devel/dfSystem/datafile-nb-2017-03-24_16-02-01-199219.dat'
#     fn = '/home/jerry/Downloads/ScanEagle Flight 1-nb-2017-01-26_12-24-59-840611.dat'
#     read_mp(fn)
#
#     _logger_DFIO.setLevel(logging.DEBUG)
#     working_path = os.getcwd()
#     home_path = os.path.expanduser('~')
#     file_path = os.path.dirname(os.path.abspath(__file__))  # Full path
#     real_file_path = os.path.dirname(os.path.realpath(__file__))  # Resolve Sym Link
#
#     write_path = os.path.join(home_path, 'datafiles')
#     read_path = os.path.join(file_path, 'datafiles')
#
#     read_files = [f for f in os.listdir(read_path) if f.endswith('.dat')]
#
#     from DFfileIO import DFfileIO
#
#     df_file_io = DFfileIO()
#     # datan, navn, rsn, t = read_mp('datafiles/datafile-nb-2015-12-03_22-52-20-706979.dat')
#     for f in read_files:
#         read_filename = os.path.join(read_path, f)
#         data_old, data_nav_old, data_radioState_old = df_file_io.readFile(read_filename)
#         out_filename = write_mp(data_old, data_nav_old, data_radioState_old, filename=f, path=write_path,
#                                 auto_basename='Test_Basename')
#         data_new, data_nav_new, data_radioState_new, data_type_new = read_mp(out_filename)
