#!/usr/bin/env python3


import LatLon23
import numpy as np
from SignalProcessing.communications import phase_unwrap_degrees
from scipy.interpolate import interp1d

# noinspection PyUnresolvedReferences
def compute_correction_vector(freqs, aoas, true_aoas, technique='linear'):
    frequencies = np.unique(freqs)

    cv = []
    for m in range(len(frequencies)):
        index = np.where(freqs == frequencies[m])[0]
        aoa1 = np.array(aoas)[index]
        true_aoa1 = np.array(true_aoas)[index]
        i1 = aoa1.argsort()
        true_aoa1 = true_aoa1[i1]
        aoa1 = aoa1[i1]
        aoa1, i2 = np.unique(aoa1, True)
        true_aoa1 = true_aoa1[i2]
        aoa1 = phase_unwrap_degrees(aoa1)
        true_aoa1 = phase_unwrap_degrees(true_aoa1)
        x1 = np.zeros(len(aoa1) + 2)
        err = np.zeros(len(aoa1) + 2)
        x1[1:-1] = aoa1
        x1[0] = x1[-2] - 360
        x1[-1] = x1[1] + 360
        err[1:-1] = true_aoa1 - aoa1
        # err[err < -180] += 360
        # err[err > 180] -= 360
        err[0] = err[-2]
        err[-1] = err[1]
        while x1[0] < -360:
            x1 += 360
        while x1[-1] > 720:
            x1 -= 360
        lookup = interp1d(x1, err, technique)
        # correction_vector = lookup(range(360))
        cv.append(lookup)

    return frequencies, cv


def write_simple_table(filename, keyword, data):
    with open(filename, 'w') as stream:
        stream.write('%s:\n' % keyword)

        for dat in data:
            stream.write('- %7.3f\n' % dat)


# noinspection PyUnresolvedReferences
def compute_tone_peaks(data, freq_offsets=0, f1=None):
    if f1 is None:
        f1 = 125e6 / 12 / 100 / 7

    _, c1 = data.shape
    n = int(2 ** (np.ceil(np.log2(c1))))
    mask = np.ones(n, dtype=np.bool)
    mask[n // 2 - 120: n // 2 + 120] = False
    mask[: n // 2 - 180] = False
    mask[n // 2 + 180:] = False
    win = np.hanning(c1) * np.exp(-1j * 2 * np.pi * freq_offsets * np.arange(c1) / f1)
    x = np.fft.fftshift(np.fft.fft(data * win, n, 1), 1)
    x = x.transpose()
    y = x[mask].copy()
    x = x[~mask]
    x1 = abs(x).max(1)
    index = x1.argmax()
    peaks = x[index]
    snr = 20 * np.log10(abs(peaks) / np.sqrt((abs(y) ** 2).max()))
    return peaks, snr


# noinspection PyUnresolvedReferences
class CalibratePlatform(object):

    def __init__(self, platform='multi-copter', antenna='2.5inCyl001',
                 target_latitude=38.893163, target_longitude=-77.072105):

        self.target_location = None
        self.set_target_location(target_latitude, target_longitude)
        self.range_limit = None
        self.bank_limit = None
        self.platform = platform
        self.antenna = antenna

    def set_target_location(self, latitude, longitude):

        self.target_location = LatLon23.LatLon(latitude, longitude)
        return self.target_location

    def get_target_location(self):
        return self.target_location

    def set_range_limit(self, range_limit):

        self.range_limit = range_limit
        return self.range_limit

    def get_range_limit(self):
        return self.range_limit

    def set_bank_limit(self, max_roll):
        self.bank_limit = max_roll
        return self.bank_limit

    def get_bank_limit(self):
        return self.bank_limit

    def compute_true_aoa(self, latitudes, longitudes, yaws):

        if not isinstance(latitudes, (list, np.ndarray)):
            latitudes = np.array([latitudes])
            longitudes = np.array([longitudes])
            yaws = np.array([yaws])

        n = len(latitudes)

        true_aoa = np.zeros(n)
        ranges = np.zeros(n)
        for m in range(n):
            position = LatLon23.LatLon(latitudes[m], longitudes[m])
            heading = self.target_location - position
            true_aoa[m] = (heading.heading - yaws[m]) % 360
            ranges[m] = heading.magnitude

        return true_aoa, ranges

    def set_platform(self, new_platform):
        self.platform = new_platform
        return self.platform

    def get_platform(self):
        return self.platform

    def set_antenna(self, new_antenna):
        self.antenna = new_antenna
        return self.antenna

    def get_antenna(self):
        return self.antenna
