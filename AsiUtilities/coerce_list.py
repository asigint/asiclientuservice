def coerce_list(variable):
    """
    Coerce a python primitive type into a list
    :param variable:
    :return:
    :rtype: list
    """
    if isinstance(variable, (str, float, int, bool)):
        variable = [variable]
    if isinstance(variable, list):
        return variable
    return []

