# -*- coding: utf-8 -*-
# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

from __future__ import division


def clamp(n, smallest, largest): return max(smallest, min(n, largest))


def scale(n, min_old, max_old, min_new, max_new):
    return (n - min_old) * (max_new - min_new) / (max_old - min_old) + min_new
