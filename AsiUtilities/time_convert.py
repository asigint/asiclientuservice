# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

import datetime
import logging
import time

import pytz
import tzlocal
from dateutil.parser import parse as date_parse
from six import string_types


def datetime_as_utc(datetime_obj, suppress_error=False):
    if isinstance(datetime_obj, datetime.datetime):
        # http://stackoverflow.com/a/27596917/3571110
        try:
            return datetime_obj.astimezone(tz=pytz.utc)
        except ValueError:
            return datetime_obj.replace(tzinfo=pytz.utc)
    elif not suppress_error:
        logging.warning("datetime_as_utc: input must be datetime, not TYPE:{}".format(type(datetime_obj)))
    return None


def datetime_as_local(datetime_obj, suppress_error=False):
    datetime_obj_utc = datetime_as_utc(datetime_obj)
    if isinstance(datetime_obj_utc, datetime.datetime):
        # http://stackoverflow.com/a/13287083/3571110
        local_tz = tzlocal.get_localzone()
        local_dt = datetime_obj_utc.astimezone(local_tz)
        return local_tz.normalize(local_dt)
    elif not suppress_error:
        logging.warning("datetime_as_local: input must be datetime, not TYPE:{}".format(type(datetime_obj_utc)))
    return None


def datetime_utc_from_epoch(epoch, suppress_error=False):
    """
    Returns a timezone aware datetime object

    :param epoch:
    :param suppress_error:
    :return:
    """
    if isinstance(epoch, (int, float)):
        return datetime.datetime.fromtimestamp(epoch, pytz.utc)
    elif not suppress_error:
        logging.warning("datetime_from_epoch: input must be a number, not TYPE:{}".format(type(epoch)))
    return None


def datetime_local_from_epoch(epoch, suppress_error=False):
    return datetime_as_local(datetime_utc_from_epoch(epoch, suppress_error=suppress_error),
                             suppress_error=suppress_error)


def datetime_utc_from_gpsd(time_str, time_format='%Y-%m-%dT%H:%M:%S.%fZ', suppress_error=False):
    if isinstance(time_str, string_types):
        return datetime.datetime.strptime(time_str, time_format).replace(tzinfo=pytz.utc)
    elif not suppress_error:
        logging.warning("datetime_utc_from_gpsd: input must be string, not TYPE:{}".format(type(time_str)))
    return None


def datetime_utc_from_gps_time(gps_week, gps_time_of_week):
    """
    Returns a datetime object in UTC
    :param gps_week:
    :type gps_week: int
    :param gps_time_of_week:
    :type gps_time_of_week: float
    :return: datetime
    """
    days_in_week = 7
    gps_start_date = datetime.datetime(1980, 1, 6, 0, 0, 0, tzinfo=pytz.utc)
    return gps_start_date + datetime.timedelta(days=(gps_week * days_in_week), seconds=gps_time_of_week)


def str_utc_from_datetime(datetime_obj, time_format='%Y-%m-%dT%H:%M:%S.%fZ', suppress_error=False):
    datetime_obj_utc = datetime_as_utc(datetime_obj)
    if isinstance(datetime_obj_utc, datetime.datetime):
        return datetime_obj_utc.strftime(time_format)
    elif not suppress_error:
        logging.warning("str_utc_from_datetime: input must be datetime, not TYPE:{}".format(type(datetime_obj_utc)))
    return None


def str_local_from_datetime(datetime_obj, time_format='%Y-%m-%dT%H:%M:%S.%f%z', suppress_error=False):
    datetime_obj_local = datetime_as_local(datetime_obj)
    if isinstance(datetime_obj_local, datetime.datetime):
        return datetime_obj_local.strftime(time_format)
    elif not suppress_error:
        logging.warning("str_local_from_datetime: input must be datetime, not TYPE:{}".format(type(datetime_obj_local)))
    return None


def str_utc_from_now(time_format='%Y-%m-%dT%H:%M:%S.%f%z'):
    return datetime.datetime.now().strftime(time_format)


def str_to_datetime(ts=None):
    """
    Convert a time string to a timezone aware datetime object.
    Assume localtime if no timezone provided.
    Only outputs a valid datetime between the unix epoch and now
    (ie. future time strings are not supported).

    :param ts: Timestamp such as 2015-12-21T14:08:10.677351-05:00
    :type ts: str | None
    :return: datetime object
    :rtype datetime.datetime | None
    """
    if isinstance(ts, str):
        try:
            dt = date_parse(ts)
        except ValueError:
            pass
        else:
            if dt.utcoffset() is None:
                local_tz = tzlocal.get_localzone()
                dt = local_tz.localize(dt)
            if datetime.datetime.fromtimestamp(0, pytz.utc) < dt < datetime.datetime.now().replace(
                    tzinfo=datetime.timezone.utc):
                return dt
    return None


def str_to_epoch(ts=None):
    """
    Convert a time string to a timezone aware datetime object.
    Assume localtime if no timezone provided.
    Only outputs a valid datetime between the unix epoch and now.
    (ie. future time strings are not supported).

    :param ts: Timestamp such as 2015-12-21T14:08:10.677351-05:00
    :type ts: str | None
    :return: unix epoch time (float)
    :rtype float | None
    """
    dt = str_to_datetime(ts)
    if isinstance(dt, datetime.datetime):
        return epoch_from_datetime(dt, suppress_error=True)
    return None


def epoch_from_datetime(datetime_obj, suppress_error=False):
    datetime_obj_utc = datetime_as_utc(datetime_obj)
    if datetime_obj_utc is not None:
        return (datetime_obj_utc - datetime.datetime.utcfromtimestamp(0).replace(tzinfo=pytz.utc)).total_seconds()
    elif not suppress_error:
        logging.warning("epoch_from_datetime_utc: input must be datetime, not TYPE:{}".format(type(datetime_obj)))
    return None


def epoch_from_string(time_str, time_format='%Y-%m-%dT%H:%M:%S.%fZ', suppress_error=False):
    if isinstance(time_str, string_types) and time_str != 'n/a':
        return epoch_from_datetime(datetime_utc_from_gpsd(time_str, time_format))
    elif not suppress_error:
        logging.warning("epoch_from_string: input must valid time string, not TYPE:{}".format(type(time_str)))
    return float('NaN')


def epoch_to_string(epoch, time_format='%Y-%m-%dT%H:%M:%S.%fZ', suppress_error=False):
    if isinstance(epoch, (float, int)):
        return time.strftime(time_format, time.localtime(epoch))
    elif not suppress_error:
        logging.warning("epoch_to_string(): input must valid epoch, not TYPE:{}".format(type(epoch)))
    return None

if __name__ == "__main__":
    print("TESTING GPS TIME")
    stamp = datetime_utc_from_gps_time(1872, 93099.114627)
    print(str_utc_from_datetime(stamp))
    print(str_local_from_datetime(stamp))
