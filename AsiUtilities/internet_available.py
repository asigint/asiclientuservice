# See http://stackoverflow.com/a/33117579/3571110
import socket


def internet_available(host="8.8.8.8", port=53, timeout=3):
    """
    :param host: default=8.8.8.8 -> google-public-dns-a.google.com
    :param port: default=53 -> dns/tcp
    :param timeout: default=3 seconds
    """
    try:
        socket.setdefaulttimeout(timeout)
        socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, port))
        return True
    except Exception as ex:
        print(ex.message)
        return False
