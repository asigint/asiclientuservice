from six import string_types


def validate_boolean(value):
    if isinstance(value, string_types):
        if value.lower() == 'true':
            value = True
        elif value.lower() == 'false':
            value = False
    if isinstance(value, bool):
        return value
    else:
        raise TypeError('TypeError: {} not boolean, Type: {}'.format(value, type(value)))
