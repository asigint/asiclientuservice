def extract_dict_data(dict_list, keys=None):
    if not isinstance(dict_list, list):
        print("INPUT ERROR")
        return
    first_dict=dict_list[0]
    if not isinstance(first_dict, dict):
        print("INPUT ERROR")
        return
    input_keys = first_dict.keys()
    if keys is None:
        keys = input_keys
    elif isinstance(keys, str):
        keys = [keys]
    if not isinstance(keys, list):
        print("INPUT ERROR")
        return
    else:
        keys = [k for k in input_keys if k in keys]

    out_dict = {}
    for k in keys:
        out_dict[k] = []

    for list_item in dict_list:
        for k in keys:
            out_dict[k].append(list_item[k])

    return out_dict