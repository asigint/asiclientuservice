# noinspection PyPep8Naming
class DataBuffer:
    """ A simple buffer that stores exactly one value """
    # http://stackoverflow.com/questions/26856378/python-thread-safe-access-without-blocking-or-uncontrolled-queue-growth
    # TODO: Decide if this is overly generic and simplify to just handling a single variable
    current = None
    latest = None

    @staticmethod
    def onNewReading(*args, **kwargs):  # or any parameters one fancies
        # Pack the results in a single object and store it
        DataBuffer.current = (args, kwargs)
        # Remove empty elements
        DataBuffer.current = [_f for _f in DataBuffer.current if _f]
        # Remove unnecessary tuples
        while isinstance(DataBuffer.current, (tuple, list)) and len(DataBuffer.current) == 1:
            DataBuffer.current = DataBuffer.current[0]
        DataBuffer.latest = DataBuffer.current

    @staticmethod
    def onUserRequest():
        return DataBuffer.latest
