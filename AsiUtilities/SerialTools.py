#!/usr/bin/env python
# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

import errno
import fcntl
import logging
import termios
import traceback

import serial
from future.builtins import dict
from serial.tools import list_ports

_logger_ST = logging.getLogger(name='SerialTools')


def get_port_list():
    try:
        ports = serial.tools.list_ports.comports()
        if ports:
            return list(d for d, p, i in ports)
        else:
            return []
    except NameError:
        return []


def get_available_ports(ports_to_scan=None, ports_to_skip=None, suppress_info_logging=True):
    """
    Get Available Ports: If ports_to_scan not provided, a complete ports_to_scan will be used.
    :param ports_to_scan:
    :type ports_to_scan: list | str | None
    :param ports_to_skip:
    :type ports_to_skip: list | str | None
    :param suppress_info_logging:
    :type suppress_info_logging: bool
    :return:
    :rtype: list
    """
    ports_available = []
    if not ports_to_scan:
        ports_to_scan = get_port_list()
    if isinstance(ports_to_scan, str):
        ports_to_scan = [ports_to_scan]
    if not ports_to_skip:
        ports_to_skip = []
    if isinstance(ports_to_skip, str):
        ports_to_skip = [ports_to_skip]
    if isinstance(ports_to_scan, list) and isinstance(ports_to_skip, list):
        for tty in list(set(ports_to_scan) - set(ports_to_skip)):
            port = open_serial_port(tty, suppress_info_logging=suppress_info_logging)
            if port is not None:
                ports_available.append(tty)
                close_serial_port(port, suppress_info_logging=suppress_info_logging)
        return ports_available
    else:
        return []


def open_serial_port(port=None, **kwargs):
    """
    Open a serial port with an exclusive lock

    :param port: Device Name
    :type port: str | None
    :param kwargs:
    :keyword baudrate (int): Default=9600
    :keyword bytesize: Default=EIGHTBITS (8-bits)
    :keyword parity: Default=PARITY_NONE (none)
    :keyword stopbits: Default=STOPBITS_ONE (1)
    :keyword timeout (float): Read Timeout Default=None (pySerial Default=None)
    :keyword xonxoff (bool): Software Flow Control Default=False
    :keyword rtscts (bool): Hardware RTS/CTS Default=False
    :keyword dsrdtr (bool): Hardware DSR/DTR Default=False
    :keyword write_timeout (float): Write Timeout Default=None (pySerial Default=None)
    :keyword inter_byte_timeout (float): Inter-character timeout Default=None
    :return: reference to the port
    :rtype: serial.Serial | None
    """
    suppress_serial_exception = kwargs.pop('suppress_serial_exception', True)
    suppress_info_logging = kwargs.pop('suppress_info_logging', False)

    # Manage Serial Port Parameters
    settings = dict(port=port,
                    baudrate=9600,
                    bytesize=serial.EIGHTBITS,
                    parity=serial.PARITY_NONE,
                    stopbits=serial.STOPBITS_ONE,
                    timeout=None,
                    xonxoff=False,
                    rtscts=False,
                    dsrdtr=False,
                    write_timeout=None,
                    inter_byte_timeout=None,
                    )
    # Update with kwargs ignoring keys not already in settings
    settings.update((key, kwargs[key]) for key in settings.keys() & kwargs.keys())

    # Manage Exclusive Access to Port (http://stackoverflow.com/a/19823120/3571110)
    try:
        interface = serial.Serial(**settings)
        if interface.is_open:
            # Get an exclusive lock on the port
            try:
                fcntl.flock(interface.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
                # http://stackoverflow.com/a/17983721/3571110
                fcntl.ioctl(interface.fileno(), termios.TIOCEXCL)
            except IOError:
                if not suppress_info_logging:
                    _logger_ST.info("Port {0}: BUSY".format(settings['port']))
                close_serial_port(interface)
                return None
            try:
                interface.reset_input_buffer()
                interface.reset_output_buffer()
                interface.write_timeout = 0.0005
                interface.write(b'\r\n')
                interface.write_timeout = settings['write_timeout']
                interface.timeout = 0.0005
                interface.read(size=2)
                interface.timeout = settings['timeout']
            except IOError:
                if not suppress_info_logging:
                    _logger_ST.info("Port {0}: NOT READY".format(settings['port']))
                close_serial_port(interface, suppress_info_logging=suppress_info_logging)
                return None
            else:
                if not suppress_info_logging:
                    _logger_ST.info("Port {0}@{1}: OPEN".format(interface.port, interface.baudrate))
                return interface
    except (serial.SerialException, OSError) as err:
        if not suppress_serial_exception:
            if err.errno == errno.ENOENT:
                _logger_ST.warning("Port {0}: DOES NOT EXIST".format(settings['port']))
            else:
                _logger_ST.warning("Port {} {}: {}".format(settings['port'], type(err).__name__, err))
        return None
    except Exception as err:
        _logger_ST.warning("Port {} {}: {}".format(settings['port'], type(err).__name__, err))
        _logger_ST.debug(traceback.format_exc())
        return None


def close_serial_port(interface, **kwargs):
    """

    :param interface:
    :type interface: serial.Serial
    :param kwargs:
    :keyword suppress_info_logging:
    :return:
    """
    suppress_info_logging = kwargs.pop('suppress_info_logging', False)

    if isinstance(interface, str):
        try:
            interface = serial.Serial(interface)
        except serial.SerialException:
            _logger_ST.warning(
                'close_serial_port({0}): UNABLE TO ACCESS VIA PORT NAME, TRY SERIAL OBJECT'.format(interface))
    if isinstance(interface, serial.Serial):
        try:
            port_name = interface.port
        except AttributeError:
            port_name = 'UNKNOWN'
        if interface.isOpen():
            try:
                fcntl.flock(interface.fileno(), fcntl.LOCK_UN)
            except (TypeError, IOError) as err:
                _logger_ST.debug("Port {} fcntl.LOCK_UN {}: {}".format(port_name, type(err).__name__, err))
            try:
                fcntl.ioctl(interface.fileno(), termios.TIOCNXCL)
            except IOError as err:
                _logger_ST.debug("Port {} termios.TIOCNXCL {}: {}".format(port_name, type(err).__name__, err))
            try:
                interface.close()
            except OSError:
                pass
            if not suppress_info_logging:
                _logger_ST.info('Port {0}: CLOSED'.format(port_name))
        else:
            if not suppress_info_logging:
                _logger_ST.info('Port {0}: ALREADY CLOSED'.format(port_name))


if __name__ == '__main__':
    from future.builtins import input
    from AsiUtilities import log

    log.init_log('DEBUG')

    my_ports_available = get_available_ports(ports_to_skip='/dev/ttyS0', suppress_info_logging=False)
    print(my_ports_available)
    # close_serial_port('/dev/ttyUSB0')
    if my_ports_available:
        my_port = open_serial_port(my_ports_available[0])
        input("\nHolding Port {} Open...PRESS ENTER TO RELEASE".format(my_port.name))
        close_serial_port(my_port)
