# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

import simplejson as json
import numpy
import datetime


class JsonStdTypesEncoder(object):
    @staticmethod
    class StdTypesEncoder(json.JSONEncoder):
        def default(self, obj):
            # Convert set to list
            if isinstance(obj, set):
                obj = list(obj)
            # Simplify Single Element Lists
            if isinstance(obj, list):
                # if len(obj) == 0:
                #     return None
                # elif len(obj) == 1:
                #     return obj[0]
                # else:
                return obj
            # Convert Datetime to ISO 8601 string
            elif isinstance(obj, datetime.datetime):
                return obj.isoformat()
            # Convert Numpy to List
            elif isinstance(obj, numpy.ndarray):
                if obj.ndim == 1:
                    return self.default(obj.tolist())
                else:
                    return [self.default(obj[i]) for i in range(obj.shape[0])]
            # Simplify Generic Numpy
            elif isinstance(obj, numpy.generic):
                return obj.item()
            return json.JSONEncoder.default(self, obj)

    # @staticmethod
    # def json_obj_hook(obj):
    #     return obj

    @staticmethod
    def dumps(*args, **kwargs):
        kwargs.setdefault('cls', JsonStdTypesEncoder.StdTypesEncoder)
        return json.dumps(*args, **kwargs)

    @staticmethod
    def loads(*args, **kwargs):
        # kwargs.setdefault('object_hook', JsonStdTypesEncoder.json_obj_hook)
        return json.loads(*args, **kwargs)

    @staticmethod
    def dump(*args, **kwargs):
        kwargs.setdefault('cls', JsonStdTypesEncoder.StdTypesEncoder)
        return json.dump(*args, **kwargs)

    @staticmethod
    def load(*args, **kwargs):
        # kwargs.setdefault('object_hook', JsonStdTypesEncoder.json_obj_hook)
        return json.load(*args, **kwargs)
