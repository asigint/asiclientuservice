import os
import sys
import yaml
import logging
import logging.config
import traceback
from colorlog import ColoredFormatter
import datetime
from six import string_types


def load_logging_config(logging_config_filename=None, logging_output_filename=None):
    DEFAULT_SETTINGS_USED = False
    FILE_LOADED = False
    LOG_CONFIG_FILE_DEFAULT = 'logging_config.yml'
    logger = None
    if logging_config_filename is None:
        if os.path.exists(LOG_CONFIG_FILE_DEFAULT):
            logging_config_filename = LOG_CONFIG_FILE_DEFAULT
    if logging_config_filename is not None and os.path.exists(logging_config_filename):
        try:
            with open(logging_config_filename, 'r') as stream:
                logging_config = yaml.safe_load(stream)
                if 'file' in logging_config['handlers']:
                    if logging_output_filename is None:
                        logging_output_filename = os.path.splitext(logging_config_filename)[0].replace('_logging',
                                                                                                       '') + '.log'
                    logging_config['handlers']['file']['filename'] = logging_output_filename
                logging.config.dictConfig(logging_config)
                FILE_LOADED = True
                logger = logging.getLogger()
        except Exception as err:
            logging.warning("load_logging_config() load config file -> {}: {}".format(type(err).__name__, err))
    else:
        logger = setup_logger()
        DEFAULT_SETTINGS_USED = True
    logging.critical("START TIME: " + str(datetime.datetime.now()))
    if DEFAULT_SETTINGS_USED:
        logging.info("LOGGING INITIALIZED WITH ASI DEFAULTS")
    elif FILE_LOADED:
        logging.info("LOGGING INITIALIZED WITH FILE: {}".format(logging_config_filename))
    if logging_config_filename is not None and not FILE_LOADED:
        logging.warning("LOGGING FAILED TO OPEN LOG CONFIG: {}".format(logging_config_filename))
    return logger


def setup_logger(level=logging.DEBUG, name=None, stream=sys.stderr):
    """Return a logger with a default ColoredFormatter.
    :param level:
    :type level: int
    :param name:
    :type name: str | None
    :param stream:
    :type stream: file
    :return logger
    :rtype logging.Logger
    """

    if isinstance(name, string_types):
        logger = logging.getLogger(name=name)
    else:
        logger = logging.getLogger()
    logger.setLevel(level=level)

    if not len(logger.handlers):
        handler = get_custom_handler(stream)
        logger.addHandler(handler)

    return logger


def get_custom_handler(stream=sys.stderr):
    """
    Return a color and custom format handler
    :param stream:
    :type stream: file
    :return:
    """

    # Attributes at: https://docs.python.org/3/library/logging.html#logrecord-attributes
    formatter = ColoredFormatter(
        '\r'
        '%(asctime)s.%(msecs)03d:'
        '%(log_color)s%(levelname)-1.1s%(reset)s'
        ':%(module)19.19s:'
        '%(log_color)s%(message)s%(reset)s',
        datefmt='%H:%M:%S',
        reset=True,
        log_colors={
            'DEBUG': 'cyan',
            'INFO': 'green',
            'WARNING': 'yellow',
            'ERROR': 'red',
            'CRITICAL': 'red,bg_white',
        }
    )

    # Pycharm makes all stderr RED...so change to stdout...only works in DEBUG
    if hasattr(traceback.sys, 'original_argv') and stream == sys.stderr:
        if 'pycharm' in traceback.sys.original_argv[0]:
            stream = sys.stdout

    handler = logging.StreamHandler(stream=stream)
    handler.setFormatter(formatter)
    return handler
