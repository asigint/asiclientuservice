#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

import base64
import datetime

import numpy as np
import simplejson as json

from .time_convert import epoch_from_datetime, datetime_local_from_epoch


class JsonNpB64Encoder(object):
    """
    Based On:
    http://stackoverflow.com/questions/27909658/json-encoder-and-decoder-for-complex-numpy-arrays/27913569#27913569
    http://stackoverflow.com/questions/3488934/simplejson-and-numpy-array
    """
    @staticmethod
    class NumpyEncoder(json.JSONEncoder):
        def default(self, obj):
            """
            If input object is an ndarray it will be converted into a dict
            holding dtype, shape and the data, base64 encoded.
            """
            if isinstance(obj, np.ndarray):
                data_b64 = base64.b64encode(np.ascontiguousarray(obj).data)
                return dict(__ndarray__=data_b64,
                            dtype=str(obj.dtype),
                            shape=obj.shape)
            # Convert single items to built-in python types
            elif isinstance(obj, np.generic):
                return obj.item()
            elif isinstance(obj, datetime.datetime):
                return dict(__datetime__=epoch_from_datetime(obj))
            # Let the base class default method raise the TypeError
            return json.JSONEncoder.default(self, obj)

    @staticmethod
    def json_numpy_obj_hook(dct):
        """
        Decodes a previously encoded numpy ndarray
        with proper shape and dtype
        :param dct: (dict) json encoded ndarray
        :return: (ndarray) if input was an encoded ndarray
        """
        if isinstance(dct, dict) and '__ndarray__' in dct:
            data = base64.b64decode(dct['__ndarray__'])
            return np.frombuffer(data, dct['dtype']).reshape(dct['shape'])
        elif isinstance(dct, dict) and '__datetime__' in dct:
            return datetime_local_from_epoch(dct['__datetime__'])
        return dct

    @staticmethod
    def dumps(*args, **kwargs):
        kwargs.setdefault('cls', JsonNpB64Encoder.NumpyEncoder)
        return json.dumps(*args, **kwargs)

    @staticmethod
    def loads(*args, **kwargs):
        kwargs.setdefault('object_hook', JsonNpB64Encoder.json_numpy_obj_hook)
        return json.loads(*args, **kwargs)

    @staticmethod
    def dump(*args, **kwargs):
        kwargs.setdefault('cls', JsonNpB64Encoder.NumpyEncoder)
        return json.dump(*args, **kwargs)

    @staticmethod
    def load(*args, **kwargs):
        kwargs.setdefault('object_hook', JsonNpB64Encoder.json_numpy_obj_hook)
        return json.load(*args, **kwargs)


if __name__ == '__main__':
    # noinspection PyTypeChecker
    expected = np.arange(100, dtype=np.complex)
    # dumped = json.dumps(expected, cls=NpJson.NumpyEncoder)
    # result = json.loads(dumped, object_hook=NpJson.json_numpy_obj_hook)
    dumped = JsonNpB64Encoder.dumps(expected)
    result = JsonNpB64Encoder.loads(dumped)

    # None of the following assertions will be broken.
    assert result.dtype == expected.dtype, "Wrong Type"
    assert result.shape == expected.shape, "Wrong Shape"
    assert np.allclose(expected, result), "Wrong Values"

    print(result)
