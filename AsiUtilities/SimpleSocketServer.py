#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

import errno
import logging
import os
import queue
import select
import socket
import sys
import threading
import traceback

from colorama import Fore

from AsiUtilities.JsonStdTypesEncoder import JsonStdTypesEncoder


class SimpleSocketServer(threading.Thread):
    def __init__(self, queue_from_clients=None, queue_to_clients=None,
                 ip_addr='', ip_port=10000, max_connections=5, netstring=False):
        """
        Simple Socket Server with a single input and single output queue.
        Output goes to ALL clients

        :param queue_from_clients:
        :type queue_from_clients: queue.Queue | None
        :param queue_to_clients:
        :type queue_to_clients: queue.Queue | None
        :param ip_addr: Server IP address to limit incoming connections to a specific interface (default='' (aka 'ANY'))
        :type ip_addr: str
        :param ip_port: Server IP port number for incoming connections (default=10000)
        :type ip_port: int
        :param max_connections: maximum client connections (default=5)
        :type max_connections: int
        """
        self.logger = logging.getLogger(name=self.__class__.__name__)
        super(SimpleSocketServer, self).__init__()
        self._stop_request = threading.Event()

        self.queue_from_clients = queue_from_clients
        self.queue_to_clients = queue_to_clients

        self.server = None
        """:type : socket.socket | None"""
        self.server_max_connections = max_connections  # socket default=5
        self.server_max_rx_buffer = 65536  # MAXBUFFER=65536 for IP Packets
        self.server_ip_addr = ip_addr
        self.server_ip_port = ip_port

        self.select_timeout = 1  # Timeout in seconds

        self.server_failed_log_suppress = False  # Used to suppress repeat failure logs

        self.netstring = netstring
        self.data = ''

    def _create_server(self):
        # Create a TCP/IP socket
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
        self.server.setblocking(False)

        # Bind the socket to the port
        try:
            self.server.bind((self.server_ip_addr, self.server_ip_port))
            self.server_failed_log_suppress = False
            self.logger.info("{}:{} <- Started".format(*self.server.getsockname()))
        except (IOError, OSError):
            if not self.server_failed_log_suppress:
                self.logger.critical("{}:{} <- FAILED".format(self.server_ip_addr, self.server_ip_port))
                self.server_failed_log_suppress = True
            self._close_server()
            # Wait 5 sec before trying again
            self._stop_request.wait(5)
        else:
            # Listen for incoming connections
            self.server.listen(self.server_max_connections)

    def _close_server(self):
        if self.server is not None:
            try:
                server_name = self.server.getsockname()
            except (IOError, OSError):
                server_name = (self.server_ip_addr, self.server_ip_addr)
            self.server.close()
            self.server = None
            self.logger.info("{}:{} <- Stopped".format(*server_name))

    def _close_client(self, client_address, active_socket):
        try:
            server_name = active_socket.getsockname()
        except (IOError, OSError):
            server_name = (self.server_ip_addr, self.server_ip_addr)
        self.logger.info("{2}:{3}{0}XX{1}{4}:{5}".format(Fore.RED, Fore.GREEN, *(client_address + server_name)))
        try:
            active_socket.close()
        except (IOError, OSError):
            pass

    def run(self):
        clients = {}
        while not self._stop_request.is_set():
            if self.server is None:
                for client in clients.copy().keys():
                    self._close_client(*clients.pop(client))
                self._create_server()
                continue
            try:
                while not self._stop_request.is_set():
                    # Check status of sockets
                    outputs = list(clients)
                    inputs = [self.server] + outputs
                    readable, writable, exceptional = select.select(inputs, outputs, inputs, self.select_timeout)

                    # Check timeout condition (aka nothing to do)
                    if not (readable or writable or exceptional):
                        # Keep out queue clear if not clients
                        if not outputs:
                            while not self.queue_to_clients.empty():
                                try:
                                    self.queue_to_clients.get_nowait()
                                except queue.Empty:
                                    pass
                        self._stop_request.wait(0.01)
                        continue

                    # Handle inputs
                    for active_socket in readable:
                        assert isinstance(active_socket, socket.socket)
                        if active_socket is self.server:
                            # A "readable" server socket is ready to accept a connection
                            client_socket, client_address = active_socket.accept()
                            self.logger.info("{0}:{1}->{2}:{3}".format(*(client_address +
                                                                         client_socket.getsockname())))
                            client_socket.setblocking(False)
                            clients[client_socket] = client_address
                        else:
                            data = None
                            try:
                                data = active_socket.recv(self.server_max_rx_buffer)
                            except (IOError, OSError) as err:
                                if err.errno == errno.ECONNRESET or err.errno == errno.EINVAL \
                                        or err.errno == errno.EHOSTUNREACH:
                                    # Windows causes a Connection Reset on Telnet Quit
                                    # Suspend/Resume can cause Invalid Argument
                                    # Host Disconnect can cause No Route to Host (aka Host Unreachable)
                                    self.logger.warning("{0}:{1} <- Connection Reset".format(*clients[active_socket]))
                                else:
                                    self.logger.error("Socket Error: {}".format(err))
                                    for err_data in traceback.extract_tb(sys.exc_info()[2], limit=3):
                                        filename, line_no, function_name, line_text = err_data
                                        self.logger.debug(
                                            "{}:{} in {}".format(os.path.basename(filename), line_no, function_name))
                            if data:
                                if self.queue_from_clients is not None:
                                    self.queue_from_clients.put((clients[active_socket], data))
                            else:
                                # Interpret empty result as closed connection
                                self._close_client(clients.pop(active_socket), active_socket)

                    # Handle outputs
                    try:
                        next_msg = self.queue_to_clients.get_nowait()
                        if not isinstance(next_msg, str):
                            next_msg = JsonStdTypesEncoder.dumps(next_msg)
                        if self.netstring:
                            netstring = '{:d}:{:s},'
                            next_msg = netstring.format(len(next_msg), next_msg)
                    except (queue.Empty, AttributeError):
                        pass
                    else:
                        for active_socket in writable:
                            try:
                                if active_socket in clients:
                                    active_socket.send(next_msg.encode())
                            except (IOError, OSError) as err:
                                if err.errno == errno.EHOSTUNREACH:
                                    self.logger.warning("{0}:{1} <- No route to host".format(*clients[active_socket]))
                                elif err.errno == errno.EBADF:
                                    self.logger.warning("{0}:{1} <- Socket Lost".format(*clients[active_socket]))
                                elif err.errno == errno.ECONNRESET:
                                    self.logger.warning("{0}:{1} <- Connection Reset".format(*clients[active_socket]))
                                else:
                                    self.logger.critical("Socket Error: {}".format(err))
                                    for err_data in traceback.extract_tb(sys.exc_info()[2], limit=3):
                                        filename, line_no, function_name, line_text = err_data
                                        self.logger.debug(
                                            "{}:{} in {}".format(os.path.basename(filename), line_no, function_name))
                                self._close_client(clients.pop(active_socket), active_socket)

                    # Handle "exceptional conditions"
                    for active_socket in exceptional:
                        if active_socket is self.server:
                            socket_name = (self.server_ip_addr, self.server_ip_port)
                            self._close_server()
                        else:
                            active_socket.close()
                            socket_name = clients.pop(active_socket)
                        self.logger.warning("{0}:{1} <- Socket Exception".format(*socket_name))

                    self._stop_request.wait(0.001)

            except (IOError, OSError) as err:
                self.logger.critical("Socket Error: {}".format(err))
                for err_data in traceback.extract_tb(sys.exc_info()[2], limit=3):
                    filename, line_no, function_name, line_text = err_data
                    self.logger.debug("{}:{} in {}".format(os.path.basename(filename), line_no, function_name))
                self._close_server()
        self._close_server()

    def stop(self):
        self._stop_request.set()

    def is_stopped(self):
        return self._stop_request.is_set()


class TimeStampGenerator(threading.Thread):
    def __init__(self, output_queues):
        """

        :param output_queues:
        :type output_queues: list | queue.Queue
        """
        self.logger = logging.getLogger(name=self.__class__.__name__)
        super(TimeStampGenerator, self).__init__()
        self.output_queues = output_queues
        if not isinstance(self.output_queues, list):
            self.output_queues = [self.output_queues]
        self._stop_request = threading.Event()

    def run(self):
        import datetime
        while not self._stop_request.is_set():
            # clear any unused prior value
            try:
                for q in self.output_queues:
                    while not q.empty():
                        _ = q.get_nowait()
                    q.put(str(datetime.datetime.now()) + "\r\n")
            except queue.Empty:
                pass
            self._stop_request.wait(0.5)
        return

    def stop(self):
        self._stop_request.set()

    def is_stopping(self):
        return self._stop_request.is_set()

    def is_running(self):
        return not self._stop_request.is_set()


def add_args(config):
    from argparse import ArgumentParser
    """Adds commandline arguments and formatted Help"""
    parser = ArgumentParser(description=os.path.basename(__file__))
    parser.add_argument('--ip_port',
                        help="CarteNav Server Port (default={})".format(config['ip_port']),
                        type=int,
                        default=config['ip_port'])
    parser.add_argument('--ip_addr',
                        help="CarteNav Server Address (default={})".format(config['ip_addr']),
                        type=str,
                        default=config['ip_addr'])
    parser.add_argument('--timestamp',
                        help="Timestamp Source",
                        action='store_true',
                        default=config['timestamp'])
    args = parser.parse_args()
    for option in config.keys():
        config[option] = getattr(args, option, config[option])
    return config


def main():
    import pprint
    from AsiUtilities import log

    application_config = dict(
        ip_addr='',
        ip_port=10000,
        timestamp=False,
    )

    log.init_log('DEBUG')
    logger = logging.getLogger(name=os.path.basename(__file__))
    application_config = add_args(application_config)
    logger.debug('CONFIG:\n' + pprint.pformat(application_config))

    echo_queue = queue.Queue()

    generator = None
    if application_config['timestamp']:
        generator = TimeStampGenerator(echo_queue)
        generator.start()

        server = SimpleSocketServer(queue_to_clients=echo_queue,
                                    ip_addr=application_config['ip_addr'],
                                    ip_port=application_config['ip_port'])
    else:
        server = SimpleSocketServer(echo_queue, echo_queue,
                                    ip_addr=application_config['ip_addr'],
                                    ip_port=application_config['ip_port'])
    server.start()

    try:
        while server.is_alive():
            server.join(1)
    except (KeyboardInterrupt, SystemExit):
        logger.info("EXITING")
    finally:
        server.stop()
        server.join(5)
        if generator is not None:
            generator.stop()
            generator.join(5)


if __name__ == '__main__':
    from signal import signal, SIGTERM
    from sys import exit
    from os.path import basename

    from setproctitle import setproctitle

    setproctitle(basename(__file__))
    # Catch SIGTERM as SystemExit
    signal(SIGTERM, lambda signum, stack_frame: exit(0))

    main()
