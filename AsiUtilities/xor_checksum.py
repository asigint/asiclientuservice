# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

from __future__ import unicode_literals
from builtins import str
from six import string_types
import operator
import functools


def xor_checksum(sentence):
    """
    Return (in HEX) the XOR checksum of the provided sentence
    :param sentence:
    :type sentence: string_types
    :return: hex string
    """
    if isinstance(sentence, string_types):
        # TODO: There has to be a cleaner Py2/3 way to do this...maybe ensuring unicode_literals?
        sentence = str.encode(str(sentence))
    checksum = format(functools.reduce(operator.xor, (s for s in sentence), 0), '02X')
    return checksum
