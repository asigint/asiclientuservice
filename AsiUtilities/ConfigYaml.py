import yaml
import os
import logging


def load_config_yaml(config_file=None, default_dict=None, logger=None):
    """
    load a yaml configuration file matching the provide file basename
    and validating against a default dictionary
    default_dict is modified in place
    :param config_file: fully qualified filename
    :type config_file: str
    :param default_dict:
    :type default_dict: dict
    :param logger:
    :type logger: logging.Logger | None
    :return:
    """
    if logger is None:
        logger = logging.getLogger()
    extras_dict = {}

    yaml_file, yaml_filename, main_filename = validate_yaml_filename(config_file, logger)

    # Verify file exists and is readable
    if os.path.isfile(yaml_file) and os.access(yaml_file, os.R_OK):
        try:
            with open(yaml_file, 'r') as stream:
                yaml_config = yaml.safe_load(stream)
                if yaml_config is not None:
                    if default_dict is None:
                        default_dict = yaml_config
                    else:
                        # update only existing items
                        extras_dict = {}
                        for k, v in yaml_config.items():
                            if k in default_dict:
                                default_dict[k] = v
                            else:
                                extras_dict[k] = v
                else:
                    logger.warning("Config File '{}' exists but is empty.".format(yaml_file))
        except IOError as err:
            logger.warning("{} IOError: {}".format(yaml_file, err))
    else:
        yaml_filename = 'built-in defaults'

    # Log Parameters In Use
    logger.info("{} configuration loaded from {}".format(main_filename, yaml_filename))
    logger.debug("{} ACTIVE parameters: {}".format(main_filename, default_dict))

    # List unmatched parameters for debugging
    if extras_dict:
        logger.debug("{} EXTRA  parameters: {}".format(main_filename, extras_dict))


def save_config_yaml(config_file=None, default_dict=None, logger=None):
    if logger is None:
        logger = logging.getLogger()
    yaml_file, yaml_filename, main_filename = validate_yaml_filename(config_file, logger)
    try:
        with open(yaml_file, 'w') as stream:
            yaml.dump(default_dict, stream, default_flow_style=False)
    except IOError as err:
        logger.warning("{} IOError: {}".format(yaml_file, err))


def validate_yaml_filename(filename=None, logger=None):
    if logger is None:
        logger = logging.getLogger()

    if filename is None:
        try:
            import __main__
            filename = __main__.__file__
        except Exception as err:
            logger.warning("load_config_yaml() get main filename -> {}: {}".format(type(err).__name__, err))

    # TODO: This is unreliable and should be fixed
    main_filename = os.path.splitext(os.path.split(filename)[1])[0]

    # Check if filename is already a yaml, else assume same basename
    main_file_ext = os.path.splitext(filename)[1]
    if main_file_ext != '.yml' and main_file_ext != '.yaml':
        yaml_file = os.path.splitext(filename)[0] + '.yml'
    else:
        yaml_file = filename
    yaml_filename = os.path.split(yaml_file)[1]
    return yaml_file, yaml_filename, main_filename
