#!/usr/bin/env python
# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

import binascii
import logging
import time
import traceback
from datetime import datetime
from queue import Queue, Empty  # queue (not Queue) requires future in Python 2.7
from threading import Thread, Event

import serial
from future.builtins import dict
from six import binary_type

import AsiUtilities.SerialTools as SerialTools


class SerialManager(Thread):
    def __init__(self, port=None, **kwargs):
        """

        :param port: eg. /dev/ttyUSB0, /dev/ttyS0
        :type port: str | None
        :param kwargs:
        :keyword baudrate: Default=9600
        :keyword bytesize: Default=8 (8-bits)
        :keyword parity: Default='N' (none)
        :keyword stopbits: Default=1
        :return:
        """
        super(SerialManager, self).__init__()
        self._stop_request = Event()

        # Custom Logger
        self._logger_sm = logging.getLogger(name='SerialManager')

        # Manage Common Port Settings
        settings = dict(port=port,
                        baudrate=9600,
                        bytesize=serial.EIGHTBITS,
                        parity=serial.PARITY_NONE,
                        stopbits=serial.STOPBITS_ONE)
        settings.update(kwargs)
        self.settings = settings

        # Initially show any serial exceptions by default
        if 'suppress_serial_exception' not in self.settings:
            self.settings['suppress_serial_exception'] = False

        # Initially show any serial info by default
        if 'suppress_info_logging' not in self.settings:
            self.settings['suppress_info_logging'] = False

        # Manage Reference to Port
        self.port = None
        """ :type : serial.Serial """

        # In/Out Queues
        self.from_serial_queue = Queue()
        self.to_serial_queue = Queue()

        # Data Read Parameters
        self.data_read_delay = None
        self.data_read_timeout = None
        self.data_read_timestamp = 0

        # Thread/Process Loop Init/Management
        self._loop_delay = 0.001  # Prevent 100% CPU
        self.daemon = True

        # Extra debugging
        self._EXTRA_DEBUG = False

        # Manage Input Buffer Flushing
        self._flush_input_buffer_interval = None
        self._flush_input_buffer_last_ts = time.time()

    def set_flush_input_buffer_interval(self, interval):
        if interval is None or isinstance(interval, (int, float)):
            self._flush_input_buffer_interval = interval
        return self._flush_input_buffer_interval

    def get_flush_input_buffer_interval(self):
        return self._flush_input_buffer_interval

    def get_queues(self):
        """
        returns in and output queues references
        :return: from_serial_queue, to_serial_queue
        :rtype: Queue, Queue
        """
        return self.from_serial_queue, self.to_serial_queue

    def get_from_serial_queue(self):
        return self.from_serial_queue

    def get_to_serial_queue(self):
        return self.to_serial_queue

    def _serial_read(self):
        try:
            read_waiting = self.port.in_waiting
            if read_waiting:
                data = self.port.read(read_waiting)
                if self._EXTRA_DEBUG:
                    self._logger_sm.warning("READ [{}]: {}".format(read_waiting, binascii.hexlify(data).upper()))
                return data
        except (IOError, serial.SerialException) as err:
            self._logger_sm.warning("{}: {}".format(type(err).__name__, err))
            self.disconnect_port()
        return None

    def connect_port(self):
        # Get the port with an exclusive lock
        self.port = SerialTools.open_serial_port(**self.settings)
        if self.port is not None:
            self.settings['suppress_serial_exception'] = False
            return True
        else:
            # Suppress retry errors until successful
            self.settings['suppress_serial_exception'] = True
            return False

    def disconnect_port(self):
        # Close port releasing locks
        SerialTools.close_serial_port(self.port)
        self.port = None

    # Override this method to post-process the serial read
    def from_serial_queue_put_wrapper(self, data_buffer, timestamp=0):
        """
        Enqueue the data_buffer and timestamp as a tuple

        :param data_buffer: serial port data buffer
        :type data_buffer: bytes | str
        :param timestamp: timestamp for data_buffer
        :type timestamp: float | int
        :return: no return value
        """
        self.from_serial_queue.put((data_buffer, timestamp))

    # Override this method to pre-process the serial write
    def to_serial_queue_get_wrapper(self):
        try:
            return self.to_serial_queue.get_nowait()
        except Empty:
            return None

    def run(self):
        while self.is_running():
            # print("***serial loop***")
            while self.port is None and self.is_running():
                # print("***connect loop***")
                if not self.connect_port():
                    self._stop_request.wait(self._loop_delay)
                # Reset timestamp used for timeout
                self.data_read_timestamp = time.time()
            if self.is_running():
                # print("***running section***")
                if self.data_read_delay:
                    self._stop_request.wait(self.data_read_delay)
                try:  # catch any unexpected serial port errors
                    # ***READ***
                    # self._logger_sm.warning("***reading***" + str(datetime.now()))
                    rx_data = self._serial_read()
                    if rx_data:  # Not None or empty string (aka '')
                        self.data_read_timestamp = time.time()
                        self.from_serial_queue_put_wrapper(rx_data, self.data_read_timestamp)
                    else:
                        # print("NO RX DATA")
                        if self.data_read_timeout and time.time() > (self.data_read_timestamp + self.data_read_timeout):
                            self._logger_sm.warning("Port {}: TIMEOUT".format(self.settings['port']))
                            self.disconnect_port()
                    if self.port is not None and self._flush_input_buffer_interval:
                        if time.time() < (self._flush_input_buffer_last_ts + self._flush_input_buffer_interval):
                            self.port.reset_input_buffer()
                            self._flush_input_buffer_last_ts = time.time()
                    # ***WRITE***
                    # self._logger_sm.warning("***writing***" + str(datetime.now()))
                    # A read error may have closed the port
                    if self.port is not None:
                        tx_data = self.to_serial_queue_get_wrapper()
                        if tx_data is not None:
                            if self._EXTRA_DEBUG:
                                self._logger_sm.warning("***writing*** BITS={} @ {}".format(len(tx_data) * 8,
                                                                                            datetime.now().strftime(
                                                                                                '%H:%M:%S.%f')))
                                self._logger_sm.debug('WRITING: {}'.format(binascii.hexlify(tx_data).upper()))
                            try:
                                self.port.write(tx_data)
                                # stream.write(tx_data)
                            except serial.SerialTimeoutException:
                                self._logger_sm.critical(
                                    "Port {}: WRITE TIMEOUT for: {}".format(self.settings['port'], tx_data))
                except Exception as err:
                    # Anticipated Exceptions should be handled already, assume port failure
                    self._logger_sm.warning("Port {} {}: {}".format(self.settings['port'], type(err).__name__, err))
                    self._logger_sm.debug(traceback.format_exc())
                    self.disconnect_port()
            # Prevent a busy loop eating up CPU
            self._stop_request.wait(self._loop_delay)
        self.disconnect_port()

    def stop(self):
        self._stop_request.set()

    def is_stopping(self):
        return self._stop_request.is_set()

    def is_running(self):
        return not self._stop_request.is_set()

    def get_port_connected(self):
        if self.port is not None:
            return True
        return False


class SerialManagerLines(SerialManager):
    """
    SerialManager that outputs only complete decoded lines

    Complete lines can end in '\n' or '\r\n'.
    ('\r' will be removed, but then combined with the next line,
    so this really doesn't support '\r')

    Data is decoded as unicode strings
    (unlike SerialManager which will return bytes)
    """

    def __init__(self, **kwargs):
        super(SerialManagerLines, self).__init__(**kwargs)
        self._decode_buffer = ''

    def from_serial_queue_put_wrapper(self, data_buffer, timestamp=0):
        """
        Enqueue the data_buffer and timestamp as a tuple
        Enqueue only complete decoded lines

        :param data_buffer: serial port data buffer
        :type data_buffer: bytes | str
        :param timestamp: timestamp for data_buffer
        :type timestamp: float | int
        :return: no return value
        """
        # Python 3 Serial Data is bytes and you can't decode a string
        if isinstance(data_buffer, binary_type):
            data_buffer = data_buffer.decode(errors='ignore')
        self._decode_buffer += data_buffer
        lines = self._decode_buffer.splitlines()

        # Keep incomplete line or clear the history
        if lines and not self._decode_buffer.endswith('\n'):
            self._decode_buffer = lines[-1]
            del lines[-1]
        else:
            self._decode_buffer = ''
        for line in lines:
            self.from_serial_queue.put((line, timestamp))


def add_args(config):
    import os
    from argparse import ArgumentParser
    """Adds commandline arguments and formatted Help"""
    parser = ArgumentParser(description=os.path.basename(__file__))
    group = parser.add_mutually_exclusive_group()
    group.add_argument('port',
                       nargs='?',
                       help="The serial port to use (COM4, /dev/ttyUSB1 or similar). (default={})".format(
                           config['port']),
                       type=str,
                       default=config['port'])
    group.add_argument('-p', '--port',
                       help="The serial port to use (COM4, /dev/ttyUSB1 or similar). (default={})".format(
                           config['port']),
                       type=str,
                       default=config['port'])
    parser.add_argument('-b', '--baudrate',
                        help="Baudrate of serial port (default={})".format(config['baudrate']),
                        type=int,
                        default=config['baudrate'])
    parser.add_argument('-t', '--timeout',
                        help='Seconds until reading from serial port times out (default={})'.format(config['timeout']),
                        type=float,
                        default=config['timeout'], )
    parser.add_argument('-s', '--sleeptime',
                        help="Seconds to sleep before reading from serial port again (default={})".format(
                            config['sleeptime']),
                        type=float,
                        default=config['sleeptime'])
    parser.add_argument('-l', '--lines',
                        help="Split output into lines and only output complete lines (default={})".format(
                            config['lines']),
                        action="store_true",
                        default=config['lines'])
    args = parser.parse_args()
    # Update config from args.  Args not in config ignored.
    for option in config.keys():
        config[option] = getattr(args, option, config[option])
    return config


class AppConfig(object):
    def __init__(self):
        self._app_config = dict(
            port='/dev/compass_serial',
            baudrate=115200,
            sleeptime=None,
            timeout=1,
            lines=False,
        )

    def get_config(self):
        return self._app_config

    def set_config(self, new_config):
        # Update config from args.  Args not in config ignored.
        for option in self._app_config.keys():
            self._app_config[option] = getattr(new_config, option, self._app_config[option])


def main(app_config=AppConfig()):
    from os.path import basename
    from pprint import pformat
    from setproctitle import setproctitle
    from time import time
    from AsiUtilities import log

    setproctitle(basename(__file__))

    log.init_log('DEBUG')
    app_logger = logging.getLogger(name=basename(__file__))
    application_config = add_args(app_config.get_config())
    app_logger.debug('CONFIG:\n' + pformat(application_config))

    lines = application_config.pop('lines')
    if lines:
        s1 = SerialManagerLines(**application_config)
    else:
        s1 = SerialManager(**application_config)

    from_serial_queue, to_serial_queue = s1.get_queues()
    s1.data_read_delay = application_config['sleeptime']
    s1.start()

    max_delta = 0
    min_delta = 0
    avg_delta = 0
    last_time = time()
    try:
        while True:
            try:
                data, ts = from_serial_queue.get(False)
                if ts != last_time:
                    delta = ts - last_time
                    if delta > 10:
                        print("******* DELTA:{} CURRENT:{} PRIOR:{}".format(delta, ts, last_time))
                    else:
                        if avg_delta == 0:
                            avg_delta = delta
                        if min_delta == 0:
                            min_delta = delta
                        if delta > max_delta:
                            max_delta = delta
                        if delta < min_delta:
                            min_delta = delta
                        avg_delta = (avg_delta + delta) / 2
                    last_time = ts
                print("{!r}\tMIN:{:07.4f} MAX:{:07.4f} AVG:{:07.4f}".format(data, min_delta, max_delta, avg_delta))
            except Empty:
                pass
            to_serial_queue.put('AA'.encode())
            s1.join(0.005)
    except (KeyboardInterrupt, SystemExit):
        app_logger.info("EXITING...")
    finally:
        s1.stop()
        app_logger.debug("JOINING...")
        s1.join()
        app_logger.info("DONE.")


if __name__ == "__main__":
    main()
